<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AuthorOrderRatioController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('AuthorOrderRatioModel');
		$this->load->model('ProductModel');
	}

	public function index()
	{	
		$data['authorOrderRatioData'] = $this->AuthorOrderRatioModel->getAuthorOrderRatioData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.AUTHOR_ORDER_RATIO.'author-order-ratio-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function authorOrderRatioByAuthorId($id)
	{
		$data['authorOrderRatioDetails'] = $this->AuthorOrderRatioModel->getAuthorOrderRatioByAuthorId($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.AUTHOR_ORDER_RATIO.'author-ratio-listing-details', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function add()
	{	
		$data['authorData'] = $this->AuthorOrderRatioModel->getAuthorDate();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.AUTHOR_ORDER_RATIO.'add-author-order-ratio', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insert()
	{
		$this->form_validation->set_rules('author_id', 'Author name', 'required');
		$this->form_validation->set_rules('book_id', 'Book name', 'required');
		$this->form_validation->set_rules('book_price', 'Book price', 'required');
		$this->form_validation->set_rules('author_ratio', 'Author ratio', 'required');
		$this->form_validation->set_rules('author_amount', 'Total amount', 'required');
		
		if ($this->form_validation->run() == FALSE) 
		{
			$data['authorData'] = $this->AuthorOrderRatioModel->getAuthorDate();
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.AUTHOR_ORDER_RATIO.'add-author-order-ratio', $data);	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$data = array(
				'addedby_id' => $this->session->userdata('user_id'),
				'author_id' => $this->input->post('author_id'),
				'book_id' => $this->input->post('book_id'),
				'book_price' => $this->input->post('book_price'),
				'author_ratio' => $this->input->post('author_ratio'),
				'author_amount' => $this->input->post('author_amount')
			);
			
			if ($this->AuthorOrderRatioModel->insert($data)) 
			{
				//------ Stroe Logs --------
					$system_ip = get_client_ip();
		    	    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Add author book ratio','bookId'=>$this->input->post('book_id'),'authorId'=>$this->input->post('author_id'),'staus'=>'success');
		    	    $activity = json_encode($data);
		    	    logs($system_ip,$activity);
					//------ Stroe Logs --------
				$this->session->set_flashdata('success', 'Record added successfully.');
				redirect(base_url() . 'listing-author-order-ratio');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not added.');
					//echo 'Not added record ';
				redirect(base_url() . 'add-author-order-ratio');
			}
		}
	}

	public function authorRatioListing()
	{
		$data['authorRatioData'] = $this->AuthorOrderRatioModel->getAuthorRatioData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.AUTHOR_ORDER_RATIO.'author-ratio-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

}
?>