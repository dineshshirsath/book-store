<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SellerController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('SellerModel');
	}

	public function index()
	{	
		$data['sellerListingData'] = $this->SellerModel->getSellerListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SELLER.'seller-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function updateView($id)
	{
		$data['sellerUpdatData'] = $this->SellerModel->getUpdatSellerData($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SELLER.'edit-seller', $data);		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function update()
	{
		$data = $this->input->post();
		$id = $this->input->post('id');
		unset($data['submit']);

		if ($this->SellerModel->update($data, $id)) 
		{
			$this->session->set_flashdata('success', 'Record updated successfully.');
			redirect(base_url() . 'listing-author');
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not updated.');
				//echo 'Not added record ';
			redirect(base_url() . 'listing-author');
		}
	}

	public function authorAddedBooks()
	{
		$data['authorBooks'] = $this->SellerModel->authorAddedBooks();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SELLER.'author-added-books', $data);		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function authorBookUpdateView($id)
	{
		$data['authorBookData'] = $this->SellerModel->authorBooksUpdateData($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SELLER.'author-book-edit', $data);		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function authorBookUpdate()
	{
		$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/book/";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['overwrite'] = FALSE;
		$config['encrypt_name'] = FALSE;
		$config['remove_spaces'] = TRUE;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if($this->upload->do_upload('image'))
		{
			$uploadData = $this->upload->data();
			$image = $uploadData['file_name'];
		}
		else
		{
			print_r($this->upload->display_errors());exit;
			$image = '';
		}
		$id = $this->input->post('id');
		$data = array(
			'image' => $image
		);

		if ($this->SellerModel->authorBookUpdate($data, $id)) 
		{
			$this->session->set_flashdata('success', 'Record updated successfully');
			redirect(base_url() . 'listing-author-book');
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not updated.');
			redirect(base_url() . 'edit-author-book/'.$id);
		}
	}
	
	public function updateBookStatus()
	{
		$id = $this->input->post('id');
		$data = array(
			'status' => $this->input->post('status')
		);
		if ($this->SellerModel->updateBookStatusById($data, $id)) {
			//echo $this->db->last_query();
			echo json_encode(array('status'=>"1")); 
		}
	}

}
?>