<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BookController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('BookModel');
		$this->load->model('ProductModel');
	}

	public function index()
	{	
		$data['bookListingData'] = $this->ProductModel->getProductListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.BOOK.'book-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function add()
	{	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.BOOK.'add-book');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function addBookChapter($id)
	{	
		$data['bookTitleList'] = $this->ProductModel->getBookTitleList($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.BOOK.'add-book-chapter', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insert()
	{		
			$count = 0;
			foreach ($this->input->post('chapter_name') as $type) {
			    $count+= count($type);
			}
			for ($i=0; $i < $count; $i++) 
			{ 
				$count2 = count($this->input->post('chapter_name')[$i]);
				
				for ($j=0; $j < $count2; $j++) 
				{ 
					$data[] = array(
						'addedby_id' => $this->session->userdata('user_id'),
						'book_title' => $this->input->post('book_title'),
						'chapter_name' => $this->input->post('chapter_name')[$i][$j],
						'content' => $this->input->post('content')[$i][$j],
						'remark' => $this->input->post('remark')[$i][$j],
						'remark_description' => $this->input->post('remark_description')[$i][$j],
						'explanation_1' => $this->input->post('explanation_1')[$i][$j],
						'explanation_description_1' => $this->input->post('explanation_description_1')[$i][$j],
						'explanation_2 ' => $this->input->post('explanation_2')[$i][$j],
						'explanation_description_2' => $this->input->post('explanation_description_2')[$i][$j],
						'order' => $this->input->post('order')[$i][$j]
					);
				}
			}
			
			if ($this->BookModel->insert($data)) 
			{
				//------ Stroe Logs --------
					$system_ip = get_client_ip();
				    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Add book chpater','staus'=>'success');
				    $activity = json_encode($data);
				    logs($system_ip,$activity);
				//------ Stroe Logs --------
				$this->session->set_flashdata('success', 'Record added successfully.');
				redirect(base_url() . 'listing-book');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not added.');
					//echo 'Not added record ';
				redirect(base_url() . 'listing-book');
			}
	}

	public function addBookSubChapter($chapterId)
	{	
		
		$data['chapterNameList'] = $this->BookModel->getChapterDataById($chapterId);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.BOOK.'add-book-subchapter',$data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insertSubChapter()
	{
		$count = 0;
		foreach ($this->input->post('sub_chapter_name') as $type) {
		    $count+= count($type);
		}
		for ($i=0; $i < $count; $i++)
		{ 
			$count2 = count($this->input->post('sub_chapter_name')[$i]);
			
			for ($j=0; $j < $count2; $j++) 
			{ 
				$data[] = array(
					'addedby_id' => $this->session->userdata('user_id'),
					'book_title' => $this->input->post('book_title'),
					'chapter_id' => $this->input->post('chapter_id'),
					'sub_chapter_name' => $this->input->post('sub_chapter_name')[$i][$j],
					'content' => $this->input->post('content')[$i][$j],
					'remark' => $this->input->post('remark')[$i][$j],
					'remark_description' => $this->input->post('remark_description')[$i][$j],
					'explanation_1' => $this->input->post('explanation_1')[$i][$j],
					'explanation_description_1' => $this->input->post('explanation_description_1')[$i][$j],
					'explanation_2 ' => $this->input->post('explanation_2')[$i][$j],
					'explanation_description_2' => $this->input->post('explanation_description_2')[$i][$j],
					'order' => $this->input->post('order')[$i][$j]
				);
			}
		}
		
		if ($this->BookModel->insertSubChapter($data)) 
		{
			$subChpterId = $this->db->insert_id();
			$bookId = $this->BookModel->getBookIdWithSubChapter($subChpterId);

			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Add book sub-chpater','staus'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------

			$this->session->set_flashdata('success', 'Record added successfully.');
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not added.');
				//echo 'Not added record ';
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
	}


	public function updateChpterView($id)
	{
		$data['chapterUpdatData'] = $this->BookModel->getUpdatChapterData($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.BOOK.'edit-book-chapter', $data);		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function updateChapter()
	{
		$bookId = $this->BookModel->getBookId($this->input->post('id'));
		$data = $this->input->post();
		$id = $this->input->post('id');
		unset($data['submit']);
		if ($this->BookModel->updateChapter($data, $id)) 
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Update book chapter','staus'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			$this->session->set_flashdata('success', 'Record updated successfully.');
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not updated.');
				//echo 'Not added record ';
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
	}

	public function updateSubChpterView($id)
	{
		$data['subchapterUpdatData'] = $this->BookModel->getUpdatSubChapterData($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.BOOK.'edit-book-subchapter', $data);		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function updateSubChapter()
	{
		$bookId = $this->BookModel->getBookIdWithSubChapter($this->input->post('id'));
		$data = $this->input->post();
		$id = $this->input->post('id');
		unset($data['submit']);
		if ($this->BookModel->updateSubChapter($data, $id)) 
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Update book sub-chapter','staus'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			$this->session->set_flashdata('success', 'Record updated successfully.');
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not updated.');
				//echo 'Not added record ';
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
	}

	public function preview($bookId)
	{
		$data['bookInformation'] = $this->BookModel->getBookDetailsByBookId($bookId);	
		$data['bookChapterListingData'] = $this->BookModel->getBookChapterListData($bookId);		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.BOOK.'book-preview', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function getChpterById()
	{
		// POST data 
	    $postData = $this->input->post();
	        
	    // get data 
	    $data = $this->BookModel->getChpterByBookId($postData);
	    echo json_encode($data); 
	}

	public function deleteCapter($id)
	{
		$bookId = $this->BookModel->getBookId($id);

		$data = array(
					'status' => '1'
		);
		if ($this->BookModel->deleteCapter($id, $data)) 
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Delete book chapter','chapterId'=>$id,'staus'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			$this->session->set_flashdata('success', 'Record deleted successfully.');
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not deleted.');
				//echo 'Not added record ';
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
	}

	public function deleteSubCapter($id)
	{
		$data = array(
					'status' => '1'
		);
		$bookId = $this->BookModel->getBookIdWithSubChapter($id);

		if ($this->BookModel->deleteSubCapter($id, $data)) 
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Delete book sub-chapter','subChapterId'=>$id,'staus'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------

			$this->session->set_flashdata('success', 'Record deleted successfully.');
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not deleted.');
				//echo 'Not added record ';
			redirect(base_url() . 'view-book/'.$bookId[0]['book_title']);
		}
	}


}
?>