<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AuthorPaymentController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('AuthorPaymentModel');
	}

	public function index()
	{	
		$data['categoryListingData'] = $this->AuthorPaymentModel->getAuthorPaymentData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.AUTHOR_PAYMENT.'author-payment-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function add()
	{	
		$data['authorData'] = $this->AuthorPaymentModel->getAuthorDate();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.AUTHOR_PAYMENT.'add-author-payment', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insert()
	{
		$this->form_validation->set_rules('author_id', 'Author name', 'required');
		$this->form_validation->set_rules('received_by', 'Received by name', 'required');
		$this->form_validation->set_rules('paid_by', 'Paid by name', 'required');
		$this->form_validation->set_rules('pay_amount', 'Payment amount', 'required');
		$this->form_validation->set_rules('pay_type', 'Payment type', 'required');
		$this->form_validation->set_rules('remarks', 'Comment', 'required');
		
		if ($this->form_validation->run() == FALSE) 
		{
			$data['authorData'] = $this->AuthorPaymentModel->getAuthorDate();
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.AUTHOR_PAYMENT.'add-author-payment', $data);	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/Payment_PDF_Doc/";
			$config['allowed_types'] = 'pdf|csv|docx|doc';
			$config['overwrite'] = FALSE;
			$config['encrypt_name'] = TRUE;
			$config['remove_spaces'] = TRUE;
			
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload('file_upload'))
			{
				$uploadData = $this->upload->data();
				$file_upload = $uploadData['file_name'];
			}
			else
			{
				print_r($this->upload->display_errors());exit;
				$file_upload = '';
			}
			$data = array(
				'author_id' => $this->input->post('author_id'),
				'addedby_id' => $this->session->userdata('user_id'),
				'received_by' => $this->input->post('received_by'),
				'paid_by' => $this->input->post('paid_by'),
				'pay_amount' => $this->input->post('pay_amount'),
				'pay_type' => $this->input->post('pay_type'),
				'cheque_no' => $this->input->post('cheque_no'),
				'cheque_date' => date("y-m-d", strtotime($this->input->post('cheque_date'))),
				'transaction_id' => $this->input->post('transaction_id'),
				'bank_name' => $this->input->post('bank_name'),
				'transaction_date' => date("y-m-d", strtotime($this->input->post('transaction_date'))),
				'remarks' => $this->input->post('remarks'),
				'file_upload' => $file_upload
			);
			
			if ($this->AuthorPaymentModel->insert($data)) 
			{
				//------ Stroe Logs --------
					$system_ip = get_client_ip();
		    	    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Add author payment in cash,chaque,bank etc.','authorId'=>$this->input->post('author_id'),'staus'=>'success');
		    	    $activity = json_encode($data);
		    	    logs($system_ip,$activity);
				//------ Stroe Logs --------
				$this->session->set_flashdata('success', 'Record added successfully.');
				redirect(base_url() . 'listing-author-payment');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not added.');
					//echo 'Not added record ';
				redirect(base_url() . 'add-author-payment');
			}
		}
	}

}
?>