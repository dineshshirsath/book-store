<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SubCategoryController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('SubCategoryModel');
	}

	public function index()
	{	
		$data['subCategoryListingData'] = $this->SubCategoryModel->getSubCategoryListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.ADMIN_SUB_CATEGORY.'sub-category-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function add()
	{	
		$data['categoryData'] = $this->SubCategoryModel->getCategoryData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.ADMIN_SUB_CATEGORY.'add-sub-category', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insert()
	{	

		$this->form_validation->set_rules('category_id', 'Category name', 'required');
		$this->form_validation->set_rules('sub_category_name', 'Sub category name', 'required');
		$this->form_validation->set_rules('sub_category_description', 'Description', 'required');
		if ($this->form_validation->run() == FALSE) 
		{
			$data['categoryData'] = $this->SubCategoryModel->getCategoryData();
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.ADMIN_SUB_CATEGORY.'add-sub-category', $data);	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$data=$this->input->post();
			unset($data['submit']);
			
			if ($this->SubCategoryModel->insert($data)) 
			{
				$this->session->set_flashdata('success', 'Record added successfully.');
				redirect(base_url() . 'listing-sub-category');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not added.');
					//echo 'Not added record ';
				redirect(base_url() . 'listing-sub-category');
			}
		}		
	}

	public function updateView($id)
	{
		$data['subcategoryUpdatData'] = $this->SubCategoryModel->getUpdatsubCategoryData($id);
		$data['categoryData'] = $this->SubCategoryModel->getCategoryData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.ADMIN_SUB_CATEGORY.'edit-sub-category', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('category_id', 'Category name', 'required');
		$this->form_validation->set_rules('sub_category_name', 'Sub category name', 'required');
		$this->form_validation->set_rules('sub_category_description', 'Description', 'required');

		$id = $this->input->post('id');

		if ($this->form_validation->run() == FALSE) 
		{
			$data['subcategoryUpdatData'] = $this->SubCategoryModel->getUpdatsubCategoryData($id);
			$data['categoryData'] = $this->SubCategoryModel->getCategoryData();
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.ADMIN_SUB_CATEGORY.'edit-sub-category', $data);	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$data = $this->input->post();
			unset($data['submit']);

			if ($this->SubCategoryModel->update($data, $id)) 
			{
				$this->session->set_flashdata('success', 'Record updated successfully.');
				redirect(base_url() . 'listing-sub-category');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not updated.');
					//echo 'Not added record ';
				redirect(base_url() . 'listing-sub-category');		
			}
		}
	}

	public function delete($id)
	{
		if ($this->SubCategoryModel->delete($id)) 
		{
			$this->session->set_flashdata('success', 'Record deleted successfully.');
			redirect(base_url() . 'listing-sub-category');
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not deleted.');
				//echo 'Not added record ';
			redirect(base_url() . 'listing-sub-category');
		}
	}

	public function getSubCategoryById()
	{
		// POST data 
	    $postData = $this->input->post();
	        
	    // get data 
	    $data = $this->SubCategoryModel->getSubCategoryByCatID($postData);
	    echo json_encode($data); 
	}

}
?>