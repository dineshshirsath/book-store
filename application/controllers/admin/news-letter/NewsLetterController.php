<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class NewsLetterController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('NewsLetterModel');
	}

	public function index()
	{	
		$data['newsLetterListingData'] = $this->NewsLetterModel->getnewsLetterListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.NEWS_LETTER.'news-letter-subscribe-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function add()
	{	
		$data['newsLetterListingData'] = $this->NewsLetterModel->getnewsLetterListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.NEWS_LETTER.'send-news-letter-email', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insert()
	{
		$this->form_validation->set_rules('from', 'From', 'required');
		$this->form_validation->set_rules('to[]', 'To', 'required');
		$this->form_validation->set_rules('subject', 'Subject', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$data['newsLetterListingData'] = $this->NewsLetterModel->getnewsLetterListingData();
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.NEWS_LETTER.'send-news-letter-email', $data);	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$to = implode(', ',$this->input->post('to'));
			$data = array(
				'from' => $this->input->post('from'),
				'to' => $to,
				'subject' => $this->input->post('subject'),
				'message' => $this->input->post('message')
			);
			if ($this->NewsLetterModel->insert($data)) 
			{
				$this->session->set_flashdata('success', 'Email send successfully.');
				redirect(base_url() . 'listing-news-letter');
			}
			else
			{
				$this->session->set_flashdata('error', 'Email not send.');
					//echo 'Not added record ';
				redirect(base_url() . 'listing-news-letter');
			}
		}	
	}

	public function newsletterHistory()
	{
		$data['newsLetterHistoryListingData'] = $this->NewsLetterModel->getnewsLetterHistoryListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.NEWS_LETTER.'news-letter-history-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');	
	}

}
?>