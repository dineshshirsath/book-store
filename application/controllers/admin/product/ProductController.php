<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ProductController extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('CategoryModel');
		$this->load->model('SubCategoryModel');
		$this->load->model('ProductModel');
		$this->load->model('UserModel');
	}
	public function index()
	{
		$data['productListingData'] = $this->ProductModel->getProductListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.PRODUCT.'product-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');	
	}

	public function add()
	{	
		$data['CategoryData'] = $this->CategoryModel->getCategoryData();
		$data['authourData'] = $this->UserModel->getAuthorDataUsinSessionId();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.PRODUCT.'add-product',$data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insert()
	{
		if ($this->session->userdata('user_type') == 's') 
		{
			$this->form_validation->set_rules('title', 'Book Title', 'required');
			$this->form_validation->set_rules('authour', 'Authour', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('book_information', 'Book Information', 'required');
			$this->form_validation->set_rules('edition', 'Edition', 'required');
			$this->form_validation->set_rules('supc', 'SUPC', 'required');
			$this->form_validation->set_rules('book_discription', 'Book Description', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$data['authourData'] = $this->UserModel->getAuthorDataUsinSessionId();
				$data['CategoryData'] = $this->CategoryModel->getCategoryData();
				$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
				$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
				$this->load->view(ADMIN.PRODUCT.'add-product',$data);	
				$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
			}
			else
			{
				$price = 0;
				if (empty($this->input->post('price')) and $this->input->post('price') == '0'){
					$price = 499;
				}else{
					$price = $this->input->post('price');
				}
				$data = array(
					'addedby_id' => $this->session->userdata('user_id'),
					'category_id' => '2',
					'sub_category_id' => '1',
					'title' => $this->input->post('title'),
					'publisher' => 'Akulina Inovation Pvt. Ltd.',
					'authour' => $this->input->post('authour'),
					'price' => $price,
					'book_information ' => $this->input->post('book_information'),
					'edition' => $this->input->post('edition'),
					'supc' => $this->input->post('supc'),
					'book_discription' => $this->input->post('book_discription'),
					'image' => 'no_book_img.png',
					'books_have' => $this->input->post('books_have'),
					'status' => 'up',
					'addedby_status' => $this->session->userdata('user_type')
				);

				if ($this->ProductModel->insert($data)) 
				{
					//------ Stroe Logs --------
					$system_ip = get_client_ip();
		    	    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Add new book','bookName'=>$this->input->post('title'),'staus'=>'success');
		    	    $activity = json_encode($data);
		    	    logs($system_ip,$activity);
					//------ Stroe Logs --------

					$this->session->set_flashdata('success', 'Record added successfully.');
					redirect(base_url() . 'listing-product');
				}
				else
				{
					$this->session->set_flashdata('error', 'Record not added.');
					//echo 'Not added record ';
					redirect(base_url() . 'add-product');
				}
			}
		}
		else
		{
			$this->form_validation->set_rules('category_id', 'category name', 'required');
			$this->form_validation->set_rules('title', 'Book Title', 'required');
			$this->form_validation->set_rules('authour', 'Authour', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('book_information', 'Book Information', 'required');
			$this->form_validation->set_rules('edition', 'Edition', 'required');
			$this->form_validation->set_rules('supc', 'SUPC', 'required');
			$this->form_validation->set_rules('book_discription', 'Book Description', 'required');
			$this->form_validation->set_rules('status', 'Status', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$data['CategoryData'] = $this->CategoryModel->getCategoryData();
				$data['authourData'] = $this->UserModel->getAuthorDataUsinSessionId();
				$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
				$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
				$this->load->view(ADMIN.PRODUCT.'add-product',$data);	
				$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
			}else{
				
				$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/book/";
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['overwrite'] = FALSE;
				$config['encrypt_name'] = FALSE;
				$config['remove_spaces'] = TRUE;
				
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('image'))
				{
					$uploadData = $this->upload->data();
					$image = $uploadData['file_name'];
				}
				else
				{
					print_r($this->upload->display_errors());exit;
					$image = '';
				}

				$price = 0;
				if (empty($this->input->post('price')) and $this->input->post('price') == '0'){
					$price = 499;
				}else{
					$price = $this->input->post('price');
				}
				$data = array(
					'addedby_id' => $this->session->userdata('user_id'),
					'category_id' => $this->input->post('category_id'),
					'sub_category_id' => $this->input->post('sub_category_id'),
					'title' => $this->input->post('title'),
					'publisher' => 'Akulina Inovation Pvt. Ltd.',
					'authour' => $this->input->post('authour'),
					'price' => $price,
					'book_information ' => $this->input->post('book_information'),
					'edition' => $this->input->post('edition'),
					'supc' => $this->input->post('supc'),
					'book_discription' => $this->input->post('book_discription'),
					'image' => $image,
					'status' => $this->input->post('status'),
					'addedby_status' => $this->session->userdata('user_type')
				);

				if ($this->ProductModel->insert($data)) 
				{
					$this->session->set_flashdata('success', 'Record added successfully.');
					redirect(base_url() . 'listing-product');
				}
				else
				{
					$this->session->set_flashdata('error', 'Record not added.');
					//echo 'Not added record ';
					redirect(base_url() . 'add-product');
				}
			}
		}		
	}
	public function updateView($id)
	{
		$data['producUpdatData'] = $this->ProductModel->getUpdatProductData($id);
		$data['CategoryData'] = $this->CategoryModel->getCategoryData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.PRODUCT.'edit-product', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function update()
	{
		if ($this->session->userdata('user_type') == 's') 
		{
			$id = $this->input->post('id');
			$data = array(
				'addedby_id' => $this->input->post('addedby_id'),
				'title' => $this->input->post('title'),
				'authour' => $this->input->post('authour'),
				'price' => $this->input->post('price'),
				'book_information ' => $this->input->post('book_information'),
				'edition' => $this->input->post('edition'),
				'supc' => $this->input->post('supc'),
				'book_discription' => $this->input->post('book_discription'),
				'books_have' => $this->input->post('books_have'),
			);
			if ($this->ProductModel->update($data, $id)) 
			{
				//------ Stroe Logs --------
					$system_ip = get_client_ip();
				    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Update book','bookName'=>$this->input->post('title'),'staus'=>'success');
				    $activity = json_encode($data);
				    logs($system_ip,$activity);
				//------ Stroe Logs --------
				$this->session->set_flashdata('success', 'Record updated successfully');
				redirect(base_url() . 'listing-product');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not updated.');
				redirect(base_url() . 'edit-product/'.$id);
			}
		}
		else
		{
			$this->form_validation->set_rules('category_id', 'category name', 'required');
			$this->form_validation->set_rules('title', 'Book Title', 'required');
			$this->form_validation->set_rules('authour', 'Authour', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('book_information', 'Book Information', 'required');
			$this->form_validation->set_rules('edition', 'Edition', 'required');
			$this->form_validation->set_rules('supc', 'SUPC', 'required');
			$this->form_validation->set_rules('book_discription', 'Book Description', 'required');
			$id = $this->input->post('id');

			if ($this->form_validation->run() == FALSE)
			{
				$data['producUpdatData'] = $this->ProductModel->getUpdatProductData($id);
				$data['CategoryData'] = $this->CategoryModel->getCategoryData();
				$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
				$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
				$this->load->view(ADMIN.PRODUCT.'edit-product', $data);	
				$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
			}else{
				
				$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/book/";
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['overwrite'] = FALSE;
				$config['encrypt_name'] = FALSE;
				$config['remove_spaces'] = TRUE;
				
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('image'))
				{
					$uploadData = $this->upload->data();
					$image = $uploadData['file_name'];
				}
				else
				{
					print_r($this->upload->display_errors());exit;
					$image = '';
				}
				$id = $this->input->post('id');
				$data = array(
					'addedby_id' => $this->input->post('addedby_id'),
					'category_id' => $this->input->post('category_id'),
					'sub_category_id' => $this->input->post('sub_category_id'),
					'title' => $this->input->post('title'),
					'publisher' => $this->input->post('publisher'),
					'authour' => $this->input->post('authour'),
					'price' => $this->input->post('price'),
					'book_information ' => $this->input->post('book_information'),
					'edition' => $this->input->post('edition'),
					'supc' => $this->input->post('supc'),
					'book_discription' => $this->input->post('book_discription'),
					'image' => $image,
					'status' => $this->input->post('status')
				);

				if ($this->ProductModel->update($data, $id)) 
				{
					$this->session->set_flashdata('success', 'Record updated successfully');
					redirect(base_url() . 'listing-product');
				}
				else
				{
					$this->session->set_flashdata('error', 'Record not updated.');
					redirect(base_url() . 'edit-product/'.$id);
				}	
			}
		}			
	}

	public function delete($id)
	{
		$data = array(
			'delete_status' => '1'
		);

		if ($this->ProductModel->delete($id, $data)) 
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Delete the book','bookId'=>$id,'staus'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			$this->session->set_flashdata('success', 'Record deleted successfully');
			redirect(base_url() . 'listing-product');
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not deleted.');
			redirect(base_url() . 'listing-product');
		}
	}

	public function addPdfFile()
	{
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.PRODUCT.'add-pdf-file');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function insertPdfFile()
	{
		/*if ($this->form_validation->run() == FALSE) 
		{
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.PRODUCT.'add-pdf-file');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{	*/
			$this->load->library('upload');
			$filesCount = count($_FILES['book_pdf']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $_FILES['book_pdf']['name'][$i];
                $_FILES['file']['type']     = $_FILES['book_pdf']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['book_pdf']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['book_pdf']['error'][$i];
                $_FILES['file']['size']     = $_FILES['book_pdf']['size'][$i];
                
                // File upload configuration
                $uploadPath = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/PDF_File/";;
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'pdf';
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('file')){
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }
                $data[] = array(
					'addedby_id' => $this->session->userdata('user_id'),
					'name' => $this->input->post('name')[$i],
					'book_pdf' => $fileData['file_name'],
					'addedby_status' => $this->session->userdata('user_type')
				);
            }
			if ($this->ProductModel->insertPdfFile($data)) 
			{
				$this->session->set_flashdata('success', 'Record added successfully.');
				redirect(base_url() . 'listing-product');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not added.');
				//echo 'Not added record ';
				redirect(base_url() . 'listing-product');
			}
		//}	
		
	}
	
	private function set_upload_options()
	{   
	    //upload an image options
	    $config = array();
	    $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/PDF_File/";
	    $config['overwrite']     = FALSE;
		return $config;
	}

}
?>