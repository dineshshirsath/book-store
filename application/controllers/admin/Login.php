<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
		$this->load->model('website/RegisterModel');
		$this->load->library('form_validation');
		
	}
	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if($this->form_validation->run() == TRUE){
			if($this->Login_model->checkLogin())
			{
				if ($this->session->userdata('user_type') == 's') {
					if ($this->Login_model->checkSellerKYCDocument()) {
						redirect(base_url('admin-dashboard'));
					}else{
						redirect(base_url() . 'author-kyc-add?id='.$this->session->userdata('user_id'));
					}
				}else{
					redirect(base_url('admin-dashboard'));
				}
			}
			else{
				$this->session->set_flashdata('error','Enter a valid login details.');
				redirect(base_url('admin'));
			}
		}
		if(!$this->session->all_userdata())
		{
			redirect(base_url('admin-dashboard'));
		}
		$this->load->view(ADMIN.'login');	
	}

	public function forgotPasswordView()
	{
		$this->load->view(ADMIN.'forgot-password');	
	}

	public function forgotPassword()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view(ADMIN.'forgot-password');	
		}
		else
		{
			$email = $this->input->post('email');
			$result = $this->RegisterModel->forgotPassword($email,"author");
			if($result > 0)
			{
				$this->session->set_flashdata('success', 'Please check your registerd email we send forget password link on your email');
				redirect(base_url() . 'admin');
			}
			else
			{
				$this->session->set_flashdata('error', 'Failed!!!');
				redirect(base_url() .'admin-forgot-password');
			}
		}
	}

	public function logout()
	{
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'user_name' && $key != 'user_email' && $key != 'id') {
                $this->session->unset_userdata($key);
            }
        }
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}
	public function resetPassword()
	{
		die;
		if(!empty($_GET['id'])){
			$data['result'] = base64_decode($_GET['id']);
			if($data){
				$this->load->view(ADMIN.'reset-password',$data);	
			}else{
				echo "Not found!";
			}
					
		}
		if(!empty($_POST)){
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
			$this->form_validation->set_rules('confirm_password', 'confirm_password', 'trim|required|min_length[8]|matches[password]');
			
			$id = $this->input->post('userId');
			$password = $this->input->post('password');
			$result = $this->RegisterModel->updatePassword($id,$password);
			if($result > 0){
				$this->session->set_flashdata('success', 'Password Updated successfully,Please login with your new password.');
	    		redirect(base_url('admin-login'));
			}else{
				$this->session->set_flashdata('error', 'Password not updated...');
	    		redirect(base_url('author-reset-password')."?id=".base64_encode($id));
			}
		}	
			
	}
}
?>