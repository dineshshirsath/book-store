<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ShoppingCartController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('ShoppingCartModel');
	}

	public function index()
	{	
	}

	public function orderListing()
	{	
		$data['orderListingData'] = $this->ShoppingCartModel->getOrderListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SHOPPING_CART.'order-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function paymentListing()
	{	
		$data['paymentListingData'] = $this->ShoppingCartModel->getPaymentListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SHOPPING_CART.'payment-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function getPaymentDetails($id)
	{	
		$data['paymentProductDetailsData'] = $this->ShoppingCartModel->paymentProductDetails($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SHOPPING_CART.'payment-product-details', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function getSellProductListByAuthor()
	{	
		$data['sellProductList'] = $this->ShoppingCartModel->getSellProductList();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SELLER.'sell-product-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function getBookPurchaseUsers()
	{	
		$data['bookPurchaseUsers'] = $this->ShoppingCartModel->getBookPurchaseUsers();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.SHOPPING_CART.'book-purchase-users-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}
}
?>