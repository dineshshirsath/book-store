<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TermsConditionsController extends CI_Controller 
{
    function __construct() 
	{
		parent::__construct();
				
    }
	public function index()
	{
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'Terms-Conditions/terms-conditions');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');	
	}
	function ChangeStatus()
	{
		$this->form_validation->set_rules('Terms', 'Terms', 'required');
		if ($this->form_validation->run() == FALSE) 
		{
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.'Terms-Conditions/terms-conditions');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');	
		}
		else
		{
			$id = $this->input->post('Terms');
			$data['status'] = 1;
			$this->db->where('id',$id);
			$this->db->update('tbl_users',$data);
			redirect(base_url('admin-dashboard'));
		}
	}
}