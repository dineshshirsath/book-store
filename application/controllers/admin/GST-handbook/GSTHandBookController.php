<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class GSTHandBookController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('GSTHandBookModel');
	}

	public function index()
	{
		if($this->session->userdata('user_name')!='')
		{
			$book_id =  $_GET['id'];
			$data['bookResult'] = $this->GSTHandBookModel->getBookDetails($book_id);
			/*$this->load->view('template/header');	
			$this->load->view('template/menu');	*/
			$this->load->view(ADMIN.'GST-handbook/handbook',$data);	
			/*$this->load->view('template/footer');*/	
		}
		else
		{
			redirect(base_url() . 'login');
		}			
	}
	public function book_purchase_msg()
	{
		$this->load->view('template/header');		
		$this->load->view('pages/book_purchase_msg');	
		$this->load->view('template/footer');	
		
	}
}
?>