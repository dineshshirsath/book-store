<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ContactController extends CI_Controller 
{
    function __construct() 
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');		
    }
	public function index()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/contact');	
		$this->load->view('template/footer');	
	}
	function insertContactUs()
	{
		$this->form_validation->set_rules('first_name', 'Name', 'required');
		$this->form_validation->set_rules('last_name', 'Name', 'required');
		$this->form_validation->set_rules('phone', 'Number', 'required|numeric|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('message', 'Message', 'required');
        if ($this->form_validation->run() == FALSE)
        {
        	$this->load->view('template/header');	
			$this->load->view('template/menu');	
			$this->load->view('pages/contact');	
			$this->load->view('template/footer');
        }
        else
        {
            
            // load success template...
            $this->load->library('email');
            $email = $this->input->post('email');
    	    $name = $this->input->post('first_name').' '.$this->input->post('first_name');
    	    $phone = $this->input->post('phone');
    	    $subject ='You have a new Enquiry from thebooklounge.in';
    	    $message = $this->input->post('message');
    	    
    	    $config = Array(
    	        'mailtype' => 'html',
    	    );

    	    $this->email->initialize($config);
            $this->email->from($email, 'thebooklounge.in');
            
            
            $this->email->to('info@thebooklounge.in');
            $this->email->cc('manoj@thebooklounge.in,sandeep@thebooklounge.in');
                        
            $this->email->subject($subject);
            $this->email->message('<html lang="en">
				<head>
					<meta charset="utf-8">
					<title>The Book Lounge</title>
				</head>
				<body style="padding-top: 60px;padding-bottom: 40px;">
				    <div class="fixed-header" style=" width: 100%;position: fixed;background: #fff;padding: 10px 0;color: #030303;top: 0;">
				        <div class="container" style="width: 80%;margin: 0 auto;">
				            <nav>
				                <div class="col-md-3 content-logo">
				                   <h2 style="text-decoration:none;font-size: 33px;">The Book<span style="color: green;"> Lounge</span></h2>
				                </div>
				            </nav>
				        </div>
				        <hr>
				    </div>
				    <div class="container">
            			<table style="height:auto;width:50%;padding-top:10px;padding-left:100px;padding-bottom:30px;">
						   <tr style="padding-top:30px;">
						    <td><b>Name:-</td>
						    <td>'.$name.'</td>
						  </tr>
						  <tr style="padding-top:30px;">
						    <td><b>Email Id:-</td>
						    <td>'.$email.'</td>
						  </tr>
						  <tr style="padding-top:30px;">
						    <td><b>Phone No:-</td>
						    <td>'.$phone.'</td>
						  </tr>
						  <tr style="padding-top:30px;">
						    <td><b>Message:-</td>
						    <td>'.$message.'</td>
						  </tr>
						  <br><br><br><br><br><br>
						  <tr style="padding-top:30px;">
						    <td><b>Thanks & Regards,</b></td>
						  </tr>
						  <tr style="padding-top:30px;">
						    <td><a href="https://thebooklounge.in/" target="_blank">thebooklounge</a></td>
						  </tr>
					   </table>
				   </div>
            	</body>
            </html>');
            $this->email->send();
            
            $this->email->print_debugger();
        	
        	// load success template...
        
        
            $this->session->set_flashdata('success', 'Thank You For visiting our site!!!');
        	redirect(base_url('contact'));
            //echo "It's all Good!";
        } 
	}
}