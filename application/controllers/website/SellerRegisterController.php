<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SellerRegisterController extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('website/SellerRegisterModel');
	}

	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/menu');		
		$this->load->view('pages/sellerRagister');	
		$this->load->view('template/footer');	
	}

	public function insert()
	{	 
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_users.email]');
		$this->form_validation->set_rules('user_name', 'User Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('g-recaptcha-response', 'recaptcha validation', 'required|callback_validate_captcha');
		$this->form_validation->set_message('validate_captcha', 'Please check the the captcha form');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('template/header');
			$this->load->view('template/menu');		
			$this->load->view('pages/sellerRagister');	
			$this->load->view('template/footer');
		}
		else
		{  	
			if ($sellerId = $this->SellerRegisterModel->insert()) 
			{
			     $this->load->library('email');
		
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from('info@thebooklounge.in', 'The Book Lounge');
		$this->email->to($this->input->post('email'));
		
		$this->email->cc('info@thebooklounge.in,manoj@thebooklounge.in,sandeep@thebooklounge.in');
		
		$this->email->subject('Author Registration Successfully on The Book Lounge');
		$this->email->message('<html lang="en">
			<head>
			<meta charset="utf-8">
			<title>The Book Lounge</title>
			</head>
			<body style="padding-top: 60px;padding-bottom: 40px;">
			    <div class="fixed-header" style=" width: 100%;position: fixed;background: #fff;padding: 10px 0;color: #030303;top: 0;">
			        <div class="container" style="width: 80%;margin: 0 auto;">
			            <nav>
			                <div class="col-md-3 content-logo">
			                   <h2 style="text-decoration:none;font-size: 33px;">The Book<span style="color: green;"> Lounge</span></h2>
			                </div>
			            </nav>
			        </div>
			        <hr>
			    </div>
			    <div class="container">
			    	 <p>Respected '.$this->input->post('first_name').'<br><br>
					 Welcome To The Book Lounge</p>
					 <p>Thanks for taking a step forward for publishing your book on "The Book Lounge" <br><br>
					 We are pleased to welcome you as a new author of “The Book Lounge”. We feel honoured that you have chosen our platform for publicizing your book and we are eager to be there for your service.<br><br>
					 As you know, we carry everything asan author you may need. We would be happy to hear from you for any customization required on the Website.<br><br>
					 We would be happy to visit your office at anyconvenienttime to discuss. Just call us at 9594872502, or 9819726219 you can also come at our office any time between 10:00 a.m. and 7:00 p.m, Monday to Friday.<br><br>
					 Thank you again for your choice of "The Book Lounge" to publish your book.<br>
					 We look forward to a long and successful association.<br><br>
					 Thanks & Regards,<br>
					Thebooklounge Team</p>
			    </div>    
			    <div class="fixed-footer" style=" width: 100%;position: fixed;background: black;padding: 10px 0;color: white;bottom: 0; ">
			        <div class="container" style="margin-left: 20px;">&copy; <?php echo date("Y"); ?> All Rights Reserved by <a href="#" target="_blank" style="color: green;">FalconX.in</a>. Design by <a href="http://orbs-solutions.com/" target="_blank" style="color: green;"> Orbs Solutions</a></div>        
			    </div>
			</body>
			</html>');

				$this->email->send();
				// redirect(base_url() . 'author-kyc-add?id='.$sellerId);

				$data = array(
					'seller_id ' => $sellerId,
					'adhar_card' => 'adharcard.jpg',
					'pan_card' => 'pancard.jpg',
					'voter_card' => 'votercard.jpg',
					'status' => '0'
				);

				if ($this->SellerRegisterModel->KYCUpload($data)) 
				{
					redirect(base_url() . 'thank-you');
				}
			}
		}		
	}

	function validate_captcha() {
        $captcha = $this->input->post('g-recaptcha-response');
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LeYsHUUAAAAAN1qIik4srVuO19SbtCxElkQ1JuS&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR'];
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        
        if ($contents . 'success' == false) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

	public function KYCAdd()
	{
		$sellerId['sellerId'] = $_GET['id'];

		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/KYC_Upload', $sellerId);	
		$this->load->view('template/footer');
	}

	public function KYCUpload()
	{
			// $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/KYC_Document/";
			// 	$config['allowed_types'] = 'jpg|png|jpeg';
			// 	$config['overwrite'] = FALSE;
			// 	$config['encrypt_name'] = FALSE;
			// 	$config['remove_spaces'] = TRUE;
				
			// 	$this->load->library('upload', $config);
			// 	$this->upload->initialize($config);
			// 	if($this->upload->do_upload('adhar_card'))
			// 	{
			// 		$uploadData = $this->upload->data();
			// 		$adhar_card = $uploadData['file_name'];
			// 	}
			// 	else
			// 	{
			// 		print_r($this->upload->display_errors());exit;
			// 		$adhar_card = '';
			// 	}

			// $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/KYC_Document/";
			// 	$config['allowed_types'] = 'jpg|png|jpeg';
			// 	$config['overwrite'] = FALSE;
			// 	$config['encrypt_name'] = FALSE;
			// 	$config['remove_spaces'] = TRUE;
				
			// 	$this->load->library('upload', $config);
			// 	$this->upload->initialize($config);
			// 	if($this->upload->do_upload('pan_card'))
			// 	{
			// 		$uploadData = $this->upload->data();
			// 		$pan_card = $uploadData['file_name'];
			// 	}
			// 	else
			// 	{
			// 		print_r($this->upload->display_errors());exit;
			// 		$pan_card = '';
			// 	}

			// $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/KYC_Document/";
			// 	$config['allowed_types'] = 'jpg|png|jpeg';
			// 	$config['overwrite'] = FALSE;
			// 	$config['encrypt_name'] = FALSE;
			// 	$config['remove_spaces'] = TRUE;
				
			// 	$this->load->library('upload', $config);
			// 	$this->upload->initialize($config);
			// 	if($this->upload->do_upload('voter_card'))
			// 	{
			// 		$uploadData = $this->upload->data();
			// 		$voter_card = $uploadData['file_name'];
			// 	}
			// 	else
			// 	{
			// 		print_r($this->upload->display_errors());exit;
			// 		$voter_card = '';
			// 	}	

			// 	$data = array(
			// 		'seller_id ' => $this->input->post('seller_id'),
			// 		'adhar_card' => $adhar_card,
			// 		'pan_card' => $pan_card,
			// 		'voter_card' => $voter_card
			// 	);

			// 	if ($this->SellerRegisterModel->KYCUpload($data)) 
			// 	{
			// 		redirect(base_url() . 'admin');
			// 	}
		
	}
	public function thankYou()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/thank_you');	
		$this->load->view('template/footer');
	}
}
?>