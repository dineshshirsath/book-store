<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BillingDetailController extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('website/BillingDetailModel');
		$this->load->model('website/CartModel');
	}
	public function insert()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('message', 'order notes', 'required');
		
        if ($this->form_validation->run() == FALSE)
        {
        	$this->load->view('template/header');	
			$this->load->view('template/menu');	
			$this->load->view('pages/check_out');	
			$this->load->view('template/footer');
        }
        else
        {
        	if ($cart = $this->CartModel->getAddToCatData()):
		    	$i = 1;
			    foreach ($cart as $item):
	        		$product[] = $item['id'];
	        	endforeach;
        	endif;
        	if(!empty($product)){
				if($this->session->userdata('user_id') !=''){
					$result = $this->BillingDetailModel->insertBillingData($product);
		        	if($this->db->affected_rows() > 0 )
		        	{
		        		$this->session->set_flashdata('success', 'Order Placed Successfully!!!');
		        		redirect(base_url('check_out?orderId='.base64_encode($result)));
					}// load success template...
		            else{
		            	$this->session->set_flashdata('error', ' Failed!!!');
		        		redirect(base_url('check_out'));
					}
				}else{
					$this->session->set_flashdata('login','login');
		        	redirect(base_url('login'));
				}
			}else{
				$this->session->set_flashdata('error', ' Sorry your cart is empty!!!');
		        redirect(base_url('check_out'));
			}
			
        } 
		//$this->RegisterModel->insert();
	}
	
}
