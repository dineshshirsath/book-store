<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('website/WebUserProfileModel');
		if(empty($this->session->userdata('user_id'))){redirect(base_url('login'));} 
	}

	public function index()
	{	
		$data['userData'] = $this->WebUserProfileModel->getProfileData();
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/web-user-profile/web-user-profile',$data);	
		$this->load->view('template/footer');
	}
	public function updateView()
	{	
		$data['userData'] = $this->WebUserProfileModel->getProfileData();
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/web-user-profile/web-user-profile-edit',$data);	
		$this->load->view('template/footer');
	}
	public function update()
	{
		$id = $this->input->post('id');
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'address' => $this->input->post('address'),
			'mobile' => $this->input->post('mobile'),
			'gender' => $this->input->post('gender')
		); 

		if ($this->WebUserProfileModel->update($id, $data)) 
		{
			$this->session->set_flashdata('success', 'Record update successfully.');
				redirect(base_url() . 'web-user-profile-edit');
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not update.');
				//echo 'Not added record ';
			redirect(base_url() . 'web-user-profile-edit');
		}
	}

	public function changePassword()
	{	
		$data['userData'] = $this->WebUserProfileModel->getProfileData();
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/web-user-profile/web-user-change-password',$data);	
		$this->load->view('template/footer');
	}

	public function changePasswordAdd()
	{
		$this->form_validation->set_rules('new_pwd', 'New Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required|min_length[8]|matches[new_pwd]');

		if ($this->form_validation->run() == FALSE) 
		{
			$data['userData'] = $this->WebUserProfileModel->getProfileData();
			$this->load->view('template/header');	
			$this->load->view('template/menu');	
			$this->load->view('pages/web-user-profile/web-user-change-password',$data);	
			$this->load->view('template/footer');
		}
		else
		{
			$id = $this->input->post('id');
			$data = array(
				'password' => $this->input->post('new_pwd')
			);
			
			if ($this->WebUserProfileModel->changePassword($id, $data)) 
			{
				$this->session->set_flashdata('success', 'Change password successfully.');
					redirect(base_url() . 'web-user-change-password');
			}
			else
			{
				$this->session->set_flashdata('error', 'Password not change.');
					//echo 'Not added record ';
				redirect(base_url() . 'web-user-change-password');
			}
		}
	}

	public function changeProfileImage()
	{
		$id = $this->input->post('id');
		$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/profile_Image/";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['overwrite'] = FALSE;
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if($this->upload->do_upload('image'))
		{
			$uploadData = $this->upload->data();
			$image = $uploadData['file_name'];
		}
		else
		{
			print_r($this->upload->display_errors());exit;
			$image = '';
		}
		$data = array(
			'image' => $image
		);

		if ($this->WebUserProfileModel->changeProfileImageUpdate($id, $data)) 
		{
			$this->session->set_flashdata('success', 'Profile upload successfully.');
			redirect(base_url() . 'web-user-profile');
		}
		else
		{
			$this->session->set_flashdata('error', 'Profile not upload.');
			//echo 'Not added record ';
			redirect(base_url() . 'web-user-profile');
		}
	}

}
?>