<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RegisterController extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('website/RegisterModel');
		$this->load->helper('url');
	}
	public function index()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/register');	
		$this->load->view('template/footer');	
	}
	public function insert()
	{
	    
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('user_name', 'User Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('gender', 'gender', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('mobile', 'Number', 'required|numeric|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('g-recaptcha-response', 'recaptcha validation', 'required|callback_validate_captcha');
		$this->form_validation->set_message('validate_captcha', 'Please check the the captcha form');
		if($this->form_validation->run() == FALSE)
        {
        	$this->load->view('template/header');	
        	$this->load->view('template/menu');	
			$this->load->view('pages/register');	
			$this->load->view('template/footer');	
	    }
	    else{
			
	    	$result = $this->RegisterModel->insertRegistrationData();
	    	if($this->db->affected_rows() > 0 )
	    	{
	    		$this->load->library('email');
		
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from('info@thebooklounge.in', 'The Book Lounge');
		$this->email->to($this->input->post('email'));
		
		$this->email->cc('info@thebooklounge.in,manoj@thebooklounge.in,sandeep@thebooklounge.in');
		
		$this->email->subject('Reader Registration Successfully on The Book Lounge');
		$this->email->message('<!DOCTYPE html>
			<html lang="en">
			<head>
			<meta charset="utf-8">
			<title>The Book Lounge</title>

			</head>
			<body style="padding-top: 60px;padding-bottom: 40px;">
			    <div class="fixed-header" style=" width: 100%;position: fixed;background: #fff;padding: 10px 0;color: #030303;top: 0;">
			        <div class="container" style="width: 80%;margin: 0 auto;">
			            <nav>
			                <div class="col-md-3 content-logo">
			                   <h2 style="text-decoration:none;font-size: 33px;">The Book<span style="color: green;"> Lounge</span></h2>
			                </div>
			            </nav>
			        </div>
			        <hr>
			    </div>
			    <div class="container">
			    	<p>Hi '.$this->input->post('first_name').',<br><br> Welcome To The Book Lounge</p>
					<p>Thanks for taking a step for staying updated.<br><br>
					We are pleased to welcome you as a new reader of The Book lounge. We feel honoured that you have chosen our platform to fill your knowledge needs, and we are eager to serve you.<br><br>
					As you know, we carry to keep all books published on our portal updated. We have a great variety of tax and law books to choose from, all at competitive prices. We have a complete line of Tax and Law books for Tax and Law professional like you.<br><br>
					We would be happy to hear from you at your convenient time to discuss your suggestion. Just call us at 9594872502, or 9819726219 drop in any time between 9:00 a.m. and 6:00 p.m., Monday to Friday.<br><br>
					Thank you again for choosing The Book Lounge to fill your knowledge thirst. <br>
					We look forward to a long and successful reading hour.<br><br>
					Thanks & Regards,<br>
					Thebooklounge Team
					</p>
			    </div>    
			    <div class="fixed-footer" style=" width: 100%;position: fixed;background: black;padding: 10px 0;color: white;bottom: 0; ">
			        <div class="container" style="margin-left: 20px;">&copy; <?php echo date("Y"); ?> All Rights Reserved by <a href="#" target="_blank" style="color: green;">FalconX.in</a>. Design by <a href="http://orbs-solutions.com/" target="_blank" style="color: green;"> Orbs Solutions</a></div>        
			    </div>
			</body>
			</html>                ');

					$this->email->send();
	    		$this->session->set_flashdata('success', 'Register successfully!!!');
	    		redirect(base_url('login'));
			}
	        else{
	        	$this->session->set_flashdata('error', 'User Registration Failed!!!');
	    		redirect($_SERVER['HTTP_REFERER']);
			}
		}
    } 

    function validate_captcha() {
        
        $captcha = $this->input->post('g-recaptcha-response');
        
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LeYsHUUAAAAAN1qIik4srVuO19SbtCxElkQ1JuS&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR'];
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $contents = curl_exec($ch);
        
        if ($contents . 'success' == false) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
	function login()
	{
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == FALSE)
        {
			$this->load->view('template/header');	
			$this->load->view('template/menu');	
			$this->load->view('pages/welcome_message');	
			$this->load->view('template/footer');
		}else{
			$result = $this->RegisterModel->checkLogin();
        	if($this->db->affected_rows() > 0 )
        	{
        		$this->session->set_flashdata('success', 'Login successfully!!!');
        			redirect(base_url());	
			}// load success template...
            else{
            	
				$this->session->set_flashdata('error', 'Login Failed!!!');
        		redirect( $_SERVER['HTTP_REFERER']);
			}
		}
	}
	function logout()
	{
	 	session_destroy();
	 	redirect(base_url('login'));
	}

	public function forgotPasswordView()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/forgot-password');	
		$this->load->view('template/footer');
	}

	public function forgotPasswordAdd()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('template/header');	
			$this->load->view('template/menu');	
			$this->load->view('pages/forgot-password');	
			$this->load->view('template/footer');
		}
		else
		{
			$email = $this->input->post('email');
			$result = $this->RegisterModel->forgotPassword($email);
			if($result > 0){
				$this->session->set_flashdata('success', 'Please check your registerd email we send forget password link on your email');
				redirect(base_url('forgat-password-view'));
			}else{
				$this->session->set_flashdata('error', 'Failed!!!');
				redirect(base_url('forgat-password-view'));
			}
		}
	}
}
?>
