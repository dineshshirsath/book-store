<?php

/**
 * 
 */
class  RatingReviewController extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('website/RatingReviewModel');
		$this->load->model('CommonFunctionModel');
	}
	public function index()
	{
		$user_id = $this->session->userdata('user_id');
		$data = array(
			'book_id' => $_POST['book_id'],
			'rating' => $_POST['rating'],
			'user_id' => $user_id
			
		);
		if ($this->RatingReviewModel->saveData($data)) {
			$s = 'suc';
			return redirect(base_url().'handbook?id='.$_POST['book_id'].'&msg='.$s);
		}else{
			$f = 'fal';
			return redirect(base_url().'handbook?id='.$_POST['book_id'].'&msg='.$f);
		}
		 
	}

	public function addLikeDislikeData()
	{
		$reviewId = $this->input->post('review_id');
		$userId = $this->input->post('user_id');
		$like_dislike = $this->input->post('like_dislike');
		$data = $this->input->post();

		if ($exists = $this->RatingReviewModel->getLikeDislikeExists($reviewId, $userId)) {
			$id = $exists[0]['id'];
			if ($this->RatingReviewModel->updataLikeDislike($id, $data))
			{
					$data['countLike'] = $this->CommonFunctionModel->getCountLikeDislike($reviewId, '1');
					$data['likeColor'] = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($reviewId, '1');

					//echo json_encode($data);

					$data['countDisLike'] = $this->CommonFunctionModel->getCountLikeDislike($reviewId, '0');
					$data['$disLikeColor'] = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($reviewId, '0');
	
					echo json_encode($data);

			}
		}else{
			if ($this->RatingReviewModel->insertLikeDislike($data))
			{
				$data['countLike'] = $this->CommonFunctionModel->getCountLikeDislike($reviewId, '1');
				$data['likeColor'] = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($reviewId, '1');

				//echo json_encode($data);

				$data['countDisLike'] = $this->CommonFunctionModel->getCountLikeDislike($reviewId, '0');
				$data['$disLikeColor'] = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($reviewId, '0');

				echo json_encode($data);
			}		
		}
	}

	public function addReview()
	{
		$data['bookId'] = $_GET['bookId'];
		$this->load->view('template/header');
		$this->load->view('template/menu');		
		$this->load->view('pages/add-review', $data);	
		$this->load->view('template/footer');
	}
	
	public function storeReview()
	{
		$data = $this->input->post();
		unset($data['submit']);
		if ($this->RatingReviewModel->addReview($data)) 
		{
			$this->session->set_flashdata('success', 'Add Review successfully!!!');
	    		redirect(base_url('product_detail?id='.$this->input->post('book_id')));
		}
		else
		{
			$this->session->set_flashdata('error', 'Add Review Failed!!!');
	    		redirect(base_url('product_detail?id='.$this->input->post('book_id')));
		}
	}

	public function allReview()
	{
		$data['bookId'] = $_GET['bookId'];
		$this->load->view('template/header');
		$this->load->view('template/menu');		
		$this->load->view('pages/all-review',$data);	
		$this->load->view('template/footer');	
	}
}
?>