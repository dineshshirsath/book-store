<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CartController extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('cart');
		$this->load->model('website/CartModel');
	}
	public function index()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/cart');	
		$this->load->view('template/footer');	
	}
	function add()
	{
		$insert_data = array(
			'id' => $this->input->post('id'),
			'name' => "abc",
			'price' => $this->input->post('price'),
			'qty' => 1,
			'image'=>$this->input->post('product_img'),
		);	

		//------ Stroe Logs --------
			$system_ip = get_client_ip();
		    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Add to cart','bookId'=>$this->input->post('id'),'staus'=>'success');
		    $activity = json_encode($data);
		    logs($system_ip,$activity);
		//------ Stroe Logs --------	

        // This function add items into cart.
		$this->cart->insert($insert_data);
	    // This will show insert data in cart.
		redirect(base_url('cart'));
	}
	function remove($rowid) {
        // Check rowid value.
		if ($rowid==="all"){
            // Destroy data which store in  session.
			$this->cart->destroy();
		}else{
            // Destroy selected rowid in session.
			$data = array(
				'rowid'   => $rowid,
				'qty'     => 0
			);
            // Update cart data, after cancle.
			$this->cart->update($data);
		}
		// This will show cancle data in cart.
		redirect(base_url('cart'));
	}
	function updateCart(){
        // Recieve post values,calcute them and update
        $cart_info =  $_POST['cart'] ;
 		foreach( $cart_info as $id => $cart)
		{	
             $rowid = $cart['rowid'];
             $price = $cart['price'];
             $amount = $price * $cart['qty'];
             $qty = $cart['qty'];
             $data = array(
				'rowid'   => $rowid,
                'price'   => $price,
                'amount' =>  $amount,
				'qty'     => $qty
			);
			$this->cart->update($data);
		}
		redirect(base_url('cart'));        
	}
}
?>