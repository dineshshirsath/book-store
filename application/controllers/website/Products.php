<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	function  __construct() {
        parent::__construct();
        $this->load->library('paypal_lib');
        
    }

    function buy(){
        //Set variables for paypal form
        $returnURL = base_url().'website/paypal/success'; //payment success url
        $cancelURL = base_url().'website/paypal/cancel'; //payment cancel url
        $notifyURL = base_url().'website/paypal/ipn'; //ipn url
        //get particular product data
        //$product = $this->product->getRows($id);
        $userID = $this->session->userdata('user_id'); //current user id
        $logo = base_url().'assets/images/codexworld-logo.png';

        
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $cart = array();//$this->cart->contents();
        $grand_total = 0;
        foreach ($cart as $item):
        	$grand_total = $grand_total + $item['subtotal'];
	        $this->paypal_lib->add_field('item_name', $item['name']);
	        $this->paypal_lib->add_field('custom', $userID);
	        $this->paypal_lib->add_field('item_number',  $item['id']);
	        $this->paypal_lib->add_field('amount',  $grand_total); 
	    endforeach;
        
        $this->paypal_lib->image($logo);
        
        $this->paypal_lib->paypal_auto_form();
    }
}
