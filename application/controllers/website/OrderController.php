<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
  class OrderController extends CI_Controller
  {
  	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$OrderListing['OrderListing'] = $this->CommonFunctionModel->getOrderList($this->session->userdata('user_id'));
		$this->load->view('template/header');	
		$this->load->view(WEBSITE.PAGES.'orders',$OrderListing);	
		$this->load->view('template/footer');	
	}
	function add()
	{
	//print_r($_POST);
	//	die;
        // Set array for send data.
		$insert_data = array(
			'id' => $this->input->post('id'),
			'name' => $this->input->post('name'),
			'price' => $this->input->post('price'),
			'qty' => $this->input->post('qty'),
			'image'=>$this->input->post('product_img'),
		);		

        // This function add items into cart.
		$this->cart->insert($insert_data);
	    // This will show insert data in cart.
		redirect(base_url('cart'));
	}
	function remove($rowid) {
        // Check rowid value.
		if ($rowid==="all"){
            // Destroy data which store in  session.
			$this->cart->destroy();
		}else{
            // Destroy selected rowid in session.
			$data = array(
				'rowid'   => $rowid,
				'qty'     => 0
			);
            // Update cart data, after cancle.
			$this->cart->update($data);
		}
		// This will show cancle data in cart.
		redirect(base_url('cart'));
	}
	function updateCart(){
        // Recieve post values,calcute them and update
        $cart_info =  $_POST['cart'] ;
 		foreach( $cart_info as $id => $cart)
		{	
             $rowid = $cart['rowid'];
             $price = $cart['price'];
             $amount = $price * $cart['qty'];
             $qty = $cart['qty'];
             $data = array(
				'rowid'   => $rowid,
                'price'   => $price,
                'amount' =>  $amount,
				'qty'     => $qty
			);
			$this->cart->update($data);
		}
		redirect(base_url('cart'));        
	}

  }  
?>