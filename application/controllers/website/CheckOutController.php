<?php

class CheckOutController extends CI_Controller
{
	function __construct() 
	{
		parent::__construct();	
		$this->load->model('website/CartModel');
		$this->load->library('cart');
    }
	function index()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/check_out');	
		$this->load->view('template/footer');	
	}
   
   
}

?>
