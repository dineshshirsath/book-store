<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ResetPasswordController extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('website/RegisterModel');
	}
	public function index()
	{
		if(!empty($_GET['id'])){
			$data['result'] = base64_decode($_GET['id']);
			if($data){
				$this->load->view('template/header');	
				$this->load->view('template/menu');	
				$this->load->view('pages/reset-password',$data);	
				$this->load->view('template/footer');
			}else{
				echo "Not found!";
			}
					
		}
		if(!empty($_POST)){
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
			$this->form_validation->set_rules('confirm_password', 'confirm_password', 'trim|required|min_length[8]|matches[password]');
			
			$id = $this->input->post('userId');
			$password = $this->input->post('password');
			$result = $this->RegisterModel->updatePassword($id,$password);
			if($result > 0){
				$this->session->set_flashdata('success', 'Password Updated successfully,Please login with your new password.');
	    		redirect(base_url('login'));
			}else{
				$this->session->set_flashdata('error', 'Password not updated...');
	    		redirect(base_url('reset-password')."?id=".base64_encode($id));
			}
		}	
	}
}
?>
