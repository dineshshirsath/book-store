<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SubscribeNewsLetterController extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('website/SubscribeNewsLetterModel');
    }
    public function index()
    {
        $data = $this->input->post();
        unset($data['subscribe']);
        if ($this->SubscribeNewsLetterModel->insertData($data))
        {
            $this->session->set_flashdata('success','Thank you for subscribing successfully!!!');
            redirect(base_url());
        }
    }
}
