<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Payumoney class this is a sample code to integrate payumoney in codeigniter 3
*/
class Payumoney extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('website/PayUMoneyModel');
	}
	//Method that handle when the payment was successful
	public function success(){
		
		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$salt = "e5iIg1jwi8";
		$sno = $_POST["udf1"];
		$product = json_decode($productinfo);
		
		$retHashSeq = $salt.'|'.$status.'||||||||||'.$sno.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

		$hash = strtolower(hash("sha512", $retHashSeq));

		if ($hash != $posted_hash) {
	       $this->session->set_flashdata('error', "An Error occured while processing your payment. Try again..");
		}

		else{
			$PayUMoneyData = array(
				'user_id' => $this->session->userdata('user_id'),
				'trans_id'=> $txnid,
				'order_id'=> 1,
				'amount' => $amount,
				'payment_status' => $status,
				'hash' => $posted_hash,
				'name_on_card' => $_POST['name_on_card'],
				'mihpayid' => $_POST['mihpayid']
			);
			
			$this->PayUMoneyModel->addPaymentDetail($PayUMoneyData);
			$productId = $this->db->insert_id();
			foreach($product as $row){
				$PayUMoneyTransData[] = array(
					'product_id'=>$row->name,
					'payment_id'=>$productId,
					'qty'=>$row->qty,
					'price_per_product'=>$row->value
				);
			}
			print_r($PayUMoneyTransData);
			$this->PayUMoneyModel->addPayUMoneyTransData($PayUMoneyTransData);
			$this->session->set_flashdata('success', "Payment was successful..");
		}
		$this->cart->destroy();
		unset($_POST);
		redirect('check_out');
	}

	//Method that handles when payment was failed
	public function error(){
		unset($_POST);
		$this->session->set_flashdata('error', "Your payment was failed. Try again..");
		redirect('check_out');
	}

	//Method that handles when payment was cancelled.
	public function cancel(){
		unset($_POST);
		$this->session->set_flashdata('error', "Your payment was cancelled. Try again..");
		redirect('check_out');
	}
}