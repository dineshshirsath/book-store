<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('ProductModel');
	}
	public function index()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('welcome_message');	
		$this->load->view('template/footer');	
	}
	
	public function ProductDetail()
	{
		if($this->session->userdata('user_name')!='')
		{
			$productId['id'] = $_GET['id'];
			$this->load->view('template/header');	
			$this->load->view('template/menu');	
			$this->load->view('pages/product_detail',$productId);	
			$this->load->view('template/footer');	
		}
		else
		{
			redirect(base_url() . 'login');
		}
		
	}
	
	public function about()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/about');	
		$this->load->view('template/footer');	
	}
	public function login()
	{
		$this->load->view('template/header');	
		$this->load->view('template/menu');	
		$this->load->view('pages/login');	
		$this->load->view('template/footer');	
	}
}
?>