 <!--Menu-->
    <header>
        <div id="top" class="container box-logo-menu">
            <div class="row">
                <nav class="col-md-12 navigation-menu">
                    <div class="col-md-3 content-logo">
                    <!--<img src="<?php echo base_url();?>assets/image/book/site/logo.png"  width="100%">-->
                        <a href="<?php echo base_url();?>" style="color: black;text-decoration:none;" alt="The Book Lounge" title="The Book Lounge"><h2>The Book<span> Lounge</span></h2></a>
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false" aria-controls="nav-collapse1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-8 mainmenu">
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="menu-first-level">
                                <li><a href="<?php echo base_url();?>" alt="Home" title="Home">Home</a></li>
                                <li><a href="<?php echo base_url();?>about" alt="About Us" title="About Us">About Us</a></li>
                                <li><a href="<?php echo base_url();?>contact" alt="Contact" title="Contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                 <!-- Top Cart -->
                    <div class="col-md-1 box-shoppingCart">
                        <div class="list-product dropdown content-count-cart-header" auto-close="disabled">
                            <div class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo base_url();?>assets/image/book/basket.png" alt="Add To Cart" title="Add To Cart">
                                <p class="number-prod-cart">
                                    <span class="fa-stack fa-3x">
                                   <?php $cart = $this->CommonFunctionModel->getAddToCatData();?>
                                        <strong class="fa-stack-1x fa-stack-text fa-inverse"><?php echo count($cart); ?></strong>
                                    </span>
                                </p>
                            </div>
                            <div id="dropdown" class="dropdown-menu vertical-li dropdown-prod-cart content" role="menu">
                                <ul>
                                <?php 
								    $grand_total = 0;
								    if ($cart = $this->CommonFunctionModel->getAddToCatData()):?>
								    <?php
								    $i = 1;
								    foreach ($cart as $item):
								    	$product = $this->CommonFunctionModel->getProductUsingId($item['id']);
									?>
                                    <li class="box-li-cart">
                                        <div class="col-xs-3 col-md-3 box-cart-book"><img src="<?php echo base_url();?>assets/image/book/<?php echo $item['image']; ?>"></div>
                                        <div class="col-xs-8 col-md-8 minicart-data">
                                            <p class="mc-name"><?php echo $product->title; ?></p>
                                            <p><?php echo $item['price']; ?></p>
                                        </div>
                                        <div class="btn-remove-prod"><a href="<?php echo base_url() ?>remove-cart/<?php echo $item['rowid'];?>" alt="Remove Cart" title="Remove Cart"><i class="fa fa-times" aria-hidden="true"></i></a></div>
                                    </li>
                                <?php 
                                $grand_total=$grand_total+$item['subtotal'];
                                 endforeach; ?>
                                </ul>
                                <div class="separator"></div>
                                <div id="cartInfo"><p class="minucart-price  ">Total &#8377;<?php echo $grand_total; ?></p></div>
                                <a href="<?php echo base_url();?>check_out" id="goToCheckOut" alt="Proceed to purchase" title="Proceed to purchase">Proceed to purchase</a>
                                <?php endif;?>

                            </div>
                        </div>
                    </div>
                </nav>

            </div>
        </div>
    </header>