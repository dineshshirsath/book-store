<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8">
	  	<link rel="icon" href="<?php echo base_url();?>favicon-32x32.png" type="image/x-icon" sizes="32x32">
	  	<title>The Book Lounge</title>
	  	<!-- Tell the browser to be responsive to screen width -->
	  	<meta name="description" content="Thebooklounge.in is ebook publication which has unique feature in which author will be given access to update book on real time basis to keep readers update.">
    <meta name="keywords" content="Thebooklounge.in, gstaudit ebooks for Readers & Authors,law ebooks for Readers & Authors, update book on real-time basis, Special offer for icai and icsi members,icai ebooks,icsi ebooks.">
	  	<!-- Bootstrap 3.3.7 -->
	  	<link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
	 	<!-- Font Awesome -->
	  	<link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
	  	<!-- Ionicons -->
	  	<link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
	  	<!-- Theme style -->
	  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
	  	<!-- iCheck -->
	  	<link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/plugins/iCheck/square/blue.css">
	  	<!-- Google Font -->
	  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	</head>
	<body class="hold-transition" style="background-image: url(<?php echo base_url(); ?>assets/image/book.jpg);background-size: 100% 100%;">
		<div class="login-box">
			  <div class="login-logo">
			    	<a href="../../index2.html"><b>Author</b>Login</a>
			  </div>
		 	  <!-- /.login-logo -->
			  <div class="login-box-body">
				    <p class="login-box-msg">Reset Password</p>
					<?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('success'); ?>
					</div>

					<?php } else if($this->session->flashdata('error')){  ?>

					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('error'); ?>
					</div>

					<?php } ?>

				    <form action="<?php echo base_url(); ?>forgot-password" method="post">
				    <input type="hidden" name="userId" value="<?php echo $result; ?>"/>
					      <!--<div class="form-group has-feedback">
					        	<input type="text" class="form-control" name="email" placeholder="Email">
					        	<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
								<span style="color: red;"><?php echo form_error('email');?></span>
					      </div>-->
					      <div class="form-group has-feedback">
						        <input type="password" class="form-control" name="password" placeholder="New Password">
						        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
								<span style="color: red;"><?php echo form_error('password');?></span>
					      </div>
					      <div class="form-group has-feedback">
						        <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
						        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
								<span style="color: red;"><?php echo form_error('confirm_password');?></span>
					      </div>
					      <div class="row">
						        <div class="col-xs-6">
						        </div>
					        	<!-- /.col -->
						        <div class="col-xs-6">
						          	<button type="submit" class="btn btn-primary btn-block btn-flat">Send</button>
						        </div>
					        	<!-- /.col -->
					      </div>
				    </form>
				    <div class="social-auth-links text-center">
				      
				    </div>
				    <!-- /.social-auth-links -->
				    <a href="<?php echo base_url(); ?>admin" class="text-center">Sign In</a>

			  </div>
		  <!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->
		<!-- jQuery 3 -->
		<script src="https://adminlte.io/themes/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="https://adminlte.io/themes/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- iCheck -->
		<script src="https://adminlte.io/themes/AdminLTE/plugins/iCheck/icheck.min.js"></script>
		<script>
		  $(function () {
		    $('input').iCheck({
		      checkboxClass: 'icheckbox_square-blue',
		      radioClass: 'iradio_square-blue',
		      increaseArea: '20%' /* optional */
		    });
		  });
		</script>
	</body>
</html>
