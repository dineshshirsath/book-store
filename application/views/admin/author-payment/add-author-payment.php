<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css"/>
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-author-payment">Author Payment Listing</a></li>
	        <li class="active">Add Author Payment</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 	<div class="col-md-12">
				        <?php if($this->session->flashdata('success')){ ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } else if($this->session->flashdata('error')){  ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('error'); ?>
						</div>
						<?php } ?>
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Add Author Payment</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url(); ?>store-author-payment" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
<div class="col-md-6">
	<div class="form-group">
		<label class="control-label">Author Name</label>
		<select name="author_id" id="author_id" class="form-control">
			<option value="">-----Select Author Name-----</option>
			<?php foreach ($authorData as $row): ?>
			<option value="<?php echo $row['id'] ?>"><?php echo $row['first_name']." ".$row['last_name'] ?></option>
			<?php endforeach ?>
		</select>
		<span style="color: red;"><?php echo form_error('author_id');?></span>
	</div>
</div>
									<div class="col-md-6">
					                	<div class="form-group">
					                  		<label class="control-label">Receive By</label>
											<input type="text" class="form-control" name="received_by" id="received_by" value="<?php echo set_value("received_by"); ?>" placeholder="Receive By">
											<span style="color: red;"><?php echo form_error('received_by');?></span>
					                	</div>
						            </div>
						            <div class="col-md-6">
					                	<div class="form-group">
					                  		<label class="control-label">Paid By</label>
											<input type="text" class="form-control" name="paid_by" id="paid_by" value="<?php echo set_value("paid_by"); ?>" placeholder="Paid By">
											<span style="color: red;"><?php echo form_error('paid_by');?></span>
					                	</div>
						            </div>
<div class="col-md-6" style="margin-bottom: 16px;">
  	<label class="control-label">Amount</label>
	<div class="input-group">
		<span class="input-group-addon">&#8377;</span>
		<input type="text" class="form-control" name="pay_amount" id="pay_amount" value="<?php echo set_value("pay_amount"); ?>" placeholder="Amount">
        <span class="input-group-addon">.00</span>
	</div>
	<span style="color: red;"><?php echo form_error('pay_amount');?></span>
</div>


						            <div class="col-md-6">
					                	<div class="form-group">
											<label class="control-label">Payment Type</label>
											<select name="pay_type" id="payType" class="form-control">
												<option value="">-----Select Payment Type -----</option>
												<option value="1">Cash</option>
												<option value="2">Chaque</option>
												<option value="3">Bank Transaction</option>
											</select>
											<span style="color: red;"><?php echo form_error('pay_type');?></span>
					                	</div>
						            </div>
<div class="col-md-6" id="chequeNo" style="display: none;">
	<div class="form-group">
  		<label class="control-label">Cheque No.</label>
		<input type="text" class="form-control" name="cheque_no" id="cheque_no" value="<?php echo set_value("cheque_no"); ?>" placeholder="Cheque Number">
		<span style="color: red;"><?php echo form_error('cheque_no');?></span>
	</div>
</div>
						            <div class="col-md-6" id="chequeDate" style="display: none;">
					                	<div class="form-group">
					                  		<label class="control-label">Cheque Date</label>
					                  		<div class="input-group date">
							                  <div class="input-group-addon">
							                    <i class="fa fa-calendar"></i>
							                  </div>
							                  <input type="text" class="form-control pull-right" name="cheque_date" id="datepicker" value="<?php echo set_value("cheque_date"); ?>" placeholder="Cheque Date">
							                </div>
											<span style="color: red;"><?php echo form_error('cheque_date');?></span>
					                	</div>
						            </div>
						            
<div class="col-md-6" id="transactionId" style="display: none;">
	<div class="form-group">
  		<label class="control-label">Transaction Id</label>
		<input type="text" class="form-control" name="transaction_id" id="transaction_id" value="<?php echo set_value("transaction_id"); ?>" placeholder="Transaction Id">
		<span style="color: red;"><?php echo form_error('transaction_id');?></span>
	</div>
</div>
						            <div class="col-md-6" id="bankName" style="display: none;">
					                	<div class="form-group">
					                  		<label class="control-label">Bank Name</label>
											<input type="text" class="form-control" name="bank_name" id="bank_name" value="<?php echo set_value("bank_name"); ?>" placeholder="Bank Name">
											<span style="color: red;"><?php echo form_error('bank_name');?></span>
					                	</div>
						            </div>
						            <div class="col-md-6" id="transactionDate" style="display: none;">
					                	<div class="form-group">
					                  		<label class="control-label">Transaction Date</label>
					                  		<div class="input-group date">
							                  <div class="input-group-addon">
							                    <i class="fa fa-calendar"></i>
							                  </div>
							                  <input type="text" class="form-control pull-right" name="transaction_date" id="datepicker1" value="<?php echo set_value("transaction_date"); ?>" placeholder="Transaction Date">
							                </div>
											<span style="color: red;"><?php echo form_error('transaction_date');?></span>
					                	</div>
						            </div>
						            <div class="col-md-6">	    
						                <div class="form-group">
							                <label for="exampleInputPassword1">Comment</label>
								            <textarea class="form-control" name="remarks"><?php echo set_value('remarks');?>
                                        	</textarea>
                                        	<span style="color: red;"><?php echo form_error('remarks');?></span>
						                </div>
						            </div>
						            <div class="col-md-6">
					                	<div class="form-group">
					                  		<label class="control-label">PDF / Word File</label>
											<input type="file" class="form-control" name="file_upload" id="file_upload" value="<?php echo set_value("file_upload"); ?>" placeholder="Receive By">
											<span style="color: red;"><?php echo form_error('file_upload');?></span>
					                	</div>
						            </div>

						        </div>    			              	
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>

<script>
	$(document).ready(function(){
	    $('#payType').on('change', function() {
		    if ( this.value == '2')
		    {
		    	$("#chequeNo").show();
		    	$("#chequeDate").show();
		    	$("#transactionId").hide();
		    	$("#transactionDate").hide();
		    	$("#bankName").hide();
		    }
		    else if (this.value == '3') 
		    {
		    	$("#transactionId").show();
		    	$("#transactionDate").show();
		    	$("#bankName").show();
		    	$("#chequeNo").hide();
		    	$("#chequeDate").hide();		    	
		    }
		    else
		    {
		    	$("#transactionId").hide();
		    	$("#transactionDate").hide();
		    	$("#bankName").hide();
		    	$("#chequeNo").hide();
		    	$("#chequeDate").hide();
		    }
	    });
	});
</script>