<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Author Payment Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Author Payment Listing</h3>
                  <a style="float: right;" href="<?php echo base_url(); ?>add-author-payment" class="btn btn-primary">
                    <span>
                        Add Author Payment
                    </span>
                    </a> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category" style="width: 100%;">
                    <thead>
                        <th style="display: none;">No</th>
                        <th>Sr. No.</th>
                        <th>Author Name</th>
                        <th>Received By</th>
                        <th>Paid By</th>
                        <th>Payment Amount</th>
                        <th>Payment Type</th>
                        <th>Date</th>
                        <th>Remarks</th>
                        <th>PDF / Word file</th>
                    </thead>
                    
                        <?php $inc = 1; $i = 1;
                        foreach($categoryListingData as $row)
                        {?>
                        <tr>
                            <td style="display: none;"><?php echo $i++;?></td>
                            <td><?php echo $inc++;?></td>
                            <td><?php echo $row['first_name']." ".$row['last_name'];?></td>
                            <td><?php echo $row['received_by'];?></td>
                            <td><?php echo $row['paid_by'];?></td>
                            <td><?php echo $row['pay_amount'];?></td>
                            <td>
                                <?php if ($row['pay_type'] == '1'): ?>
                                    <?php echo 'Cash' ?>
                                <?php elseif ($row['pay_type'] == '2'): ?>
                                    <a href="" class="toggler" data-prod-cat="<?php echo $row['id'];?>">Chaque</a>
                                <?php else: ?>
                                    <a href="" class="toggler" data-prod-cat="<?php echo $row['id'];?>">Bank Transaction</a>
                                <?php endif ?>
                            </td>
                            <td><?php echo $row['created_at'];?></td>
                            <td><?php echo $row['remarks'];?></td>
                            <td>
                                <?php if (!empty($row['file_upload'])): ?>

                                    <a href="<?php echo base_url();?>assets/image/Payment_PDF_Doc/<?php echo $row['file_upload'] ?>" target="_blank" style="cursor:pointer">PDF</a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php if ($row['pay_type'] == '2'): ?>
                            <tr style="display: none;" class="cat<?php echo $row['id'];?>">
                                <td style="display: none;"><?php echo $i++;?></td>
                                <td></td>
                                <td></td>
                                <td colspan="4">
                                    <table class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <th>Cheque Numbur</th>
                                            <th>Cheque Date</th>
                                        </thead>
                                        <tr>
                                            <td><?php echo $row['cheque_no'];?></td>
                                            <td><?php echo $row['cheque_date'];?></td>
                                        </tr>
                                    </table>
                                </td>
                                <td></td>
                                <td></td>
                                <td style="display: none;"></td>
                                <td style="display: none;"></td>
                                <td style="display: none;"></td>
                                <td style="display: none;"></td>
                            </tr>
                        <?php elseif ($row['pay_type'] == '3'): ?>  
                            <tr style="display: none;" class="cat<?php echo $row['id'];?>">
                                <td style="display: none;"><?php echo $i++;?></td>
                                <td></td>
                                <td></td>
                                <td colspan="4">
                                    <table class="table table-bordered table-striped dataTable">
                                        <thead>
                                            <th>Transaction Id</th>
                                            <th>Bank Name</th>
                                            <th>Transaction Date</th>
                                        </thead>
                                        <tr>
                                            <td><?php echo $row['transaction_id'];?></td>
                                            <td><?php echo $row['bank_name'];?></td>
                                            <td><?php echo $row['transaction_date'];?></td>
                                        </tr>
                                    </table>
                                </td>
                                <td></td>
                                <td></td>
                                <td style="display: none;"></td>
                                <td style="display: none;"></td>
                                <td style="display: none;"></td>
                                <td style="display: none;"></td>
                            </tr> 
                        <?php endif ?>

                        <?php }
                        ?>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,9],
                    "orderable":false,
                },
            ],
          });
      });
</script>
<script>
    $(document).ready(function(){
        $(".toggler").click(function(e){
            e.preventDefault();
            $('.cat'+$(this).attr('data-prod-cat')).toggle();
        });
    });
</script>