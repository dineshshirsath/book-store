
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!--<style>
    .content
    	{ 
    	  border:2px solid #ccc;
    	  width:300px;
    	  height: 100px; 
    	  overflow-y: scroll;
}
    </style>-->
    <!-- Main content -->
    <section class="content">
<div class="box box-primary">
    <div class="box-header with-border" style="margin-left: 425px;">
        <h3 class="box-title">Terms And Conditions</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <form method="POST" action="<?php echo base_url(); ?>change-status">
            <div class="box box-primary" style="border: 1px solid #d2d2d2">
                <!-- form start -->
                <div class="box-body">
                    <div class="col-md-12 p-b-30" style="max-height: 500px; overflow-y: scroll;">
                        <p style="font-weight: 400;">This agreement applies as between you, the User of this Website and we, the owner(s) of this Website. Your agreement to comply with and be bound by these Terms and Conditions is deemed to occur upon your first use of the Website. If you do not agree to be bound by these Terms and Conditions, you should stop using the Website immediately.</p>
                        <p style="font-weight: 400;">No part of this Website is intended to constitute a contractual offer capable of acceptance. Your order constitutes a contractual offer and our acceptance of that offer is deemed to occur upon our sending a dispatch email to you indicating that your order has been fulfilled and is on its way to you.</p>
                        <ol>
                        <li style="font-weight: 400;">
                        <span></span>Definitions and Interpretation</li>
                        </ol>
                        <p style="font-weight: 400;">In this Agreement the following terms shall have the following meanings: </p>
                        <p style="font-weight: 400;"> </p>
                        <table style="font-weight: 400;">
                        <tbody>
                        <tr>
                        <td>
                        <p><b><strong>“Account”</strong></b></p>
                        </td>
                        <td>
                        <p>means collectively the personal information, Payment Information and credentials used by Users to access Paid Content and / or any communications System on the Website;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“Carrier”</strong></b></p>
                        </td>
                        <td>
                        <p>means any third party responsible for transporting purchased Goods from our Premises to customers;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“</strong></b><b><strong>Content</strong></b><b><strong>”</strong></b></p>
                        </td>
                        <td>
                        <p>means any text, graphics, images, audio, video, software, data compilations and any other form of information capable of being stored in a computer that appears on or forms part of this Website;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“Goods”</strong></b></p>
                        </td>
                        <td>
                        <p>means any products that we advertises and  / or makes available for sale through this Website;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“</strong></b><b><strong>Service</strong></b><b><strong>”</strong></b></p>
                        </td>
                        <td>
                        <p>means collectively any online facilities, tools, services or information that we makes available through the Website either now or in the future;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“Payment Information”</strong></b></p>
                        </td>
                        <td>
                        <p>means any details required for the purchase of Goods from this Website.  This includes, but is not limited to, credit / debit card numbers, bank account numbers and sort codes;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“Purchase Information”</strong></b></p>
                        </td>
                        <td>
                        <p>means collectively any orders, invoices, dispatch notes, receipts or similar that may be in hard copy or electronic form;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“</strong></b><b><strong>System</strong></b><b><strong>”</strong></b></p>
                        </td>
                        <td>
                        <p>means any online communications infrastructure that we makes available through the Website either now or in the future.  This includes, but is not limited to, web-based email, message boards, live chat facilities and email links;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“User” / “Users”</strong></b></p>
                        </td>
                        <td>
                        <p>means any third party that accesses the Website and is not employed by we and acting in the course of their employment;</p>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <p><b><strong>“Website”</strong></b></p>
                        </td>
                        <td>
                        <p>means the website that you are currently using (www.orbs-solutions.com) and any sub-domains of this site (e.g. subdomain.www.orbs-solutions.com/sale) unless expressly excluded by their own terms and conditions.</p>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        <p style="font-weight: 400;"> </p>
                        <ol start="2">
                        <li style="font-weight: 400;">
                        <span></span>Age Restrictions</li>
                        </ol>
                        <p style="font-weight: 400;">Persons under the age of 18 should use this Website only with the supervision of an Adult. Payment Information must be provided by or with the permission of an Adult.</p>
                        <ol start="3">
                        <li style="font-weight: 400;">
                        <span></span>Business Customers</li>
                        </ol>
                        <p style="font-weight: 400;">These Terms and Conditions also apply to customers buying Goods in the course of business. If you are a business customer, please contact<span> </span>orbs-solutions.com.</p>
                        <ol start="4">
                        <li style="font-weight: 400;">
                        <span></span>International Customers</li>
                        </ol>
                        <p style="font-weight: 400;">If Goods are being ordered from outside we’s country of residence, import duties and taxes may be incurred once your Goods reach their destination. we are not responsible for these charges and we undertake to make no calculations or estimates in this regard. If you are buying internationally, you are advised to contact your local customs authorities for further details on costs and procedures. As the purchaser of the Goods, you will also be the importer of record and as such should ensure that your purchase is in full compliance with the laws of the country into which the Goods are being imported. Please be aware that Goods may be inspected on arrival at port for customs purposes and we cannot guarantee that the packaging of your Goods will be free of signs of tampering.</p>
                        <ol start="5">
                        <li style="font-weight: 400;">
                        <span></span>Intellectual Property</li>
                        </ol>
                        <p style="font-weight: 400;"><b>5.1</b>&nbsp;Subject to the exceptions in Clause 6 of these Terms and Conditions, all Content included on the Website, unless uploaded by Users, including, but not limited to, text, graphics, logos, icons, images, sound clips, video clips, data compilations, page layout, underlying code and software is the property of we, our affiliates or other relevant third parties. By continuing to use the Website you acknowledge that such material is protected by applicable United Kingdom and International intellectual property and other laws.</p>
                        <p style="font-weight: 400;"><b>5.2</b>Subject to Clause 7 you may not reproduce, copy, distribute, store or in any other fashion re-use material from the Website unless otherwise indicated on the Website or unless given express written permission to do so by we.</p>
                        <ol start="6">
                        <li style="font-weight: 400;">
                        <b><strong></strong></b><b><strong></strong></b><span> </span>Third Party Intellectual Property</li>
                        </ol>
                        <p style="font-weight: 400;"><b>6.1</b> Subject to Clause 7 you may not reproduce, copy, distribute, store or in any other fashion re-use such material unless otherwise indicated on the Website or unless given express written permission to do so by the relevant manufacturer or supplier.</p>
                        <ol start="7">
                        <li style="font-weight: 400;">
                        <span></span>Fair Use of Intellectual Property</li>
                        </ol>
                        <p style="font-weight: 400;">Material from the Website may be re-used without written permission where any of the exceptions detailed in Chapter III of the Copyright Designs and Patents Act 1988 apply.</p>
                        <ol start="8">
                        <li style="font-weight: 400;">
                        <span></span>Links to Other Websites</li>
                        </ol>
                        <p style="font-weight: 400;">This Website may contain links to other sites. Unless expressly stated, these sites are not under the control of we or that of our affiliates. We assume no responsibility for the content of such websites and disclaim liability for any and all forms of loss or damage arising out of the use of them. The inclusion of a link to another site on this Website does not imply any endorsement of the sites themselves or of those in control of them.</p>
                        <ol start="9">
                        <li style="font-weight: 400;">
                        <span></span>Links to this Website</li>
                        </ol>
                        <p style="font-weight: 400;">Those wishing to place a link to this Website on other sites may do so only to the home page of the site www.lavishalice.com without prior permission. Deep linking (i.e. links to specific pages within the site) requires the express permission of we. To find out more please contact us by email at<span> </span>matthew@lavishalice.comor<span> </span>contact<span> </span>enquiries@lavishalice.com.</p>
                        <ol start="10">
                        <li style="font-weight: 400;">
                        <span></span>Use of Communications Facilities</li>
                        </ol>
                        <p style="font-weight: 400;">10.1       <span> </span>When using the enquiry form or any other System on the Website you should do so in accordance with the following rules:</p>
                        <p style="font-weight: 400;">10.1.1  <span> </span>You must not use obscene or vulgar language;</p>
                        <p style="font-weight: 400;">10.1.2  <span> </span>You must not submit Content that is unlawful or otherwise objectionable.  This includes, but is not limited to, Content that is abusive, threatening, harassing, defamatory, ageist, sexist or racist;</p>
                        <p style="font-weight: 400;">10.1.3  <span> </span>You must not submit Content that is intended to promote or incite violence;</p>
                        <p style="font-weight: 400;">10.1.4  <span> </span>It is advised that submissions are made using the English language(s) as we may be unable to respond to enquiries submitted in any other languages;</p>
                        <p style="font-weight: 400;">10.1.5  <span> </span>The means by which you identify yourself must not violate these Terms and Conditions or any applicable laws;</p>
                        <p style="font-weight: 400;">10.1.6  <span> </span>You must not impersonate other people, particularly employees and representatives of weor our affiliates; and</p>
                        <p style="font-weight: 400;">10.1.7  <span> </span>You must not use our System for unauthorised mass-communication such as “spam” or “junk mail”.</p>
                        <p style="font-weight: 400;">10.2       <span> </span>You acknowledge that we reserves the right to monitor any and all communications made to us or using our System.</p>
                        <p style="font-weight: 400;">10.3       <span> </span>You acknowledge that we may retain copies of any and all communications made to us or using our System.</p>
                        <p style="font-weight: 400;">10.4       <span> </span>You acknowledge that any information you send to us through our System or post on the enquiry form may be modified by us in any way and you hereby waive your moral right to be identified as the author of such information. Any restrictions you may wish to place upon our use of such information must be communicated to us in advance and we reserve the right to reject such terms and associated information.</p>
                        <ol start="11">
                        <li style="font-weight: 400;">
                        <span></span>Accounts</li>
                        </ol>
                        <p style="font-weight: 400;">11.1       <span> </span>In order to purchase Goods on this Website and to use the enquiry form facilities you are required to create an Account which will contain certain personal details and Payment Information which may vary based upon your use of the Website as we may not require payment information until you wish to make a purchase. By continuing to use this Website you represent and warrant that:</p>
                        <p style="font-weight: 400;">11.1.1  <span> </span>all information you submit is accurate and truthful;</p>
                        <p style="font-weight: 400;">11.1.2  <span> </span>you have permission to submit Payment Information where permission may be required; and</p>
                        <p style="font-weight: 400;">11.1.3  <span> </span>you will keep this information accurate and up-to-date.</p>
                        <p style="font-weight: 400;">Your creation of an Account is further affirmation of your representation and warranty.</p>
                        <p style="font-weight: 400;">11.2       <span> </span>It is recommended that you do not share your Account details, particularly your username and password.  we accepts no liability for any losses or damages incurred as a result of your Account details being shared by you.  If you use a shared computer, it is recommended that you do not save your Account details in your internet browser.</p>
                        <p style="font-weight: 400;">11.3       <span> </span>If you have reason to believe that your Account details have been obtained by another without consent, you should contact we immediately to suspend your Account and cancel any unauthorised purchases that may be pending. Please be aware that purchases can only be cancelled until they are dispatched. In the event that an unauthorised purchase is dispatched prior to your notifying us of the unauthorised nature of the purchase, we accepts no liability or responsibility and you should make contact with the Carrier detailed in the Purchase Information.</p>
                        <p style="font-weight: 400;">11.4       <span> </span>When choosing your username you are required to adhere to the terms set out above in Clause 10. Any failure to do so could result in the suspension and/or deletion of your Account.</p>
                        <ol start="12">
                        <li style="font-weight: 400;">
                        <span></span>Termination and Cancellation</li>
                        </ol>
                        <p style="font-weight: 400;">12.1       <span> </span>Either we or you may terminate your Account. If we terminates your Account, you will be notified by email and an explanation for the termination will be provided.  Notwithstanding the foregoing, we reserve the right to terminate without giving reasons.</p>
                        <p style="font-weight: 400;">12.2       <span> </span>If we terminates your Account, any current or pending purchases on your Account will be cancelled and will not be dispatched.</p>
                        <p style="font-weight: 400;">12.3       <span> </span>we reserves the right to cancel purchases without stating reasons, for any reason prior to processing payment and dispatch.</p>
                        <p style="font-weight: 400;">12.4       <span> </span>If purchases are cancelled for any reason prior to dispatch you will be refunded any monies paid in relation to those purchases.</p>
                        <p style="font-weight: 400;">12.5       <span> </span>If you terminate your Account any non-dispatched purchases will be cancelled and you will be refunded any monies paid in relation to those purchases.</p>
                        <ol start="13">
                        <li style="font-weight: 400;">
                        <span></span>Goods, Pricing and Availability</li>
                        </ol>
                        <p style="font-weight: 400;">13.1        By placing an order with we, you automatically accept our Terms &amp; Conditions.</p>
                        <p style="font-weight: 400;">13.2       <span> </span>Where appropriate, you may be required to select the required [size] [model] [colour] [number] [other features] of the Goods that you are purchasing.</p>
                        <p style="font-weight: 400;">13.3       <span> </span>we does not represent or warrant that such Goods will be available. Stock indications are provided on the Website and are subject to availability.</p>
                        <p style="font-weight: 400;">13.4       <span> </span>All pricing information on the Website is correct at the time of going online. we reserves the right to change prices and alter or remove any special offers from time to time and as necessary.  All pricing information is reviewed and updated every working day.</p>
                        <p style="font-weight: 400;">13.5       <span> </span>In the event that prices are changed during the period between an order being placed for Goods and we processing that order and taking payment, you will be contacted prior to your order being processed with details of the new price;</p>
                        <ol start="14">
                        <li style="font-weight: 400;">
                        <span></span>Delivery</li>
                        </ol>
                        <p style="font-weight: 400;">14.1       <span> </span>we will notify you by way of email when your goods are to be dispatched to you. The message will contain details of estimated delivery times in addition to any reasons for a delay in the delivery of the Goods purchased by you.</p>
                        <p style="font-weight: 400;">14.2       <span> </span>If we receives no communication from you, within 14 days of delivery, regarding any problems with the Goods, you are deemed to have received the Goods in full working order and with no problems. If goods haven't been received within 14 working days, please notify use as soon as possible.</p>
                        <p style="font-weight: 400;">14.3        By placing an order with we, you accept all of our Terms &amp; Conditions on deliveries. If we do not meet the allocated timeframe for delivery, you are entitled to a full postage refund. If you have used a free delivery service, no refund is applicable. we is not liable for any failed or late deliveries using our free delivery services. On occasion, our free delivery service can experience delays on dispatch. If we know your goods are going to be 'dispatched' late, we will endeavour to contact you to let you know. Unfortunately we are unable to monitor every delivery once the goods have been dispatched - this will be left to the customer to track and manage. Any delays on our free delivery services will need to be dealt with by the carrier used. All contact information will be provided on request.</p>
                        <ol start="15">
                        <li style="font-weight: 400;">
                        <span></span>Returns Policy</li>
                        </ol>
                        <p style="font-weight: 400;">we aims to always provide high quality Goods that are fault free and undamaged.  On occasion however, goods may need to be returned.  Returns are governed by these Terms and Conditions.</p>
                        <p style="font-weight: 400;">- we operates a 14 day returns policy (including bank holidays and weekends)</p>
                        <p style="font-weight: 400;">- Goods must be back with us by the 14th day</p>
                        <p style="font-weight: 400;">- The returns period starts from the day delivery was attempted</p>
                        <p style="font-weight: 400;">- Returns are valid as long as you return your item/s back to us in unworn &amp; unused condition with all original packaging. We'll process your refund or exchange within 14 working days after receipt of your return</p>
                        <p style="font-weight: 400;">- All items must have the original swing tag still firmly attached, so please take care when trying the goods on at home. If the tag is removed, no refund will be issued and the item will be forwarded back to your address or a credit note issued</p>
                        <p style="font-weight: 400;">- If you return something outside of the returns period, we reserve the right to issue a credit note or resend the goods ordered back to the original delivery address</p>
                        <p style="font-weight: 400;">- It is your responsibility to make sure you send the return using a recorded &amp; trackable service, as we cannot refund any lost, missing or damaged parcels. It will be your responsibilty to claim</p>
                        <p style="font-weight: 400;">- Please note we are unable to refund any postage or carriage charges for returns.</p>
                        <p style="font-weight: 400;">- We are unable to refund swimwear &amp; earrings for hygiene reasons</p>
                        <p style="font-weight: 400;">- Don't forget - all refunds will be issued back to the original method of payment. If your item is faulty, please send it back to us as soon as you can. We'll try our best to repair or replace it, but if we can't, you will be issued with a full refund</p>
                        <p style="font-weight: 400;">- If you would like to exchange your goods, please state this clearly on your returns form. If you are exchanging goods, please note the 14 day money back period still applies from the date of original purchase so bear this in mind if you are wanting to exchange your goods for another size as we'll only be able to issue you with a credit note</p>
                        <p style="font-weight: 400;">15.1       <span> </span>If you have received Goods which do not match those that you have ordered, unless accompanied by an explanatory note detailing the changes, stating reasons for the changes and setting out your options, you should contact us within 5 days from delivery to arrange the return and replacement.  we is not responsible for paying shipment costs.  You will be given the option to have the Goods replaced with those ordered (if available) or to be refunded through the payment method used by you when purchasing the Goods.  Refunds and replacements will be issued upon our receipt of the returned Goods.</p>
                        <p style="font-weight: 400;">15.2       <span> </span>If any Goods you have purchased have faults when they are delivered to you, you should contact we within 5 days from delivery to arrange the return and replacement.  we is responsible for paying refunding the shipment costs if the Goods are deemed faulty.  Goods must be returned in their original condition with all packaging and documentation.  Upon receipt of the returned Goods, the price of the Goods, as paid by you, will be refunded to you through the payment method used by you when purchasing the Goods.</p>
                        <p style="font-weight: 400;">15.3       <span> </span>If any Goods develop faults within their warranty period, you are entitled to a repair or replacement under the terms of that warranty.</p>
                        <p style="font-weight: 400;">15.4       <span> </span>If Goods are damaged in transit and the damage is apparent on delivery, you should sign the delivery note to the effect that the goods have been damaged.  In any event, you should report such damage to we within 5 days and arrange collection and return.  we is responsible for refunding the returns shipment costs.  Upon receipt of the returned Goods, the price of the Goods, as paid by you, will be refunded to you through the payment method used by you when purchasing the Goods.</p>
                        <p style="font-weight: 400;">15.5       <span> </span>On June 13th 2014 the new Consumer Contract Regulations came into force. This entitles you to a 14 day cancellation period. This period begins once your order is complete and ends 14 working days after the Goods have been attempted for delivery.  If you change your mind about the goods within this period, please return them to we within 14 days of receipt, including weekends and bank holidays.  You are responsible for paying shipment costs if Goods are returned for this reason. Sale items are also included as part of the same Consumer Contract Regulations period.</p>
                        <p style="font-weight: 400;">15.6       <span> </span>If you wish to return Goods to we for any of the above reasons, please contact us using the details on contact page to make the appropriate arrangements.</p>
                        <p style="font-weight: 400;">15.7       <span> </span>we reserves the right to exercise discretion with respect to any returns under these Terms and Conditions.  Factors which may be taken into account in the exercise of this discretion include, but are not limited to:</p>
                        <p style="font-weight: 400;">15.7.1  <span> </span>Any use or enjoyment that you may have already had out of the Goods;</p>
                        <p style="font-weight: 400;">15.7.2  <span> </span>Any characteristics of the Goods which may cause them to deteriorate or expire rapidly;</p>
                        <p style="font-weight: 400;">15.7.3  <span> </span>The fact that the Goods consist of audio or video recordings or computer software and that the packaging has been opened;</p>
                        <p style="font-weight: 400;">15.7.4  <span> </span>Any discounts that may have formed part of the purchase price of the Goods to reflect any lack of quality made known to the Customer at the time of purchase.</p>
                        <p style="font-weight: 400;">Such discretion to be exercised only within the confines of the law.</p>
                        <ol start="16">
                        <li style="font-weight: 400;">
                        <span></span>Privacy</li>
                        </ol>
                        <p style="font-weight: 400;">16.1       <span> </span>Use of the Website is also governed by our Privacy Policy which is incorporated into these terms and conditions by this reference. To view the Privacy Policy, please click on the link below.</p>
                        <p style="font-weight: 400;">16.2       <span> </span>The Website places the following cookies onto your computer or device. These cookies are used for the purposes described herein. Full details of the cookies used by the Website and your legal rights with respect to them are included in our Privacy Policy. By accepting these terms and conditions, you are giving consent to we to place cookies on your computer or device. Please read the information contained in the Privacy Policy prior to acceptance.</p>
                        <p style="font-weight: 400;">16.3       <span> </span>If you wish to opt-out of our placing cookies onto your computer or device You may also wish to delete cookies which have already been placed. For instructions on how to do this, please consult your internet browser’s help menu.</p>
                        <ol start="17">
                        <li style="font-weight: 400;">
                        <span></span>Disclaimers</li>
                        </ol>
                        <p style="font-weight: 400;">17.1       <span> </span>we makes no warranty or representation that the Website will meet your requirements, that it will be of satisfactory quality, that it will be fit for a particular purpose, that it will not infringe the rights of third parties, that it will be compatible with all systems, that it will be secure and that all information provided will be accurate. We make no guarantee of any specific results from the use of our Services.</p>
                        <p style="font-weight: 400;">17.2       <span> </span>No part of this Website is intended to constitute advice and the Content of this Website should not be relied upon when making any decisions or taking any action of any kind.</p>
                        <p style="font-weight: 400;">17.3       <span> </span>No part of this Website is intended to constitute a contractual offer capable of acceptance.</p>
                        <p style="font-weight: 400;">17.4       <span> </span>Whilst we uses reasonable endeavours to ensure that the Website is secure and free of errors, viruses and other malware, all Users are advised to take responsibility for their own security, that of their personal details and their computers.</p>
                        <ol start="18">
                        <li style="font-weight: 400;">
                        <span></span>Changes to the Service and these Terms and Conditions</li>
                        </ol>
                        <p style="font-weight: 400;">we reserves the right to change the Website, its Content or these Terms and Conditions at any time.  You will be bound by any changes to the Terms and Conditions from the first time you use the Website following the changes.  If we is required to make any changes to Terms and Conditions relating to sale of Goods by law, these changes will apply automatically to any orders currently pending in addition to any orders placed by you in the future.</p>
                        <ol start="19">
                        <li style="font-weight: 400;">
                        <span></span>Availability of the Website</li>
                        </ol>
                        <p style="font-weight: 400;">19.1       <span> </span>The Service is provided “as is” and on an “as available” basis.  We give no warranty that the Service will be free of defects and / or faults.  To the maximum extent permitted by the law we provide no warranties (express or implied) of fitness for a particular purpose, accuracy of information, compatibility and satisfactory quality.</p>
                        <p style="font-weight: 400;">19.2       <span> </span>we accepts no liability for any disruption or non-availability of the Website resulting from external causes including, but not limited to, ISP equipment failure, host equipment failure, communications network failure, power failure, natural events, acts of war or legal restrictions and censorship.</p>
                        <ol start="20">
                        <li style="font-weight: 400;">
                        <span></span>Limitation of Liability</li>
                        </ol>
                        <p style="font-weight: 400;">20.1       <span> </span>To the maximum extent permitted by law, we accepts no liability for any direct or indirect loss or damage, foreseeable or otherwise, including any indirect, consequential, special or exemplary damages arising from the use of the Website or any information contained therein. Users should be aware that they use the Website and its Content at their own risk.</p>
                        <p style="font-weight: 400;">20.2       <span> </span>Nothing in these Terms and Conditions excludes or restricts we’s liability for death or personal injury resulting from any negligence or fraud on the part of we.</p>
                        <p style="font-weight: 400;">20.3       <span> </span>Nothing in these Terms and Conditions  excludes or restricts we’s liability for any direct or indirect loss or damage arising out of the incorrect delivery of Goods or out of reliance on incorrect information included on the Website.</p>
                        <p style="font-weight: 400;">20.4       <span> </span>Whilst every effort has been made to ensure that these Terms and Conditions adhere strictly with the relevant provisions of the Unfair Contract Terms Act 1977, in the event that any of these terms are found to be unlawful, invalid or otherwise unenforceable, that term is to be deemed severed from these Terms and Conditions and shall not affect the validity and enforceability of the remaining Terms and Conditions.  This term shall apply only within jurisdictions where a particular term is illegal.</p>
                        <ol start="21">
                        <li style="font-weight: 400;">
                        <span></span>No Waiver</li>
                        </ol>
                        <p style="font-weight: 400;">In the event that any party to these Terms and Conditions fails to exercise any right or remedy contained herein, this shall not be construed as a waiver of that right or remedy.</p>
                        <ol start="22">
                        <li style="font-weight: 400;">
                        <span></span>Previous Terms and Conditions</li>
                        </ol>
                        <p style="font-weight: 400;">In the event of any conflict between these Terms and Conditions and any prior versions thereof, the provisions of these Terms and Conditions shall prevail unless it is expressly stated otherwise.</p>
                        <ol start="23">
                        <li style="font-weight: 400;">
                        <span></span>Third Party Rights</li>
                        </ol>
                        <p style="font-weight: 400;">Nothing in these Terms and Conditions shall confer any rights upon any third party.  The agreement created by these Terms and Conditions is between you and we.</p>
                        <div class="box-footer">
                          <div class="row">
                            <div class="col-sm-12 col-xs-6">
                              <div class="description-block border-right">
                              <INPUT TYPE="checkbox" NAME="Terms" VALUE="<?php echo $this->session->userdata('user_id'); ?>"> "I have read and agree with all Terms and conditions" <br>
                              <span style="color: red;"><?php echo form_error('Terms');?></span>
                              </div>
                              <!-- /.description-block -->
                            </div>
                          </div>
                          <!-- /.row -->
                        </div>
                    </div>
                </div>
            <!-- /.box -->
            </div>
            <div class="box-footer">
                <div class="row" style="margin-left: 500px;">
                    <button type="submit" class="btn btn-primary">I Agree</button>
                </div>
            </div>
        </form>
    </div>
<!-- /.box -->
</div>
               
    </section>
    <!-- /.content -->
</div>
<script>
	window.onbeforeunload = function () {
  window.scrollTo(0, 0);
}
</script>
<script>
	$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
    	
        $('#check').fadeIn();
    } else {
        $('#check').fadeOut();
    }
});

</script>
<script>
	$("#check").bind("change",function(){
		var id = "<?php echo $this->session->userdata('user_id'); ?>";
	    var url_action = "<?php echo base_url() ?>change-status?id="+id;
	    if($(this).is(":checked")){
			 $.ajax({
		        type: "GET",
		        url: url_action,
		        success: function (msg) {
		            window.location.href = "<?php echo base_url('admin-dashboard') ?>";
		        }
		    });	
		}
	    // set value for ajax
	});
</script>