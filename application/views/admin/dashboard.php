<?php 
if($this->session->userdata('user_id') !='' and $this->session->userdata('user_type') == 's' ){
	$data = $this->CommonFunctionModel->checkAuthorAgreeOrNot($this->session->userdata('user_id'));	
	if($data > 0 ){
		redirect(base_url('terms-and-conditions'));
	}
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <?php   
        $status = $this->CommonFunctionModel->checkSellerStatusAprovelorNot();
    ?>
    <section class="content">
        <?php if (!empty($status)): ?>
            <?php if ($status[0]['status'] == '1'): ?>
                <?php if ($this->session->userdata('user_type') == 's'): ?>
                    <h3 style="margin-left: 10%;">Author book selling details will be coming soon.</h3>
                <?php endif ?>
            <?php endif ?>        
        <?php endif ?>
        
        <?php if (!empty($status)): ?>
            <?php if ($status[0]['status'] == '0'): ?>
                <div style=" width: 100%; font-size: 40px; margin-left: 25%; margin-top: 180px;">
                    Please wait for admin approval
                </div>        
            <?php endif ?> 
        <?php endif ?>        
    </section>
    <!-- /.content -->
</div>