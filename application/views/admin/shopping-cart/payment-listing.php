<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Paymetn Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Payment Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Name</th>
                        <th>Transaction Id</th>
                        <th>Order Id</th>
                        <th>Total Amaunt</th>
                        <th>Payment Status</th>
                        <th>Payment Date</th>
                        <th>Action</th>
                    </thead>    
                    
                        <?php $inc = 1;
                        foreach($paymentListingData as $row)
                        {?>
                        <tr>
                            <td><?php echo $inc++;?></td>
                            <td><?php echo $row['first_name']." "; echo $row['last_name']; ?></td>
                            <td><?php echo $row['trans_id'];?></td>
                            <td><?php echo $row['order_id'];?></td>
                            <td><?php echo $row['amount'];?></td>
                            <td><?php echo $row['payment_status'];?></td>
                            <td><?php echo date('Y-m-d',strtotime($row['created']));?></td>
                            <td>
                                <div class="action-btns">
                                    <a href="<?php echo base_url(); ?>payment-product-details/<?php echo $row['id'] ?>" title="Payment Product Details" class="btn-lg edit-btn"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                        <?php }
                        ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,3],
                    "orderable":false,
                },
            ],
          });
      });
</script>
