<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Order Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Order Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>E-Mail</th>
                    </thead>    
                    
                        <?php $inc = 1;
                        foreach($bookPurchaseUsers as $row)
                        {?>
                        <tr>
                            <td><?php echo $inc++;?></td>
                            <td><?php echo $row['first_name']." "; echo $row['last_name']; ?></td>
                            <td>
                            	<?php
                            		$star = "";
									$len =  strlen($row['mobile']);
									$displayTowNu = substr($row['mobile'], 0, 2);
									for ($i=1; $i <= $len ; $i++) 
									{ 
										if ($i == 2) 
										{
											echo $displayTowNu;
										}
										elseif ($i > 2) {
											echo '*';
										}
									}
								?>
                            </td>
                            <td>
                            	<?php
                            		$star = "";
									$len =  strlen($row['email']);
									$displayTowNu = substr($row['email'], 0, 3);
									for ($i=1; $i <= $len ; $i++) 
									{ 
										if ($i == 2) 
										{
											echo $displayTowNu;
										}
										elseif ($i > 2) {
											echo '*';
										}
									}
								?>
                            </td>
                        </tr>
                        <?php }
                        ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,4],
                    "orderable":false,
                },
            ],
          });
      });
</script>
