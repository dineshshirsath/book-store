<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url(); ?>listing-payment"><i class="active"></i> Payment Listing</a></li>
            <li class="active">Product Details</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <!-- box-body -->
            <section class="invoice">
              <!-- title row -->
              <div class="row">
                <div class="col-xs-12">
                  <h2 class="page-header">
                    <i class="fa fa-shopping-cart"></i> Product Details
                    <small class="pull-right">Date: <?php echo date('d/m/Y'); ?></small>
                  </h2>
                </div>
                <!-- /.col -->
              </div>
              <!-- Table row -->
              <div class="row">
                <div class="col-xs-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Sr. No.</th>
                      <th>Product Name</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                              
                      <?php $i = 1; $grandTotal = 0;?>
                      <?php foreach ($paymentProductDetailsData as $row): ?>
                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $row['title'] ?></td>
                          <td><?php echo $row['qty'] ?></td>
                          <td><?php echo $row['price_per_product'] ?></td>
                          <td>
                            <?php
                              $total = "";
                              $total =  $row['qty'] * $row['price_per_product']; 
                              echo $total;
                              $grandTotal = $grandTotal + $total;
                            ?>
                          </td>
                        </tr>
                      <?php endforeach ?>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Grand Total</th>
                        <th><?php echo $grandTotal; ?></th>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-xs-12">
                  <h2 class="page-header">
                  </h2>
                </div>
                <!-- /.col -->
              </div>
              

              <!-- this row will not appear when printing -->
    </section>

            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
  


    