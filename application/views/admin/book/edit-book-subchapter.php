<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css"/>
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
	    		Author Dashboard
	    	<?php else: ?>
	    		Admin Dashboard
	    	<?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>view-book/<?php echo $subchapterUpdatData[0]['book_title']; ?>">Book Preview</a></li>
	        <li class="active">Update Sub Chapter</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 	<div class="col-md-12">
				        <?php if($this->session->flashdata('success')){ ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } else if($this->session->flashdata('error')){  ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('error'); ?>
						</div>
						<?php } ?>
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Update Sub Chapter</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url().ADMIN.BOOK; ?>BookController/updateSubChapter" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
				              		<div class="col-md-12" style="border: 1px solid #d2d6de; border-radius: 5px; ">
				              			<div class="box-header with-border">
							              	<h3 class="box-title">Update Sub Chapter</h3>
							            </div>
							            <div class="col-md-12">
						                	<div class="form-group">
												<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $subchapterUpdatData[0]['id'];  ?>" placeholder="Chpater Name">
						                	</div>
							            </div>
							            <div class="col-md-12">
											<div class="form-group">
										  		<label>Book Title</label>
<select name="book_title" id="book_title" class="form-control">
	<option value="<?php echo $subchapterUpdatData[0]['book_title'];?>"><?php echo $subchapterUpdatData[0]['title'];?></option>
</select>
											</div>
							            </div>	
							            <div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Add Chapter Name</label>
<select name="chapter_id" id="chapter_id" class="form-control">
	<option value="<?php echo $subchapterUpdatData[0]['chapter_id'];?>"><?php echo $subchapterUpdatData[0]['chapter_name']; ?></option>
</select>
											</div>
										</div>
					              		<div class="col-md-12">
						                	<div class="form-group">
						                  		<label>Add Chapter Name</label>
												<input type="text" class="form-control" name="sub_chapter_name" id="sub_chapter_name" value="<?php echo $subchapterUpdatData[0]['sub_chapter_name'];  ?>" placeholder="Chpater Name">
												<span style="color: red;"><?php echo form_error('sub_chapter_name');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-12">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1">Content</label>
									            <textarea id="editor01" name="content"><?php echo $subchapterUpdatData[0]['content']; ?>
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('content');?></span>
							                </div>
							            </div>
							            <div class="col-md-6">
						                	<div class="form-group">
						                  		<label class="control-label">Remark</label>
												<input type="text" class="form-control" name="remark" id="remark" value="<?php echo $subchapterUpdatData[0]['remark']; ?>" placeholder="Remark">
												<span style="color: red;"><?php echo form_error('remark');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1"></label>
									            <textarea id="editor02" name="remark_description"><?php echo $subchapterUpdatData[0]['remark_description']; ?>
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('remark_description');?></span>
							                </div>
							            </div>
							            <div class="col-md-6">
						                	<div class="form-group">
						                  		<label class="control-label">Explanation 1</label>
												<input type="text" class="form-control" name="explanation_1" id="explanation_1" value="<?php echo $subchapterUpdatData[0]['explanation_1']; ?>" placeholder="explanation 1">
												<span style="color: red;"><?php echo form_error('explanation_1');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1"></label>
									            <textarea id="editor03" name="explanation_description_1"><?php echo $subchapterUpdatData[0]['explanation_description_1'];?>
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('explanation_description_1');?></span>
							                </div>
							            </div>
							            <div class="col-md-6">
						                	<div class="form-group">
						                  		<label class="control-label">Explanation 2</label>
												<input type="text" class="form-control" name="explanation_2" id="explanation_2" value="<?php echo $subchapterUpdatData[0]['explanation_2']; ?>" placeholder="Explanation 2">
												<span style="color: red;"><?php echo form_error('explanation_2');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1"></label>
									            <textarea id="editor04" name="explanation_description_2"><?php echo $subchapterUpdatData[0]['explanation_description_2'];?>
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('explanation_description_2');?></span>
							                </div>
							            </div>
							            <div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1">Ordering:</label>
									            <input type="number" class="form-control" name="order" id="order" value="<?php echo $subchapterUpdatData[0]['order'];?>" placeholder="Ordering">
							                </div>
							            </div>      
							        </div>
				              	</div> 				              	
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Update</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>

