
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
	    		Author Dashboard
	    	<?php else: ?>
	    		Admin Dashboard
	    	<?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-book">Book Listing</a></li>
	        <li class="active">Preview</li>
      	</ol>
    </section>
    <section class="content">
    	<?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">BOOK INFORMATION</h3>
				              	<a style="float: right; margin-right: 10px;" href="<?php echo base_url(); ?>handbook?id=<?php echo $bookInformation[0]['id'] ?>" target="_blank" class="btn btn-primary">
				                    <span>
				                        Book Preview
				                    </span>
			                	</a>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <div class="box-body">
			            		<table width="100%">
			            			<tr>
			            				<td>
			            					<table width="100%">
			            						<tr>
			            							<th width="20%" height="50px">Book Title</th>
						            				<th width="10%" height="50px">:-</th>
													<td><?php echo $bookInformation[0]['title'] ?></td>
						            			</tr>
						            			<tr>
						            				<th width="20%" height="50px">Publisher</th>
						            				<th width="10%" height="50px">:-</th>
													<td><?php echo $bookInformation[0]['publisher'] ?></td>
			            						</tr>
			            					</table>
			            				</td>
			            				<td>
			            					<table width="100%">
			            						<tr>
			            							<th width="30%" height="50px">Author Name</th>
						            				<th width="10%" height="50px">:-</th>
													<td><?php echo $bookInformation[0]['authour'] ?></td>
						            			</tr>
						            			<tr>
						            				<th width="30%" height="50px">Price (₹)</th>
						            				<th width="10%" height="50px">:-</th>
													<td><?php echo '₹ '.$bookInformation[0]['price'] ?></td>
			            						</tr>
			            					</table>
			            				</td>            				
			            			</tr>
			            			<tr>
			            				<td colspan="2">
			            					<table width="100%">
			            						<tr>
			            							<th width="15%" height="50px">Book Information</th>
						            				<th width="5%" height="50px">:-</th>
													<td><?php echo $bookInformation[0]['book_information'] ?></td>
						            			</tr>
			            					</table>
			            				</td>
			            			</tr>		            			
			            		</table>
				        	</div>
				    <!-- /.box -->
				    </div>
				    <div class="box box-primary" style="width: 49%; float: left;">
			            <div class="box-header with-border">
			              	<h3 class="box-title">CHAPTER LISTING</h3>
			              	<a style="float: right; margin-right: 10px;" href="<?php echo base_url(); ?>add-book-chapter/<?php echo $bookInformation[0]['id'] ?>" class="btn btn-primary">
			                    <span>
			                        Add Chapter
			                    </span>
			                </a>
			            </div>
			            <!-- /.box-header -->
			            <!-- form start -->
			            <div class="box-body">
			            	<table class="table table-bordered table-striped dataTable" id="category">
			                    <thead>
			                        <th>Sr No</th>
			                        <th>Chapter Name</th>
			                        <th>Action &nbsp;&nbsp;&nbsp;[ <i class="fa fa-plus-circle" style="color:green;"></i> Add Sub Chapter ]</th>
			                    </thead>
			                    
			                        <?php $inc = 1;
			                        foreach($bookChapterListingData as $row)
			                        {?>
			                        <tr>
			                            <td><?php echo $inc++;?></td>
			                            <td>
<?php if ($this->BookModel->checkSubChapterByChapterId($row['id'])): ?>
	<a href="<?php echo base_url(); ?>view-book/<?php echo $bookInformation[0]['id'] ?>?chapterId=<?php echo $row['id']; ?>"><?php echo $row['chapter_name'];?></a>
<?php else: ?>
	<?php echo $row['chapter_name'];?>
<?php endif ?>
			                            </td>
			                            <td>
			                                <div class="action-btns">
			                                	<a href="<?php echo base_url(); ?>add-book-subchapter/<?php echo $row['id']; ?>" title="Add sub chapter" class="btn-lg edit-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
			                                    <a href="<?php echo base_url(); ?>edit-book-chapter/<?php echo $row['id']; ?>" title="Edit Chapter" class="btn-lg edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
			                                    <a href="<?php echo base_url(); ?>delete-book-chapter/<?php echo $row['id']; ?>" title="Delete Chapter" class="btn-lg edit-btn"><i class="fa fa-trash" aria-hidden="true"></i></a>
			                                </div>
			                            </td>
			                        </tr>
			                        <?php }
			                        ?>
			                </table>
			        	</div>
				    <!-- /.box -->
				    </div>
				    <?php
				    if (!empty($_GET['chapterId']))
				    {
				    	$bookSubChapterListingData = $this->BookModel->getBookSubChapterListData($_GET['chapterId']);
				    }
				    ?>
				    <div class="box box-primary" style="width: 49%; float: right;">
			            <div class="box-header with-border">
			              	<h3 class="box-title">SUB-CHAPTER LISTING</h3>
			              	<h3 class="box-title" style="float: right;"><?php 
			              		if (!empty($bookSubChapterListingData))
								{
			              			echo '<b>'. $bookSubChapterListingData[0]['chapter_name']. '</b>'; 
			              		}
			              	?></h3>
			            </div>
			            <!-- /.box-header -->
			            <!-- form start -->
			            <div class="box-body">
			            	<table class="table table-bordered table-striped dataTable" id="category">
			                    <thead>
			                        <th>Sr No</th>
			                        <th>Sub Chapter Name</th>
                        			<th>Action</th>
			                    </thead>
								<?php
								    if (!empty($bookSubChapterListingData))
								    {
								    $inc = 1;
								    foreach($bookSubChapterListingData as $row)
								    {?>
								    <tr>
								        <td><?php echo $inc++;?></td>
								        <td><?php echo $row['sub_chapter_name'];?></td>
								        <td>
								            <div class="action-btns">
								                <a href="<?php echo base_url(); ?>edit-book-sub-chapter/<?php echo $row['id']; ?>" title="Edit sub chapter" class="btn-lg edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
								                <a href="<?php echo base_url(); ?>delete-book-sub-chapter/<?php echo $row['id']; ?>" title="Delete sub chapter" class="btn-lg edit-btn"><i class="fa fa-trash" aria-hidden="true"></i></a>
								            </div>
								        </td>
								    </tr>
									<?php 
									}
									}
								?>
			                </table>
			        	</div>
				    <!-- /.box -->
				    </div>
			    </div>
<!-- <button >Open Modal</button> -->	
			</div>
		</div>
	</section>
</div>



<!-- <tr>
				            				<th width="10%" height="50px">Book Title</th>
				            				<th width="5%" height="50px">:-</th>
											<td>Police Law</td>
				            			</tr>
				            			<tr>
				            				<th width="10%" height="50px">Author Name</th>
				            				<th width="5%" height="50px">:-</th>
											<td>Police Law</td>
				            			</tr> -->