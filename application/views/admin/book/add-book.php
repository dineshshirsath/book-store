
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
	    		Author Dashboard
	    	<?php else: ?>
	    		Admin Dashboard
	    	<?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-category">Book Listing</a></li>
	        <li class="active">Add Book</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 		<?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('success'); ?>
					</div>

				<?php } else if($this->session->flashdata('error')){  ?>

					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('error'); ?>
					</div>

					<?php } ?>
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Add Book</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form id="idForm" action="<?php echo base_url(); ?>store-book" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
					              <!-- radio -->
					              <div class="col-md-12">
						              	<div class="form-group">
						                <label class="control-label">Books Have</label>	
							                <div class="form-group">                	
							                  <input type="radio" onclick="idForm()" name="books_have" value="1" ><label> &nbsp;&nbsp;Multiply volumn with Multiply Chapter</label>
							              	</div>
											<div class="form-group">                	
							                  <input type="radio" onclick="idForm()" name="books_have" class="flat-red" value="2"><label>  &nbsp; &nbsp;Multiply Chapter with Multiply Sub- Chapter</label>
							              	</div>
							              	<div class="form-group">                	
							                  <input type="radio" onclick="idForm()" name="books_have" class="flat-red" value="3"><label>  &nbsp; &nbsp;One chapter with Multiply Sub- Chapter</label>
							              	</div>
						              </div>
						          </div>
					            </div>						                
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>				            
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
<!-- <button >Open Modal</button> -->	
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">

	function open_model(){
		$('#myModal').modal('show');
	}

	function close_model(){
		$('#myModal').modal('hide');
	}

	function idForm(){
   	var selectvalue = $('input[name=books_have]:checked', '#idForm').val();

		if(selectvalue == "1")
		{
			window.open('<?php echo base_url(); ?>add-book-chapter','_self');
			return true;
		}
		else if(selectvalue == "2")
		{
			window.open('http://www.google.com','_self');
			return true;
		}
		else if(selectvalue == '3')
		{
			window.open('<?php echo base_url(); ?>add-book-subchapter','_self');
			return true;
		}
			return false;
	};

</script>


