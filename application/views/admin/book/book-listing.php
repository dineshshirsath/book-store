<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Book Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <div class="box-header">
                <h3 class="box-title">Book Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Book Title</th>
                        <th>Author</th>
                        <th>Price (₹)</th>
                        <th>Image</th>
                        <th>Action &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ <i class="fa fa-plus-circle" style="color:green;"></i> Add Chapter ]</th>
                    </thead>
                    
                        <?php $inc = 1;
                        foreach($bookListingData as $row)
                        {?>
                        <tr>
                            <td><?php echo $inc++;?></td>
                            <td><?php echo $row['title']?></td>
                            <td><?php echo $row['authour']?></td>           
                            <td><?php echo '₹ '.$row['price']?></td>
                            <td><img style="width: 50px; height:70px" src="<?php base_url(); ?>assets/image/book/<?php echo $row['image'];?>" alt=""></td>
                            <td>
                                <div class="action-btns">
                                    <a href="<?php echo base_url(); ?>add-book-chapter/<?php echo $row['id']; ?>" title="Add book chapter" class="btn-lg edit-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                    <a href="<?php echo base_url(); ?>view-book/<?php echo $row['id']; ?>" title="Preview" class="btn-lg edit-btn"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                        <?php }
                        ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,5],
                    "orderable":false,
                },
            ],
          });
      });
</script>
