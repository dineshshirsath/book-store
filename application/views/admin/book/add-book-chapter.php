<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css"/>
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	    	<?php if ($this->session->userdata('user_type') == 's'): ?>
	    		Author Dashboard
	    	<?php else: ?>
	    		Admin Dashboard
	    	<?php endif ?>	        
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-book">Book Listing</a></li>
	        <li class="active">Add Chapter</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 	<div class="col-md-12">
				        <?php if($this->session->flashdata('success')){ ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } else if($this->session->flashdata('error')){  ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('error'); ?>
						</div>
						<?php } ?>
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Add Book Chapter</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url(); ?>store-book" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
				              		<div class="col-md-12" style="border: 1px solid #d2d6de; border-radius: 5px; ">
				              			<div class="box-header with-border">
							              	<h3 class="box-title">Add Chapter</h3>
							            </div>
							            <div class="col-md-12">
											<div class="form-group">
										  		<label>Book Title</label>
												<select name="book_title" id="book_title" class="form-control">
													<?php foreach ($bookTitleList as $row): ?>
														<option selected="" value="<?php echo $row['id'] ?>"><?php echo $row['title'] ?></option>
													<?php endforeach ?>
												</select>
											</div>
							            </div>	
					              		<div class="col-md-12">
						                	<div class="form-group">
						                  		<label>Add Chapter Name</label>
												<input type="text" class="form-control" name="chapter_name[0][]" id="chapter_name" value="<?php echo set_value("chapter_name"); ?>" placeholder="Chpater Name">
												<span style="color: red;"><?php echo form_error('chapter_name');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-12">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1">Content</label>
									            <textarea id="editor01" name="content[0][]"><?php echo set_value('content');?>
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('content');?></span>
							                </div>
							            </div>
							            <div class="col-md-6">
						                	<div class="form-group">
						                  		<label class="control-label">Remark</label>
												<input type="text" class="form-control" name="remark[0][]" id="remark" value="<?php echo set_value("remark"); ?>" placeholder="Remark">
												<span style="color: red;"><?php echo form_error('remark');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1"></label>
									            <textarea id="editor02" name="remark_description[0][]" placeholder="Remarks Discription">
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('remark_description');?></span>
							                </div>
							            </div>
							            <div class="col-md-6">
						                	<div class="form-group">
						                  		<label class="control-label">Explanation 1</label>
												<input type="text" class="form-control" name="explanation_1[0][]" id="explanation_1" value="<?php echo set_value("Explanation_1"); ?>" placeholder="explanation 1">
												<span style="color: red;"><?php echo form_error('explanation_1');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1"></label>
									            <textarea id="editor03" name="explanation_description_1[0][]"><?php echo set_value('explanation_description_1');?>
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('explanation_description_1');?></span>
							                </div>
							            </div>
							            <div class="col-md-6">
						                	<div class="form-group">
						                  		<label class="control-label">Explanation 2</label>
												<input type="text" class="form-control" name="explanation_2[0][]" id="explanation_2" value="<?php echo set_value("explanation_2"); ?>" placeholder="Explanation 2">
												<span style="color: red;"><?php echo form_error('explanation_2');?></span>
						                	</div>
							            </div>	
							            <div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1"></label>
									            <textarea id="editor04" name="explanation_description_2[0][]"><?php echo set_value('explanation_description_2');?>
	                                        	</textarea>
	                                        	<span style="color: red;"><?php echo form_error('explanation_description_2');?></span>
							                </div>
							            </div>
										
										<div class="col-md-6">	    
							                <div class="form-group">
								                <label for="exampleInputPassword1">Ordering:</label>
									            <input type="number" class="form-control" name="order[0][]" id="order" value="0" placeholder="Ordering">
							                </div>
							            </div>
										
							            <div id="main">
									        
									    </div>							            
							        </div>
							        <?php if ($bookTitleList[0]['books_have'] != '3'): ?>
							        	<div class="box-footer" style="float: right;">
						            		<button id="btAdd" type="button" class="btn btn-primary"><i class="fa fa-plus" ></i>&nbsp;&nbsp;Add More</button>
						            	<button type="button" id="btRemove" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Remove</button>            	
			              				</div>
							        <?php endif ?>							        
				              	</div> 				              	
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>

<script>
    $(document).ready(function() {

        var i = 0;
        // CREATE A "DIV" ELEMENT AND DESIGN IT USING jQuery ".css()" CLASS.
        var container = $(document.createElement('div'))

        $('#btAdd').click(function() {
            if (i < 4) {

                i = i + 1;

                // ADD TEXTBOX.
                $(container).append('<hr><br><div class="col-md-12"><div class="form-group"><label>Add Chapter Name</label><input type="text" class="form-control" name="chapter_name['+ i +'][]" id="chapter_name" placeholder="Chapter Name"></div></div><div class="col-md-12"><div class="form-group"><label for="exampleInputPassword1">Content</label><textarea class="form-control" id="content'+i+'" name="content['+ i +'][]"></textarea></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Remark</label><input type="text" class="form-control" name="remark['+ i +'][]" id="remark" placeholder="Remard"></div></div><div class="col-md-6"><div class="form-group"><label for="exampleInputPassword1"></label><textarea class="form-control" id="remark_description'+i+'" name="remark_description['+ i +'][]"></textarea></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Explanation 1</label><input type="text" class="form-control" name="explanation_1['+ i +'][]" id="explanation_1" placeholder="Explanation 1"></div></div><div class="col-md-6"><div class="form-group"><label for="exampleInputPassword1"></label><textarea class="form-control" id="explanation_1'+i+'" name="explanation_description_1['+ i +'][]"></textarea></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Explanation 2</label><input type="text" class="form-control" name="explanation_2['+ i +'][]" id="explanation_2" placeholder="Explanation 2"></div></div><div class="col-md-6"><div class="form-group"><label for="exampleInputPassword1"></label><textarea class="form-control" id="explanation_2'+i+'" name="explanation_description_2['+ i +'][]"></textarea></div></div><div class="col-md-6"><div class="form-group"><label for="exampleInputPassword1">Ordering:</label><input type="number" class="form-control" name="order['+i+'][]" id="order" value="0" placeholder="Ordering"></div></div>');


                // SHOW SUBMIT BUTTON IF ATLEAST "1" ELEMENT HAS BEEN CREATED.
                if (i == 1) {
                    var divSubmit = $(document.createElement('div'));
                    $(divSubmit).append('<input type=button class="bt"' + 
                        'onclick="GetTextValue()"' + 
                            'id=btSubmit value=Submit />');
                }

                // ADD BOTH THE DIV ELEMENTS TO THE "main" CONTAINER.
                $('#main').after(container);
                initializeEditors(i);
            }
            // AFTER REACHING THE SPECIFIED LIMIT, DISABLE THE "ADD" BUTTON.
            // (20 IS THE LIMIT WE HAVE SET)
            else {      
                $(container).append('<label>Reached the limit</label>'); 
                $('#btAdd').attr('class', 'bt-disable'); 
                $('#btAdd').attr('disabled', 'disabled');
            }
        });

        // REMOVE ONE ELEMENT PER CLICK.
        $('#btRemove').click(function() {
            if (i != 0) { $('#tb' + i).remove(); i = i - 1; }
        
            if (i == 0) { 
                $(container)
                    .empty() 
                    .remove(); 

                $('#btSubmit').remove(); 
                $('#btAdd')
                    .removeAttr('disabled') 
                    .attr('class', 'bt');
            }
        });

        // REMOVE ALL THE ELEMENTS IN THE CONTAINER.
        $('#btRemoveAll').click(function() {
            $(container)
                .empty()
                .remove(); 

            $('#btSubmit').remove(); 
            i = 0; 
            
            $('#btAdd')
                .removeAttr('disabled') 
                .attr('class', 'bt');
        });
    });

    // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
    var divValue, values = '';

    function GetTextValue() {
        $(divValue) 
            .empty() 
            .remove(); 
        
        values = '';

        $('.input').each(function() {
            divValue = $(document.createElement('div')).css({
                padding:'5px', width:'200px'
            });
            values += this.value + '<br />'
        });

        $(divValue).append('<p><b>Your selected values</b></p>' + values);
        $('body').append(divValue);
    }
</script>
<script type="text/javascript">
	function initializeEditors(i)
	{
		$(function () {
			CKEDITOR.replace('content'+i)
		    $('.textarea').wysihtml5()	
		});
		$(function () {    
		    CKEDITOR.replace('remark_description'+i)
		    $('.textarea').wysihtml5()	
	    });
	    $(function () {
		    CKEDITOR.replace('explanation_1'+i)
		    $('.textarea').wysihtml5()	
	    });
	    $(function () {
		    CKEDITOR.replace('explanation_2'+i)
		    $('.textarea').wysihtml5()	
		});
	}
</script>