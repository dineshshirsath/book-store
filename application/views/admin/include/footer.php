<!-- /.content-wrapper -->
		  	<footer class="main-footer">
			    <div class="pull-right hidden-xs">
			      	
			    </div>
		    	<strong>Copyright &copy; <?php echo date("Y"); ?> </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All rights
		    	reserved by FalconX.in.
		  	</footer>
			<!-- Control Sidebar -->
		  	<aside class="control-sidebar control-sidebar-dark">
		    	<!-- Create the tabs -->
		    	<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
		      		<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
		      		<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		    	</ul>
		    	<!-- Tab panes -->
		    	<div class="tab-content">
		      		<!-- Home tab content -->
			      	<div class="tab-pane" id="control-sidebar-home-tab"></div>
			      	<!-- /.tab-pane -->
		    	</div>
		  	</aside>
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
		</div>
	</body>
</html>
<script type="text/javascript">
	
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    })

$(function(){	
    var start = new Date();
	// set end date to max one year period:
	var end = new Date(new Date().setYear(start.getFullYear()+1));

	$('#from_date').datepicker({
	    startDate : start,
	    endDate   : end
		// update "to_date" defaults whenever "from_date" changes
	}).on('changeDate', function(){
	    // set the "to_date" start to not be later than "from_date" ends:
	    $('#to_date').datepicker('setStartDate', new Date($(this).val()));
	}); 

	$('#to_date').datepicker({
	    startDate : start,
	    endDate   : end
		// update "from_date" defaults whenever "to_date" changes
	}).on('changeDate', function(){
	    // set the "from_date" end to not be later than "to_date" starts:
	    $('#from_date').datepicker('setEndDate', new Date($(this).val()));
	});

	//Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    $('#datepicker1').datepicker({
      autoclose: true
    })
    
 });


$(function () {
    CKEDITOR.replace('editor01')
    $('.textarea').wysihtml5()
 })
$(function () {
    CKEDITOR.replace('editor02')
    $('.textarea').wysihtml5()
 })
$(function () {
    CKEDITOR.replace('editor03')
    $('.textarea').wysihtml5()
 })
$(function () {
    CKEDITOR.replace('editor04')
    $('.textarea').wysihtml5()
 })
 
</script>
