<?php 
$hide = "";
if($this->session->userdata('user_id') !='' and $this->session->userdata('user_type') == 's' ){
	$data = $this->CommonFunctionModel->checkAuthorAgreeOrNot($this->session->userdata('user_id'));	
	if($data > 0 ){
		$hide = "display:none;";
	}
}
?>
<!-- Akulina PVT LTD -->
<?php
  $imageData = $this->CommonFunctionModel->checkProfileImage($this->session->userdata('user_id')); 
?>
<aside class="main-sidebar" style="height: auto;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
              <?php if ($imageData[0]['image'] != '0'): ?>
                <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="img-circle" alt="User Image">
              <?php else: ?>                  
                <img src="<?php echo base_url() ?>assets/image/profile_Image/profile-picture.jpg" class="img-circle" alt="User Image">
              <?php endif ?>
          </div>
          <div class="pull-left info">
              <p><?php echo $this->session->userdata('user_name'); ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                      <i class="fa fa-search"></i>
                  </button>
                </span>
          </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
  <?php   
    $status = $this->CommonFunctionModel->checkSellerStatusAprovelorNot();
  ?>
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>
          <li class="active treeview menu-open">
            <a href="#">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>
          <?php if (!empty($status[0]['status'])): ?>
            <?php if (($status[0]['status'] == '1') and ($this->session->userdata('user_type') == 's')): ?>
              <li class="treeview" style="<?php echo $hide; ?>">
                <a href="#">
                  <i class="fa fa-edit"></i> <span>Book</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                                    
                  <li><a href="<?php echo base_url('listing-product') ?>"><i class="fa fa-circle-o"></i>Book Information List</a></li>
                  
                  <li><a href="<?php echo base_url('listing-book') ?>"><i class="fa fa-circle-o"></i>Book Chapter List</a></li>
                  
                  <!-- <li><a href="<?php echo base_url('add-pdf-file') ?>"><i class="fa fa-circle-o"></i>Add PDF</a></li> -->
    
                </ul>             
              </li> 
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-shopping-cart"></i> <span>Report</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  
                  <li><a href="<?php echo base_url('listing-sell-product') ?>"><i class="fa fa-circle-o"></i>Sell Product List</a></li>
                  
                  <li><a href="<?php echo base_url('Book-purchase-users-listing') ?>"><i class="fa fa-circle-o"></i>Book Purchase Users List</a></li>
                  
                </ul>             
              </li> 
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-cogs"></i> <span>Settings</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  
                  <li><a href="<?php echo base_url('listing-author-ratio') ?>"><i class="fa fa-circle-o"></i>Author Ratio List</a></li>
                  
                </ul>             
              </li> 
            <?php endif ?>
          <?php else: ?>
            <?php if ($this->session->userdata('user_type') == 'a'): ?>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-user"></i> <span>Users</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  
                  <li><a href="<?php echo base_url('listing-user') ?>"><i class="fa fa-circle-o"></i>Reader</a></li>
                  
                  <li><a href="<?php echo base_url('listing-author') ?>"><i class="fa fa-circle-o"></i>Author</a></li>
                
                </ul>             
              </li> 
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-book"></i> <span>Book</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  
                  <li><a href="<?php echo base_url('listing-category') ?>"><i class="fa fa-circle-o"></i>Category</a></li>

                  <li><a href="<?php echo base_url('listing-sub-category') ?>"><i class="fa fa-circle-o"></i>Sub Category</a></li>
                  
                  <li><a href="<?php echo base_url('listing-product') ?>"><i class="fa fa-circle-o"></i>Books Added by Admin </a></li>          
                  
                  <li><a href="<?php echo base_url('listing-author-book') ?>"><i class="fa fa-circle-o"></i>Books Added by Author</a></li>          
                  
                  <li><a href="<?php echo base_url('listing-news-letter') ?>"><i class="fa fa-circle-o"></i>Subscribe News Letter List </a></li>          

                </ul>             
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-shopping-cart"></i> <span>Report</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  
                  <li><a href="<?php echo base_url('listing-order') ?>"><i class="fa fa-circle-o"></i>Order List</a></li>
                  
                  <li><a href="<?php echo base_url('listing-payment') ?>"><i class="fa fa-circle-o"></i>Payment Details</a></li>
                  
                </ul>             
              </li> 
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-cogs"></i> <span>Settings</span>
                  <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  
                  <li><a href="<?php echo base_url('listing-author-payment') ?>"><i class="fa fa-circle-o"></i>Author Payment</a></li>
                  
                  <li><a href="<?php echo base_url('listing-author-order-ratio') ?>"><i class="fa fa-circle-o"></i>Author Order Ratio</a></li>
                  
                </ul>             
              </li>
          <?php endif ?>  
          <?php endif ?> 
        </ul>
    </section>
    <!-- /.sidebar -->
 </aside>
 <!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/website/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url() ?>assets/js/adminlte.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dashboard2.js"></script>
<script src="<?php echo base_url() ?>assets/js/demo.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/icheck.min.js"></script>
<!-- CK Editor -->
<script src="<?php echo base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.min.js"></script>
