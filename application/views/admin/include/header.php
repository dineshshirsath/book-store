<?php if(empty($this->session->userdata('user_id'))){redirect(base_url('admin'));} ?>
<!DOCTYPE html>
<html>
	<head>
		  <title>The Book Lounge</title>
		  <meta name="description" content="Thebooklounge.in is ebook publication which has unique feature in which author will be given access to update book on real time basis to keep readers update.">
    <meta name="keywords" content="Thebooklounge.in, gstaudit ebooks for Readers & Authors,law ebooks for Readers & Authors, update book on real-time basis, Special offer for icai and icsi members,icai ebooks,icsi ebooks.">
      <meta charset="UTF-8">
		  <!-- Bootstrap 3.3.7 -->
		  <link rel="icon" href="<?php echo base_url();?>favicon-32x32.png" type="image/x-icon" sizes="32x32"> 
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap/bootstrap.min.css">
		  
		  <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" />
		  <!-- bootstrap datepicker -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-datepicker.min.css">      
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/ionicons.min.css">
		  <!-- jvectormap -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/jquery-jvectormap.css">
		  <!-- Theme style -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/_all-skins.min.css">
      <!-- datatable bootstrap -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dataTables.bootstrap.min.css">
      <!-- Google Font -->
		  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
      <!-- bootstrap wysihtml5 - text editor -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css">	
      
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/iCheck.css">
    </head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
  			<header class="main-header">

    		<!-- Logo -->
		    <a href="<?php echo base_url(); ?>admin-dashboard" class="logo">
		      	<!-- mini logo for sidebar mini 50x50 pixels -->
		      	<span class="logo-mini"><b>B</b>L</span>
		      		<!-- logo for regular state and mobile devices -->
		      	<span class="logo-lg"><b>The Book Lounge</b></span>
		    </a>

    		<!-- Header Navbar: style can be found in header.less -->
    		<nav class="navbar navbar-static-top">
      		<!-- Sidebar toggle button-->
			      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			        	<span class="sr-only">Toggle navigation</span>
			      </a>
			      <!-- Navbar Right Menu -->
			      <div class="navbar-custom-menu">
			      	 	<ul class="nav navbar-nav">
			         	<!-- Messages: style can be found in dropdown.less-->
          					<!-- <li class="dropdown messages-menu">
					            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					              	<i class="fa fa-envelope-o"></i>
					              	<span class="label label-success">4</span>
					            </a>
            					<ul class="dropdown-menu">
              						<li class="header">You have 4 messages</li>
              						
					              	<li class="footer"><a href="#">See All Messages</a></li>
					            </ul>
					        </li> -->
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li> -->
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a> -->
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
<?php
  $imageData = $this->CommonFunctionModel->checkProfileImage($this->session->userdata('user_id')); 
?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php if ($imageData[0]['image'] != '0'): ?>
                <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="user-image" alt="User Image">
              <?php else: ?>                  
                <img src="<?php echo base_url() ?>assets/image/profile_Image/profile-picture.jpg" class="user-image" alt="User Image">
              <?php endif ?>
              <span class="hidden-xs"><?php echo $this->session->userdata('user_name'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php if ($imageData[0]['image'] != '0'): ?>
                  <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="img-circle" alt="User Image">
                <?php else: ?>                  
                  <img src="<?php echo base_url() ?>assets/image/profile_Image/profile-picture.jpg" class="img-circle" alt="User Image">
                <?php endif ?>
                <p>
                  <?php echo $this->session->userdata('user_name'); ?>
                  <small></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url(); ?>user-profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url() ?>admin-logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- jQuery 3 -->
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
