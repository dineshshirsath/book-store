<link rel="stylesheet" href="assets/multiple_selection/slimselect.min.css">
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
	    		Author Dashboard
	    	<?php else: ?>
	    		Admin Dashboard
	    	<?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-news-letter">Subscribe Listing</a></li>
	        <li class="active">Send Mail</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 		<?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('success'); ?>
					</div>

				<?php } else if($this->session->flashdata('error')){  ?>

					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('error'); ?>
					</div>

					<?php } ?>
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Send Mail</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url(); ?>store-news-letter-mail" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
					                <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">From</label>
											<input type="text" class="form-control" name="from" id="from" readonly="" value="John.doe@yourdomain.com" placeholder="From">
											<span style="color: red;"><?php echo form_error('from');?></span>
					                	</div>
						            </div>	
						            <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">To</label>
											<select name="to[]" id="multiple" multiple>
												<?php foreach ($newsLetterListingData as $row): ?>
													<option value="<?php echo $row['email']; ?>"><?php echo $row['email']; ?></option>		
												<?php endforeach ?>
											</select>
											<span style="color: red;"><?php echo form_error('to');?></span>
					                	</div>
						            </div>	
						            <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Subject</label>
											<input type="text" class="form-control" name="subject" id="subject" value="<?php echo set_value("subject"); ?>" placeholder="Subject">
											<span style="color: red;"><?php echo form_error('subject');?></span>
					                	</div>
						            </div>	
						            <div class="col-md-12">	    
						                <div class="form-group">
							                <label for="exampleInputPassword1">Message</label>
								            <textarea id="editor01" name="message"><?php echo set_value('message');?>
                                        	</textarea>
                                        	<span style="color: red;"><?php echo form_error('message');?></span>
						                </div>
						            </div>
						                
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Send</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="assets/multiple_selection/slimselect.min.js"></script>
<script>
      setTimeout(function() {
        new SlimSelect({
          select: '#multiple'
        })
      }, 300)
</script>
