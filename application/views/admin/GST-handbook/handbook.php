<title>The Book Lounge</title>
		  <meta name="description" content="Thebooklounge.in is ebook publication which has unique feature in which author will be given access to update book on real time basis to keep readers update.">
    <meta name="keywords" content="Thebooklounge.in, gstaudit ebooks for Readers & Authors,law ebooks for Readers & Authors, update book on real-time basis, Special offer for icai and icsi members,icai ebooks,icsi ebooks.">
      <meta charset="UTF-8">
		  <!-- Bootstrap 3.3.7 -->
		  <link rel="icon" href="<?php echo base_url();?>favicon-32x32.png" type="image/x-icon" sizes="32x32">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"/>
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/handbook/') ?>/style2.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
<?php 
	
	$chapterId = (!empty($_GET['chapterId']))?$_GET['chapterId']:"";
?>
<link href="<?php echo base_url(); ?>/assets/css/book-index/waKB.css" rel="stylesheet" />
<style>
.menu-group .active {
 	color: #000;
    background: #f4f4f4;
    border-left: 5px solid #1271B4;
    text-decoration: none;
}
.logo-container{
	font-weight: 900;
    text-transform: none;
    font-family: "Catamaran",sans-serif;
    letter-spacing: -1px;
    color: #3b4251;
}
.logo-container h2 span {
    color: #6f9a37;
	/* width: calc(100% - 250px); */
    /* padding: 40px; */

}
.checked{
	left: 0px;
}
.checked_true{
	left: 0px;
}
.checkedTrue{
    display: none;
}

#background{
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:189%;
    color:yellow;
}

#content{
    position:absolute;
    z-index:1;
}

#wotermark
{
    color:#e8e7e7;
    font-size:70px;
    transform:rotate(300deg);
    -webkit-transform:rotate(330deg);
}


button#sidebarCollapse {
    margin-right: 250px;
	margin-top: 5px;
}
.h3, h3 {
    font-size: 1.75rem;
   /* margin-top: -30px;*/
}
.btn-info {
    margin-left: 256px;
}
#content ol,p,span {
   color:black;
}
#content {
	padding-right: 20px;
	font-size:15px;
	font-familly:"open sans",sans-serif !important;
}
#index{ height:100%; overflow:hidden;}
#index:hover{overflow-y:auto;}

#content{ overflow:hidden;}
#content:hover{overflow-y:auto;}

#articleListScroll{ overflow:hidden;}
#articleListScroll:hover{overflow-y:auto;}
.modal-backdrop{position: inherit;}

</style>
<!-- ------Rating CSS-------- -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.rateyo.min.css"/>

<?php if (!empty($_GET['subChapterId'])): ?>
	<style>
	 	.no-sub{
			left:300px;
		}
		.no-sub1{
			left:300px;
		}
		.no-sub2{
			left:75px;
		}
	</style>
<?php else: ?>
		<style>
		 	.no-sub{
				left:530px;
			}
			.no-sub1{
				left:300px;
			}
		</style>
<?php endif ?> 
<?php 
	if (empty($_GET['chapterId']) and empty($_GET['subChapterId'])) {
	 	$margin_left = 'margin-left: -20%;';
	 } 
?>
<body onload="window.location.hash = '<?php echo (!empty($_GET['chapterId']))?$_GET['chapterId']:""; ?>'"></body>
<section class="handbook" >
        <div class="block-service block-service-opt">
            <div class="container">
                <div class="row">				    
                  <app-root ng-version="4.1.3">
                    <app-side-nav id="myDIV" >
                    
         <div class="menu-bar" id="sidebar">
         <button type="button" id="sidebarCollapse" class="btn btn-info" ><i class="fas fa-align-left"></i></button>
         	<div class="logo-container" style="cursor: pointer;">
         		<?php if ($this->session->userdata('user_type') == "u"): ?>
         			<a href="<?php echo base_url();?>" style="color: black;"><h2 style="font-size: 22px; margin-top: -26px;">The Book<span> Lounge</span></h2></a>
         		<?php else: ?>
	        		<a href="<?php echo base_url();?>view-book/<?php echo $_GET['id']; ?>" style="color: black;"><h2 style="font-size: 22px; margin-top: -26px;">The Book<span> Lounge</span></h2></a>
         		<?php endif ?>
			</div>
           <div class="menu-group" >
	          <div class="height100Percent" id="folderListScrollContainer">
	            <div class="ps-container ps ps--theme_default ps--active-y" id="folderListScroll" data-ps-id="a8760a1f-b9db-fe00-114d-d5377fa9e5b3">
	                <!---->
	                <!---->
	                <div class="eachMenuContainerOuter" id="index" style="height: 100%;">
	                	<?php $i = 0; foreach($bookResult as $row) {?> 
		                <a href="<?php echo base_url('handbook').'?id='.$_GET['id'] ?>&<?php echo 'chapterId='.$row['id'] ?> " > 	
		                	<div class="eachMenuContainer <?php echo (empty($chapterId) and $i == 0)?'active':''; ?><?php echo ($row['id'] == $chapterId)?'active':''; ?>" id="<?php echo $row['id'] ?>" >
		                        <!--<div class="eachFolderIcon">
		                            <img src="assets/images/iconIndex.png">
		                        </div>-->
		                        <div class="eachMenuText ellipsisText"><?php echo $row['chapter_name'] ?></div>
		                        <div class="articleCount"></div>
		                        <div class="clr"></div>
		                    </div>
	                    <a>
	                    <?php $i++; } ?>
	                </div>
	            </div>
	        </div>
    	</div>  
        </div>
      </app-side-nav>
<ka-article>
<?php
$display = "";
if(!empty($chapterId)){
	$result = $this->GSTHandBookModel->getSubChapterDetails($chapterId);	
	$display = (count($result) == 0 )?"display:none;":"";
	$margin_left = (count($result) == 0 )?"margin-left: -17%;":"";
}
?>			
  <div class="content-panel" id="content">
    <ka-article-list>
    	<div id="articleListContianer"  style="<?php echo $display; ?>">
        <div class="colQuestions" id="divColQuestions">
           <div class="height_99Per" id="articleListScrollContainer">
           <div class="logo-container" style="cursor: pointer;background-color: #1271B4 !important;color:white;">

           		<!--<button type="button" id="sidebarCollapse" class="btn btn-info" ><i class="fas fa-align-left"></i></button>	-->        	
	        	<h3 style="margin-left: 45px;">Sub-Chapters</h3>
			</div>
              <div class="ps-container ps ps--theme_default ps--active-y" id="articleListScroll" data-ps-id="51a33fb3-52ca-654d-9287-4c6b418c3e28" style="height: 100%;" >
                <?php
                 if(!empty($chapterId)){
                		foreach($result as $subRow){ 
                		?>
                		<a href="<?php echo base_url('handbook').'?id='.$_GET['id'] ?>&<?php echo 'chapterId='.$chapterId ?>&<?php echo 'subChapterId='.$subRow['id'] ?> ">	
		             		<div class="txtQuestion txtQuestion" ka-ngfor-finish="" id="articleRow4210" >
			                    <div style="color:#1271B4;"><?php echo $subRow['sub_chapter_name'] ?></div>
			                        <div class="txtGrey">
			                        <div class="eachFolderName ellipsisText" ka-title=""></div>
			                        <div class="f_left"></div>
			                        <div class="clr"></div>
			                    </div>
			                </div>
		                </a>
		                <?php }
					}
				else{
                 	foreach($bookResult as $row){ 
                	
						$result = $this->GSTHandBookModel->getSubChapterDetails($row['id']);	
						foreach($result as $subRow){ 
                		?>
                		<a href="<?php echo base_url('handbook').'?id='.$_GET['id'] ?>&<?php echo 'chapterId='.$chapterId ?>&<?php echo 'subChapterId='.$subRow['id'] ?> ">	
		             		<div class="txtQuestion txtQuestion" ka-ngfor-finish="" id="articleRow4210">
			                    <div style="color:#1271B4;"><?php echo $subRow['sub_chapter_name'] ?></div>
			                        <div class="txtGrey">
			                        <div class="eachFolderName ellipsisText" ka-title=""></div>
			                        <div class="f_left"></div>
			                        <div class="clr"></div>
			                    </div>
			                </div>
			            </a>
		                <?php }
						} 
					}?>
            </div>   
           </div>
        </div>
</div>
</ka-article-list>
    <router-outlet></router-outlet>
    <ka-article-detail>
    	<?php $subChapterResult = [];$style = ""; $class="";
    	if(!empty($_GET['subChapterId'])){
			$subChapterResult = $this->GSTHandBookModel->getSubChapterDetailsIdWise($_GET['subChapterId']);
			$style = "position: absolute;";
		}
    	if(!empty($chapterId) and empty($_GET['subChapterId']) ){
			$subChapterResult = $this->GSTHandBookModel->getChapterDetailsIdWise($chapterId);
			$style = "position: fixed;";
			$class = "no-sub";	
		}	
		?>
      <div class="eachArticleContainer <?php echo $class;?>" id="articleDetailScrollContainer" style="<?php echo $style;if(!empty($margin_left)){echo $margin_left;} ?>">
         <div class="ps-container ps ps--theme_default ps--active-y" id="articleDetailScroll" data-ps-id="21ed80af-8db1-7d5f-4d3c-77f746821dc7">
         <button type="button" id="sidebarCollapse1" class="btn btn-info" style="margin-left: -10px;<?php echo $display; ?>"><i class="fas fa-align-left"></i></button>
         <button type="button" id="" class="btn btn-info" style="padding: 3px 10px; float:right; margin-right:10px;<?php echo $display; ?>" data-toggle="modal" data-target="#modal-default">Rate This Book</button>

     <div class="modal fade" id="modal-default" style="position: absolute;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            
            <h4 class="modal-title">Rate This Book !!!</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" action="rating" method="POST"  role="form" id="rating" >
              <div class="form-group">
                <div class="col-sm-12">
                  <input type="hidden" class="form-control" name="book_id" id="id" value="<?php echo $_GET['id']; ?>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                  <input type="hidden" class="form-control" name="rating" id="ratingId">
                </div>
              </div>
              <div class="col-sm-12">
                <div id="rateYo"></div>
              </div>            
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" id="submit_rating" class="btn btn-primary">Done</button>
            </div>
          </form>
			<?php
			if(!empty($_GET['msg']) && $_GET['msg']=='suc'){
				echo '<script language="javascript">';
				echo 'alert("Thanks For Rating !!!")';
				echo '</script>';
			}
			if(!empty($_GET['msg']) && $_GET['msg']=='fal'){
				echo '<script language="javascript">';
				echo 'alert("You Have Already add Rating !!!")';
				echo '</script>';
			} 
			?>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
            <?php				
            	foreach($subChapterResult as $subChapterRow){
            ?>
            <div id="articleDetail">
                <div class="pad_right_10">
	                 <div>
	                    <div class="eachArticleIconsContainer">
	                       <div class="iconExpand f_right" id="btnExpendArticle" title="Expand Article View"></div>
	                    </div>
	                </div>
	                <div class="eachArticleHeader">
	                    <div class="verticalAlignMiddle">
	                        <h1><?php echo (!empty($subChapterRow['sub_chapter_name']))?$subChapterRow['sub_chapter_name']:$subChapterRow['chapter_name']; ?></h1>
	                        <div class="txtGrey">
	                            <span></span>
	                        </div>
	                    </div>
	                </div>
	                <div class="noArticle-heading" hidden=""></div>
	                <div class="article-detail-content-panel disp_none" style="display: block;">
	                    <span id="background">
						 	<p id="wotermark">The Book Lounge</p>
						</span>

						<div id="content" style="height:84%; width:98.5%;"> 
							<p>
								<?php echo $subChapterRow['content'] ?>
							</p>
							<br>
							<h3>	
								<?php echo $subChapterRow['remark'] ?>
							</h3>
							<p style="margin-left: 70px;">
								<?php echo $subChapterRow['remark_description'] ?>
							</p>
							<h3>	
								<?php echo $subChapterRow['explanation_1'] ?>
							</h3>
							<p style="margin-left: 70px;">
								<?php echo $subChapterRow['explanation_description_1'] ?>
							</p>
							<h3>	
								<?php echo $subChapterRow['explanation_2'] ?>
							</h3>
							<p style="margin-left: 70px;">
								<?php echo $subChapterRow['explanation_description_2'] ?>
							</p>
						</div>
	                </div>
	            </div>
	        </div>
	        <?php } ?>
        </div>
   
									    <wa-spinner>
										    <div class="wa-hide">
										        <div class="spinnerOverlay"></div>
										        <div class="spinnerBg spinner-middle">
										            <div class="spinner"></div>
										        </div>
										    </div>
									    </wa-spinner>
									</div>
									<div class="botSearchContainer">
									<div class="botTextarea" id="txtSendMessage" style="padding-top: 15px;margin-left: -30px;">
   &copy; <?php echo date("Y"); ?> All Rights Reserved by <a href="#" target="_blank" style="color: green;">FalconX.in</a>. Design by <a href="http://orbs-solutions.com/" target="_blank" style="color: green;"> Orbs Solutions</a>
   		<div style="float: right;">
   			<?php if ($this->session->userdata('user_type') == 'u'): ?>
   				<a href="<?php echo base_url(); ?>">Home</a>
	    		<b>|</b>
	    		<a href="<?php echo base_url(); ?>about">About Us</a>
	    		<b>|</b>
	    		<a href="<?php echo base_url(); ?>contact">Contact Us</a>
	    	<?php else: ?>
	    		<a href="<?php echo base_url(); ?>view-book/<?php echo $_GET['id']; ?>">Back to dashboard</a>
   			<?php endif ?>
    		
    	</div>
   </div>    
</div>
								</ka-article-detail>
							</div>
						</ka-article>
					</app-root>   
                </div>
            </div>
        </div>
        <input type="text" id="secondDiv" name="secondDiv" value="0"/>
        <input type="text" id="firstDiv" name="firstDiv" value="0"/>
        
    </section>   
<!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
            	$('#sidebar').toggleClass('active');
                $('#articleDetailScrollContainer').removeClass('no-sub');
                $('#articleDetailScrollContainer').toggleClass('no-sub1');
            	$('#content').toggleClass('checked');
            	if($('#firstDiv').val() == 0)
            	{
					$('#firstDiv').val(1);
				}else{
					$('#firstDiv').val(0);
				}
				if($('#firstDiv').val() == 0 && $('#secondDiv').val() == 0 )
            	{
					$('#articleDetailScrollContainer').addClass('no-sub');
				}
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse1').on('click', function () {
                $('#articleListContianer').toggleClass('active');
            	$('#articleDetailScrollContainer').removeClass('no-sub');
            	$('#articleDetailScrollContainer').toggleClass('checked_true');
            	$('#articleDetailScrollContainer').toggleClass('no-sub1');
            	$('#articleDetailScrollContainer').toggleClass('no-sub2');
            	$('#articleListContianer').toggleClass('checkedTrue');
            	if($('#secondDiv').val() == 0)
            	{
					$('#secondDiv').val(1);
				}else{
					$('#secondDiv').val(0);
				}
				if($('#firstDiv').val() == 0 && $('#secondDiv').val() == 0 )
            	{
            		$('#articleDetailScrollContainer').addClass('no-sub');
				}
            });
        });
    </script>

<!--   -->
<!-- <script type="text/javascript" src="./jquery.min.js"></script> --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.rateyo.js"></script>

<script type="text/javascript">
 
$(function () {
 
  $("#rateYo").rateYo({
 
    onSet: function (rating, rateYoInstance) {
      var rating_amaunt = rating;
      //alert("Rating is set to: " + rating_amaunt);
      $('#ratingId').val(rating_amaunt);
    }
  });
});

    
</script>

 <!--    
  <script>
	$(document).ready(function(){
        // click on button submit
        $("#submit_rating").on('click', function(){
            // send ajax
           var data = $("#rating").serialize();
            $.ajax({
                url: "<?php //echo base_url();?>/website/RatingReviewController/index", // url where to submit the request
                type : "POST", // type of action POST || GET
                //dataType : 'json', // data type
                data : data, // post data || get data
                success : function(result) {
                    // you can see the result from the console
                    // tab of the developer tools
                    alert('Thanks For Rating !!!');
                    //console.log(result);
                },
                error: function(xhr, resp, text) {
                    //console.log(xhr, resp, text);
                    alert('Problme to give Rating !!!');
                }
            })
        });
    });
</script>
 -->