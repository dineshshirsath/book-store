<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Book Information Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
          <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
		   <?php echo $this->session->flashdata('success'); ?>
		</div>

		<?php } else if($this->session->flashdata('error')){  ?>
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert">&times;</a>
		    <?php echo $this->session->flashdata('error'); ?>
		</div>
		<?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Book Listing</h3>
                  <a style="float: right;" href="<?php echo base_url(); ?>add-product" class="btn btn-primary">
                    <span>
                        Add New Book
                    </span>
                    </a> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="Category_table">
                    <thead>
                        <th>Sr No</th>

                        <th>Title</th>
                        <th>Author</th>
                        <th>Price (₹)</th>
                        <th>Image</th>
                        <?php if ($this->session->userdata('user_type') == 'a'): ?>
                            <th>Category Name</th>
                            <th>Sub Category Name</th>
                        <?php endif ?>                            
                            <th>Action</th>
                    </thead>
                    <tbody>
                        <?php $i = 1;
                        foreach($productListingData as $row)
                        {?>
                        <tr>
                            <td><?php echo $i++;?></td>                            
                            <td><?php echo $row['title'];?></td>
                            <td><?php echo $row['authour'];?></td>
                            <td><?php echo '₹ '.$row['price'];?></td>
                            <td><img style="width: 50px; height:70px" src="<?php base_url(); ?>assets/image/book/<?php echo $row['image'];?>" alt=""></td>
                            <?php if ($this->session->userdata('user_type') == 'a'): ?>
                                <td><?php echo $row['category_name'];?></td>
                                <td><?php echo $row['sub_category_name'];?></td>
                            <?php endif ?>                            
                               <td>
                                <div class="action-btns">
                                    <a href="<?php echo base_url(); ?>edit-product/<?php echo $row['id']; ?>" title="Edit" class="btn-lg edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="<?php echo base_url(); ?>delete-product/<?php echo $row['id']; ?>" title="Delete" class="btn-lg delete-btn" data-toggle="modal"><i class="fa fa-trash-o fa-lg" aria-hidden="true" onClick="return doconfirm();"></i></a>
                                </div>
                            </td> 
                        </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#Category_table').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,5],
                    "orderable":false,
                },
            ],
          });
      })
      
function doconfirm()
{
    job=confirm("Are you sure to delete record permanently?");
    if(job!=true)
    {
        return false;
    }
}
</script>