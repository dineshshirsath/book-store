<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css"/>
<style>
	#preview {
	    margin-top: 10px;
	}
	#preview img {
		max-width: 100%;
		border: solid #cdcdcd 1px;
		padding: 5px;
		border-radius: 3px;
		-webkit-box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, .2);
		box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, .2);
		overflow: hidden;
		width: 160px;
		height: 150px;
	}
</style>
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-product">Book Listing</a></li>
	        <li class="active">Add New Book</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 	<div class="col-md-12">
				        <?php if($this->session->flashdata('success')){ ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } else if($this->session->flashdata('error')){  ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('error'); ?>
						</div>
						<?php } ?>
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Add New Book</h3>
				            </div>
				            <div class="box-body">
				            	<div class="col-md-12">
				                	<div class="form-group">
				                  		<label class="control-label">Type</label>
				                		<select name="category_id" id="typeCategory" class="form-control" onchange="getPaymentMethod(this.value)">
											<option value="">-----Select Type-----</option>
											<option value="classes" selected="">Classes</option>
											<option value="book">Book</option>
										</select>
				                	</div>
					            </div>
					        <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url(); ?>store-pdf-file" method="post" enctype="multipart/form-data" role="form" id="ClassesForm" style="display: block;">
					            <div id="pdfUploadDiv">
				              	    <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">PDF Name</label>
											<input type="text" class="form-control name_input" name="name[]" id="name_0" value="" placeholder="PDF Name">
					                	</div>
						            </div>	
						            		         
						            <div class="col-md-12">	    
						                <div class="form-group">
							                <label for="exampleInputPassword1">Classes PDF &nbsp;&nbsp;&nbsp;<small style="color: #159203;">[ Please choose only PDF file ]</small></label>      
							                <!--<input type="file" class="form-control" name="book_pdf" id="book_pdf" value="<?php echo set_value('book_pdf');?>"/>-->
						                	<input id="uploadImage_0" class="fileVal" type="file" name="book_pdf[]"  onchange="document.getElementById('preview_0').src = window.URL.createObjectURL(this.files[0])"/>
											<div id="preview"><img id="preview_0" src="<?php echo base_url('assets/image/'); ?>filed.png"/></div><br>
										</div>
						            </div>							            
								</div>
								<input type="hidden" id="inc" name="inc" value="0"/>
								<div class="col-md-12">
									<div class="row" >
										<div style="float: right;">
							            	<button id="btAdd" type="button" class="btn btn-primary" onclick=""><i class="fa fa-plus"></i>&nbsp;&nbsp;Add More</button>&nbsp;&nbsp;&nbsp;&nbsp;   
							            </div>
						            </div>
		              			</div>	
								<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>
				            <form action="<?php echo base_url(); ?>store-product" method="post" enctype="multipart/form-data" role="form" id="BookForm" style="display: none;">
				              		<?php if ($this->session->userdata('user_type') == 'a'): ?>		              			
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Category Name</label>
											<select name="category_id" id="category_id" class="form-control">
												<option value="">-----Select Category Name-----</option>
												<?php foreach ($CategoryData as $row): ?>
												<option value="<?php echo $row['id'] ?>"><?php echo $row['category_name'] ?></option>
												<?php endforeach ?>
											</select>
											<span style="color: red;"><?php echo form_error('category_id');?></span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Sub Category Name</label>
											<select name="sub_category_id" id="sub_category_id" class="form-control">
												<option value="">-----Select Sub Category Name-----</option>
											</select>
										</div>
									</div> 
				              		<?php endif ?>
									
						            <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Book Title</label>
											<input type="text" class="form-control" name="title" id="title" value="<?php echo set_value('title');?>" placeholder="Title">
											<span style="color: red;"><?php echo form_error('title');?></span>
					                	</div>
						            </div>	
						            <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Author</label>
<input type="text" readonly="" class="form-control" name="authour" id="authour" <?php if (!empty($authourData)): ?> value="<?php echo $authourData[0]['first_name']." ".$authourData[0]['last_name']?>"<?php endif ?> >
											<span style="color: red;"><?php echo form_error('authour');?></span>
					                	</div>
						            </div>
						            <!-- <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Publisher</label>
											<input type="text" class="form-control" name="publisher" id="publisher" value="<?php echo set_value('publisher');?>" placeholder="Publisher">
											<span style="color: red;"><?php echo form_error('publisher');?></span>
					                	</div>
						            </div>	 -->
					            	<div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Price (₹)</label>
											<input type="text" class="form-control" name="price" id="price" value="<?php echo set_value('price');?>" placeholder="Price">
											<span style="color: red;"><?php echo form_error('price');?></span>
					                	</div>
						            </div>
						            <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Information</label>
                                            <textarea id="editor01" name="book_information" ><?php echo set_value('book_information');?>
                                            </textarea>
                                            <span style="color: red;"><?php echo form_error('book_information');?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Edition</label>
											<input type="text" class="form-control" name="edition" id="edition" value="<?php echo set_value('edition');?>" placeholder="Edition">
											<span style="color: red;"><?php echo form_error('edition');?></span>
					                	</div>
						            </div>	
					            	<div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">SUPC</label>
											<input type="text" class="form-control" name="supc" id="supc" value="<?php echo set_value('supc');?>" placeholder="SUPC">
											<span style="color: red;"><?php echo form_error('supc');?></span>
					                	</div>
						            </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description</label>
                                            <textarea id="editor02" name="book_discription" ><?php echo set_value('book_discription');?>
                                            </textarea>
                                            <span style="color: red;"><?php echo form_error('book_discription');?></span>
                                        </div>
                                    </div>
				              		<?php if ($this->session->userdata('user_type') == 's'): ?>
                                    <div class="col-md-12">
						              	<div class="form-group">
						                	<label class="control-label">Books Have</label>	
							                <div class="form-group">                	
							                  <input type="radio" name="books_have"  value="1" ><label> &nbsp;&nbsp;Multiply volumn with Multiply Chapter</label>
							              	</div>
											<div class="form-group">                	
							                  <input type="radio" name="books_have" value="2"><label>  &nbsp; &nbsp;Multiply Chapter with Multiply Sub- Chapter</label>
							              	</div>
							              	<div class="form-group">               	
							                  <input type="radio" name="books_have" value="3"><label>  &nbsp; &nbsp;One chapter with Multiply Sub- Chapter</label>
							              	</div>
						              	</div>
						          	</div>
				              		<?php endif ?>

				              		<?php if ($this->session->userdata('user_type') == 'a'): ?>			         
						            <div class="col-md-12">	    
						                <div class="form-group">
							                <label for="exampleInputPassword1">Book Image</label>      
						                	<input type="file" class="form-control" name="image" id="image" value="<?php echo set_value('image');?>"/>
						                </div>
						            </div>	
						            <div class="col-md-12">
					                	<div class="form-group">
					                		<label class="control-label">Status</label>
					                  		<input type="radio" name="status" id="status" value="p">Published
					                  		<input type="radio" name="status" id="status" value="up">UnPublished
					                  		<span style="color: red;"><?php echo form_error('status');?></span>
					                	</div>
						            </div>
				              		<?php endif ?>
													              	
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>
				            </div>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>

<script>

	$(document).ready(function(){
 	$('#category_id').change(function(){ //any select change on the dropdown with id country trigger this code
 	$("#sub_category_id > option").remove(); //first of all clear select items
 	var category_id = $('#category_id').val(); // here we are taking country id of the selected one.

 	var url = "<?php echo base_url(); ?>admin/sub-category/SubCategoryController/getSubCategoryById";
 	
 	$.ajax({
 		type: "POST",
 		url: url, 
 		data:{
 			'category_id':category_id
 		},
 		dataType: 'json',
 		success: function(response) 
 		{  
 		   	// Remove options 
          	$('#subcategory_id').find('option').not(':first').remove();
          	// Add options
          	$.each(response,function(index,data){
          	if(data != '')
          	{
             	$('#sub_category_id').append('<option value="'+data['id']+'">'+data['sub_category_name']+'</option>');
          	}
          	else{
          		$('#sub_category_id').append('<option value="">--Select Sub Category --</option>');
          	}
          });
 			
 		}
 
 		});
 
 	});
 });

</script>
<script>
function getPaymentMethod(value)
{
	$('#payuForm').hide();	
	if(value == "classes"){
		$('#ClassesForm').show();	
		$('#BookForm').hide();
	}else if(value == "book"){
		$('#ClassesForm').hide();	
		$('#BookForm').show();
	}
}
</script>
<script>
/*function readURL(input,i) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview_'+i).attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

$("#uploadImage_0").change(function(){
    inc = $('#inc').val();
    readURL(this,inc);
});*/
</script>

<script>
$(document).ready(function() {
	$('#btAdd').click(function(){
		var i = $('#inc').val();
		if(i <= 5){
			i = parseInt(i) + 1; 
			preId = "'preview_"+i+"'";
			var ImageUrl = "<?php echo base_url('assets/image/'); ?>";
			html = '<div class="col-md-12"><div class="form-group"><label class="control-label">PDF Name</label><input type="text" class="form-control name_input" name="name[]" id="name_'+i+'" value="" placeholder="PDF Name"></div></div><div class="col-md-12"><div class="form-group"><label for="exampleInputPassword1">Classes PDF &nbsp;&nbsp;&nbsp;<small style="color: #159203;">[ Please choose only PDF file ]</small></label><input id="uploadImage_'+i+'" type="file" class="fileVal" name="book_pdf[]"  onchange="document.getElementById('+preId+').src = window.URL.createObjectURL(this.files[0])"/><div id="preview"><img id="preview_'+i+'" src="'+ImageUrl+'filed.png" /></div><br></div></div>';
			$('#pdfUploadDiv').append(html);	
			$('#inc').val(i + 1);
			
		}else{
			
		}
	});
});
</script>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script>
$('form#ClassesForm').on('submit', function(event) {
        //Add validation rule for dynamically generated name fields
    ignore:'',
    $('.name_input').each(function() {
        $(this).rules("add", 
            {
                required: true,
                messages: {
                    required: "Name is required",
                }
            });
    });
    //Add validation rule for dynamically generated email fields
    
});
$("#ClassesForm").validate();
</script>-->

<script type="text/JavaScript">
$(document).ready(function(){
	$('.fileVal').change(function(e) {
        var files = e.originalEvent.target.files;
        for (var i=0, len=files.length; i<len; i++){
            var n = files[i].name,
                s = files[i].size,
                t = files[i].type;
            var fextension = n.substring(n.lastIndexOf('.')+1);
            var validExtensions = ["pdf"];
			if ($.inArray(fextension, validExtensions) == -1){
                alert("This type of files are not allowed! Please select only pdf file.");
                this.value = "";
                return false;
            }
        }
    });	
})

</script>