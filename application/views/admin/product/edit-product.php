<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css"/>
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-product">Book Listing</a></li>
	        <li class="active">Update Book</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 		<?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } else if($this->session->flashdata('error')){  ?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('error'); ?>
					</div>
					<?php } ?>
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Update Book</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url().ADMIN.PRODUCT; ?>ProductController/update" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
					                <div class="col-md-12">
					                	<div class="form-group">
					                	<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $producUpdatData[0]['id'] ?>">
					                	<input type="hidden" class="form-control" name="addedby_id" id="addedby_id" value="<?php echo $producUpdatData[0]['addedby_id'] ?>">
					                	</div>
					                </div>
					               
					               	<?php if ($this->session->userdata('user_type') == 'a'): ?>	
					                <div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Sub Category Name</label>
<select name="category_id" id="category_id" class="form-control">
	<option value="">-----Select Category Name-----</option>
	<?php foreach ($CategoryData as $row): ?>
		<?php if ($producUpdatData[0]['category_id'] == $row['id']): ?>
		<option selected="" value="<?php echo $row['id'] ?>"><?php echo $row['category_name'] ?></option>
		<?php else: ?>
		<option value="<?php echo $row['id'] ?>"><?php echo $row['category_name'] ?></option>		
		<?php endif ?>	
	<?php endforeach ?>
</select>	
										</div>
						            </div>
						            <div class="col-md-12">
<div class="form-group">
	<label class="control-label">Sub Category Name</label>
	<select name="sub_category_id" id="sub_category_id" class="form-control">
		<option value="">-----Select Sub Category Name-----</option>
	</select>
</div>
						            </div>
				              		<?php endif ?>
									<div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Title</label>
											<input type="text" class="form-control" name="title" id="title" value="<?php echo $producUpdatData[0]['title'] ?>" placeholder="Title">
											<span style="color: red;"><?php echo form_error('title');?></span>
					                	</div>
						            </div>	
						            <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Authour</label>
											<input type="text" readonly="" class="form-control" name="authour" id="authour" value="<?php echo $producUpdatData[0]['authour'] ?>" placeholder="Authour">
											<span style="color: red;"><?php echo form_error('authour');?></span>
					                	</div>
						            </div>
						            <!-- <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Publisher</label>
											<input type="text" class="form-control" name="publisher" id="publisher" value="<?php echo $producUpdatData[0]['publisher'] ?>" placeholder="Publisher">
											<span style="color: red;"><?php echo form_error('publisher');?></span>
					                	</div>
						            </div>	 -->
					            	<div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Price (₹)</label>
											<input type="text" class="form-control" name="price" id="price" value="<?php echo $producUpdatData[0]['price'] ?>" placeholder="Price">
											<span style="color: red;"><?php echo form_error('price');?></span>
					                	</div>
						            </div>
						            <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Information</label>
                                            <textarea id="editor01" name="book_information" ><?php echo $producUpdatData[0]['book_information'] ?>
                                            </textarea>
                                            <span style="color: red;"><?php echo form_error('book_information');?></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Edition</label>
											<input type="text" class="form-control" name="edition" id="edition" value="<?php echo $producUpdatData[0]['edition'] ?>" placeholder="Edition">
											<span style="color: red;"><?php echo form_error('edition');?></span>
					                	</div>
						            </div>	
					            	<div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">SUPC</label>
											<input type="text" class="form-control" name="supc" id="supc" value="<?php echo $producUpdatData[0]['supc'] ?>" placeholder="SUPC">
											<span style="color: red;"><?php echo form_error('supc');?></span>
					                	</div>
						            </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description</label>
                                            <textarea id="editor02" name="book_discription" ><?php echo $producUpdatData[0]['book_discription'] ?>
                                            </textarea>
                                            <span style="color: red;"><?php echo form_error('book_discription');?></span>
                                        </div>
                                    </div>
<?php if ($this->session->userdata('user_type') == 's'): ?>
<div class="col-md-12">
  	<div class="form-group">
    	<label class="control-label">Books Have</label>	
        <div class="form-group">                	
          	<?php if ($producUpdatData[0]['books_have'] == '1'): ?>
          		<input type="radio" checked="" name="books_have"  value="1" ><label> &nbsp;&nbsp;Multiply volumn with Multiply Chapter</label>
          	<?php else: ?>	
          		<input type="radio" name="books_have"  value="1" ><label> &nbsp;&nbsp;Multiply volumn with Multiply Chapter</label>
         	<?php endif ?>
      	</div>
		<div class="form-group">
			<?php if ($producUpdatData[0]['books_have'] == '2'): ?>
          		<input type="radio" checked="" name="books_have" value="2"><label>  &nbsp; &nbsp;Multiply Chapter with Multiply Sub- Chapter</label>
          	<?php else: ?>	
          		<input type="radio" name="books_have" value="2"><label>  &nbsp; &nbsp;Multiply Chapter with Multiply Sub- Chapter</label>
         	<?php endif ?>                	
      	</div>
      	<div class="form-group">   
      		<?php if ($producUpdatData[0]['books_have'] == '3'): ?>
        	   <input type="radio" checked="" name="books_have" value="3"><label>  &nbsp; &nbsp;One chapter with Multiply Sub- Chapter</label>
          	<?php else: ?>	
   		       <input type="radio" name="books_have" value="3"><label>  &nbsp; &nbsp;One chapter with Multiply Sub- Chapter</label>
         	<?php endif ?>            	
      	</div>
  	</div>
</div>
									<?php endif ?>
                                    					         
				              		<?php if ($this->session->userdata('user_type') == 'a'): ?>			         
						            <div class="col-md-12">	    
						                <div class="form-group">
							                <label for="exampleInputPassword1">Book Image</label>      
						                	<input type="file" name="image" id="image" class="form-control" value="<?php echo $producUpdatData[0]['image'] ?>"/>
						                </div>
						            </div>	
						            <div class="col-md-12">
					                	<div class="form-group">
					                		<label class="control-label">Status</label>
											<input type="radio" name="status" id="status" value="p" <?php if($producUpdatData[0]['status']=="p") {echo "checked";} ?>>Published
											<input type="radio" name="status" id="status" value="up" <?php if($producUpdatData[0]['status']=="up") {echo "Single";} ?>>UnPublished
					                  		<span style="color: red;"><?php echo form_error('status');?></span>
					                	</div>
						            </div>
				              		<?php endif ?>
						        </div>    				                
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Update</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>

<script>

	$(document).ready(function(){
 	$('#category_id').change(function(){ //any select change on the dropdown with id country trigger this code
 	$("#sub_category_id > option").remove(); //first of all clear select items
 	var category_id = $('#category_id').val(); // here we are taking country id of the selected one.

 	var url = "<?php echo base_url().ADMIN.ADMIN_SUB_CATEGORY; ?>SubCategoryController/getSubCategoryById";
 	
 	$.ajax({
 		type: "POST",
 		url: url, 
 		data:{
 			'category_id':category_id
 		},
 		dataType: 'json',
 		success: function(response) 
 		{  
 		   	// Remove options 
          	$('#subcategory_id').find('option').not(':first').remove();
          	// Add options
          	$('#sub_category_id').append('<option value="">-- Select Sub Category --</option>');
          	$.each(response,function(index,data){          		
          	if(data != '')
          	{
             	$('#sub_category_id').append('<option value="'+data['id']+'">'+data['sub_category_name']+'</option>');
          	}else{
          		$('#sub_category_id').append('<option value="">-- Select Sub Category --</option>');
          	}
          });
 			
 		}
 
 		});
 
 	});
 });

</script>