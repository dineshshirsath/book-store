<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Author Book Ratio Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Author Book Ratio Listing</h3>
                  <a style="float: right;" href="<?php echo base_url(); ?>add-author-order-ratio" class="btn btn-primary">
                    <span>
                        Add Author Book Ratio
                    </span>
                    </a> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Author Name</th>
                        <th>Mobile</th>
                        <th>E-Mile</th>
                    </thead>
                    
                        <?php $inc = 1; 
                        foreach($authorOrderRatioData as $row)
                        {?>
                        <tr>
                            <td><?php echo $inc++;?></td>
                            <td><a href="<?php echo base_url(); ?>details-author-order-ratio/<?php echo $row['id']; ?>" title="Author Book Ratio Details"><?php echo $row['first_name'];?> <?php echo $row['last_name'];?></a></td>
                            <td><?php echo $row['mobile'];?></td>
                            <td><?php echo $row['email'];?></td>
                        </tr>
                        <?php }
                        ?>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,3],
                    "orderable":false,
                },
            ],
          });
      });
</script>
