<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css"/>
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-author-order-ratio">Author Order Ratio Listing</a></li>
	        <li class="active">Add Author Order Ratio</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 	<div class="col-md-12">
				        <?php if($this->session->flashdata('success')){ ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } else if($this->session->flashdata('error')){  ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
						   <?php echo $this->session->flashdata('error'); ?>
						</div>
						<?php } ?>
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Add Author Order Ratio</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url(); ?>store-author-order-ratio" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
<div class="col-md-12">
	<div class="form-group">
		<label class="control-label">Author Name</label>
		<select name="author_id" id="author_id" class="form-control">
			<option value="">--- Select Author Name ---</option>
			<?php foreach ($authorData as $row): ?>
			<option value="<?php echo $row['id'] ?>"><?php echo $row['first_name']." ".$row['last_name'] ?></option>
			<?php endforeach ?>
		</select>
		<span style="color: red;"><?php echo form_error('author_id');?></span>
	</div>
</div>

<div class="col-md-12">
	<div class="form-group">
		<label class="control-label">Book Name</label>
		<select name="book_id" id="book_id" class="form-control">
			<option value="">--- Select Book Name ---</option>
		</select>
		<span style="color: red;"><?php echo form_error('book_id');?></span>
	</div>
</div>
<div class="col-md-12">
	<div class="form-group">
		<label class="control-label">Book Price</label>
		<div class="input-group">
	        <span class="input-group-addon">&#8377;</span>
			<input class="form-control" type="text" name="book_price" id="book_price" readonly="" placeholder="Book Price">
	 		<span class="input-group-addon">.00</span>
		</div>
		<span style="color: red;"><?php echo form_error('book_price');?></span>
	</div>
</div>
<div class="col-md-12">
	<div class="form-group">
		<label class="control-label">Author Ratio <span style="color:green;">(%)</span></label>
		<div class="input-group">
	        <span class="input-group-addon">&#8377;</span>
			<input class="form-control" type="text" name="author_ratio" id="author_ratio" placeholder="Author Ratio" onKeyPress="edValueKeyPress()" onKeyUp="edValueKeyPress()">
	 		<span class="input-group-addon">%</span>
		</div>
		<span style="color: red;"><?php echo form_error('author_ratio');?></span>
	</div>
</div> 
<div class="col-md-12">
	<div class="form-group">
		<label class="control-label">Author Total Amount</label>
		<div class="input-group">
	        <span class="input-group-addon">&#8377;</span>
			<input class="form-control" type="text" name="author_amount" id="author_amount" placeholder="Author Amount" readonly="">
	 		<span class="input-group-addon">.00</span>
		</div>
		<span style="color: red;"><?php echo form_error('author_amount');?></span>
	</div>
</div>
									
						        </div>    			              	
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>

<script>
	$(document).ready(function(){
 	$('#author_id').change(function(){ //any select change on the dropdown with id country trigger this code
 	$("#book_id > option").remove(); //first of all clear select items
 	var author_id = $('#author_id').val(); // here we are taking country id of the selected one.

 	var url = "<?php echo base_url(); ?>admin/product/productController/getBookById";
 	
 	$.ajax({
 		type: "POST",
 		url: url, 
 		data:{
 			'author_id':author_id
 		},
 		dataType: 'json',
 		success: function(response) 
 		{  
 		   	// Remove options 
          	$('#subcategory_id').find('option').not(':first').remove();
          	// Add options
          	$('#book_id').append('<option value="">--- Select Book Name ---</option>');
          	$.each(response,function(index,data){
          	if(data != '')
          	{
             	$('#book_id').append('<option value="'+data['id']+'">'+data['title']+'</option>');
          	}
          });
 			
 		}
 
 		});
 
 	});
 });
</script>
<script>
	$(document).ready(function(){
 	$('#book_id').change(function(){ 
 	//$("#book_price > option").remove();
 	var book_id = $('#book_id').val();
 	var url = "<?php echo base_url(); ?>admin/product/productController/getBookPriceById";
 	
 	$.ajax({
 		type: "POST",
 		url: url, 
 		data:{
 			'book_id':book_id
 		},
 		dataType: 'json',
 		success: function(response) 
 		{  
            $('#book_price').val();

          	$.each(response,function(index,data){
          	if(data != '')
          	{
             	$('#book_price').val(data['price']);
          	}
          	else{
             	$('#book_price').val();
          	}
          });
 			
 		}
 
 		});
 
 	});
 });
</script>
<script>
    function edValueKeyPress()
    {
        var book_price = document.getElementById("book_price");
        var author_ratio = document.getElementById("author_ratio");
        var price = book_price.value;
        var ratio = author_ratio.value;
    	var perc="";

    	if(isNaN(price) || isNaN(ratio)){
            perc=" ";
        }else{
           perc = ((ratio / 100) * price);
        }

        $('#author_amount').val(perc);

    }
</script> 