<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url(); ?>listing-author-order-ratio"><i class="active"></i> Author Book Ratio Listing</a></li>
            <li class="active">Author Book Ratio Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Author Book Ratio Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Book Name</th>
                        <th>Book Price</th>
                        <th>Author Ratio</th>
                        <th>Author Amount</th>
                    </thead>
                    
                        <?php $inc = 1; 
                        foreach($authorOrderRatioDetails as $row)
                        {?>
                        <tr>
                            <td><?php echo $inc++;?></td>
                            <td><?php echo $row['title'];?></td>
                            <td><?php echo $row['book_price'].' '.'/-';?></td>
                            <td><?php echo $row['author_ratio'].' '.'%';?></td>
                            <td><?php echo $row['author_amount'].' '.'/-';?></td>
                        </tr>
                        <?php }
                        ?>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,4],
                    "orderable":false,
                },
            ],
          });
      });
</script>
