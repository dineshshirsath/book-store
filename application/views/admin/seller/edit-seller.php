<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css"/>
<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        <?php if ($this->session->userdata('user_type') == 's'): ?>
                Author Dashboard
            <?php else: ?>
                Admin Dashboard
            <?php endif ?>
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>listing-author">Author Listing</a></li>
	        <li class="active">Update Author</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 		<?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } else if($this->session->flashdata('error')){  ?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('error'); ?>
					</div>
					<?php } ?>
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Update Author</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url().ADMIN.SELLER; ?>SellerController/update" method="post" role="form">
				              	<div class="box-body">
					                <div class="col-md-12">
					                	<div class="form-group">
					                	<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $sellerUpdatData[0]['id'] ?>">
					                	</div>
					                </div>					                	
						            <div class="col-md-12">
					                	<div class="form-group">
					                		<label class="control-label">Status</label>
											<input type="radio" name="status" id="status" value="0" <?php if($sellerUpdatData[0]['status']=="0") {echo "checked";} ?>>Pending
											<input type="radio" name="status" id="status" value="1" <?php if($sellerUpdatData[0]['status']=="1") {echo "Single";} ?>>Aprovel
					                  		<span style="color: red;"><?php echo form_error('status');?></span>
					                	</div>
						            </div>
						        </div>    				                
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>
