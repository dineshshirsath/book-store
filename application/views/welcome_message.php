  <style type="text/css">
      .fa-star{color: #ffb300; font-size: 14px;}
  </style>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.rateyo.min.css"/>
  <!-- <script type="text/javascript" src="<?php //echo base_url();?>assets/js/jquery.min.js"></script> -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.rateyo.js"></script>
  <!--Slider-->
    <section>
        <div id="owl-demo" class="owl-carousel">
            <div class="item item2" style="background-image: url(<?php echo base_url();?>assets/image/book/slider/slider1.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="description">
                             <span class="title" style="color: white;font-size: 31px;line-height: 2;">The smartest way to stay updated with knowledge.
<br>The Coming Age of staying updated
</span>
                            <!--<span class="subtitle">Offer 20% Off</span>
                            <span class="des"> Best sound ready only for you. Take your time and choose your perfect Tv!</span>-->
                            <a href="<?php echo base_url(); ?>register" class="btn">User Register now</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item item2" style="background-image: url(<?php echo base_url();?>assets/image/book/slider/book.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="description">
                            <span class="title" style="color: white;font-size: 31px;line-height: 2;">The smartest way to stay updated with knowledge. 
</span>
                            <!--<span class="subtitle" style="color: white;">The smartest way to stay updated with knowledge.
"The Coming Age of staying updated"</span>
                            <span class="des" style="color: white;"> Best headphone ready only for you. Take your time and choose your perfect headphone!</span>-->
                            <a href="<?php echo base_url(); ?>register" class="btn">User Register now</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item item1" style="background-image: url(<?php echo base_url();?>assets/image/book/slider/slider3.png);">
                <div class="container">
                    <div class="row">
                        <div class="description">
                            <span class="title" style="color: white;font-size: 31px;line-height: 2;"> The Coming Age of staying updated
</span>
                            <!--<span class="subtitle">TV Lcd 40% Off</span>
                            <span class="des"> Best Lcd ready only for you. Take your time and choose your perfect tv!</span>-->
                            <a href="<?php echo base_url(); ?>register" class="btn">User Register now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!--TOP SERVICES-->

    <section class="home5">
        <div class="block-service block-service-opt">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="item">

                            <div class="col-md-12 simbol-desc">
                               <p class="title">24/7 hours online</p>
                                <p>24/7 All books available </p>
                            </div>
                            <div class="col-md-12 simbol-ft"><i class="fa fa-life-ring" aria-hidden="true"></i></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="item">

                            <div class="col-md-12 simbol-desc">
                                <p class="title">90 Days free </p>
                                <p>First 90 days free for all books</p>
                            </div>
                            <div class="col-md-12 simbol-ft"><i class="far fa-clock"></i></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="item">

                            <div class="col-md-12 simbol-desc">
                                <p class="title">24/7 support</p>
                                <p>online Consultations</p>
                            </div>
                            <div class="col-md-12 simbol-ft"><i class="fa fa-headphones" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--FEATURED PRODUCTS-->
    <section class="container">
        <div class="row">
            <div class="col-lg-12 text-center shop-title-store">
                <h1 class="section-title">FEATURED <span>BOOKS</span></h1>
                <!--<p class="sub-title">Excepteur sint occaecat cupidatat non proident sunt<br>in culpa officia deserunt</p>-->
            </div>
            <div class="col-lg-12 content-store">
                <div class="">
                    
                    <?php $pdfData = $this->ProductModel->getPdfData(); ?>

                    <?php $ProductData = $this->ProductModel->getProductData(); ?>
                                        
                    <?php 
                        $result = array_merge($pdfData, $ProductData);
                        foreach ($result as $row):
                        $show = ""; 
                        $url = "product_detail";
                        $checkUrl = $this->CommonFunctionModel->checkBook($row['addedby_id']);
                        if( $checkUrl > 0 )
                        {
                        	$url = "handbook";
    						// if($this->session->userdata('user_name')==''){
    						// 	$show = "display:block";
    						// }
    						if($this->session->userdata('user_name')!=''){
    							
    							$checkUser = $this->CommonFunctionModel->checkUser();
    							
    							$expiryDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($checkUser->created)) . " +90 day"));
    							$first_date = strtotime($expiryDate);
    							$second_date = strtotime(date('Y-m-d'));
    							if($first_date <= $second_date){
    								$url = "book-purchase-message";
    							}
    						}
    					}
                    ?>
                    <?php if (!empty($row['name'])): ?>
                        <div class="col-md-2 my-shop-animation">
                            <!--<div class="top-discount-book">30%</div>-->
                            <div class="box-prod group-book">
                             
                              <a href="<?php echo base_url();?>assets/PDF_File/<?php echo $row['book_pdf'] ?>" target="_blank" style="cursor:pointer" >
                                <div class="box-img-book">
                                    <div class="book-cover-shadow"></div>
                                    <a href="<?php echo base_url();?>assets/PDF_File/<?php echo $row['book_pdf'] ?>" target="_blank" style="cursor:pointer"><img id="preview_0" src="<?php echo base_url('assets/image/'); ?>filed.png" style="max-width: 100%; height: 40%;"/></a>
                                    <div class="box-btn-shop">   
                                        <div class="btn btn-det-cart bt-img"><i class="fa fa-list"></i></div>   
                                    </div>
                                </div>
                                </a>
                                <h2 class="title-book"><?php echo $row['name']; ?></h2>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="col-md-2 my-shop-animation">
                            <!--<div class="top-discount-book">30%</div>-->
                            <div class="box-prod group-book">
                             
                              <a href="<?php echo base_url().$url; ?>?id=<?php echo $row['id'] ?>" >
                                <div class="box-img-book">
                                    <div class="book-cover-shadow"></div>
                                <img src="<?php echo base_url();?>assets/image/book/<?php echo $row['image'] ?>" >
                                    <div class="box-btn-shop">   
                                        <div class="btn btn-det-cart bt-img"><i class="fa fa-list"></i></div>   
                                    </div>
                                </div>
                                </a>
                                <h2 class="title-book"><?php echo $row['title']; ?></h2>

                                <p class="author-txt"><?php echo $row['authour']; ?></p>

                                <p class="category-txt"><?php echo $row['edition']; ?></p>
                                <?php
                                 $rating = round($row['rates'], 1);
                                 if(!empty($rating)){
                                    echo '<p class="author-txt" style="background:#6f9a37; width:50%;color:#fff;padding:0px 5px;">';
                                    echo $rating.' ';
                                    //$rate = $rating;
                                    echo '  <i class="fa fa-star"></i>';
                                    echo '  ('.$row['num_rating'].') </p>';
                                    }else{
                                        if($url != "product_detail"){
                                            echo '<p class="author-txt" style="background:#6f9a37;color:#fff;padding: 0px 5px;">';
                                            echo "No Rating available!";
                                            echo ' <i class="fa fa-star"></i></p>';
                                            }
                                        }
                                  ?> 

                                <p class="book-price">&#8377;<?php echo $row['price']; ?><span>&#8377;150</span></p>
                            </div>
                        </div>    
                    <?php endif ?>
                    
              <?php endforeach ?>
                </div>
            </div>
        </div>
    </section>
