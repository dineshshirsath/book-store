	<style type="text/css">
		.alert-error p{
			color: red;
		}
	</style>
	<section class="content-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-promotion register-box-text">
                        <h1 class="register-top-title tlt">First 90<sup>th</sup> days</h1>
                        <h1 class="register-top-title register-top-second tlt2"> free on all books</h1>
                    </div>
                    <div class="fadeInRight animated abc-book">
                        <img src="<?php echo base_url();?>assets/image/book/slide-book1.png" alt="The Book Lounge" title="The Book Lounge" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-lg-12 table-padding">
                <div class="register-img-background"></div>
                <div id="loginbox" class="mainbox col-md-12 register-box">
                    <div class="panel panel-info">
                        <div>
                            <h3 alt="Create New Author Account" title="Create New Author Account">Create New Author Account</h3>
                        </div>
                        <div class="panel-heading">
                            <div class="panel-title">Register</div>
                        </div>  
                        <div style="padding-top: 20px" class="panel-body">
                            <form name="loginForm" class="form-horizontal regietr-form" method="POST" action="<?php echo base_url(); ?>author-add">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-user"></i>
                                    </span>
                        <input type="text" class="form-control" name="first_name" placeholder="First Name"  value="<?php echo set_value('first_name');?>" autofocus>
                                </div>
                                <?php if(form_error('first_name')){?>
				                   <div class="alert-error" id="1">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(1);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('first_name');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-user"></i>
                                    </span>
                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?php echo set_value('last_name');?>"/>     
                                                  </div>
                                <?php if(form_error('last_name')){?>
				                   <div class="alert-error" id="2">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(2);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('last_name');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-user"></i>
                                    </span>
                                    <input type="text" class="form-control" name="user_name" placeholder="Username" value="<?php echo set_value('user_name');?>" />                               
                                </div>
                                <?php if(form_error('user_name')){?>
				                   <div class="alert-error" id="3">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(3);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('user_name');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-lock"></i>
                                    </span>
                                    <input type="password" class="form-control" name="password" placeholder="Password"/>             
                                </div>
                                <span style="margin-left: 50px;">[ Minimum 8 charecter password required ]</span>
                                <?php if(form_error('password')){?>
				                   <div class="alert-error" id="4">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(4);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('password');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-send"></i>
                                    </span>
                                <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo set_value('email');?>" />                                
                                </div>
                                <?php if(form_error('email')){?>
				                   <div class="alert-error" id="5">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(5);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('email');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-ok"></i>
                                    </span>
                                    <select class="form-control" name="gender">
                                        <option value="">--- Select Gender ---</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <?php if(form_error('gender')){?>
				                   <div class="alert-error" id="6">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(6);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('gender');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-globe"></i>
                                    </span>
                                    <input type="text" class="form-control" name="mobile" placeholder="Mobile No" value="<?php echo set_value('mobile');?>" />
                                </div>
                                <?php if(form_error('mobile')){?>
				                   <div class="alert-error" id="7">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(7);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('mobile');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-globe"></i>
                                    </span>
                                    <input type="text" class="form-control" name="address" placeholder="Address" value="<?php echo set_value('address');?>" />                     
                                </div>
                                <?php if(form_error('address')){?>
				                   <div class="alert-error" id="8">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(8);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('address');?>
				                   </div>
				               <?php } ?>
                                <div class="input-group">
                                    <div class="g-recaptcha" data-sitekey="6LeYsHUUAAAAANew9jyc0xs7nX4J8CLYJCkbgDS3"></div>
                                    <?php if(form_error('g-recaptcha-response')){?>
				                   <div class="alert-error" id="9">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(9);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('g-recaptcha-response');?>
				                   </div>
				               <?php } ?>
                                </div>
                                <div style="margin-top: 20px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls">
                                        <button type="submit" class="btn btn-success login-btn" value="Register" alt="submit" title="submit">Register</button>
                                    </div><br /><br />
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
	function removeErrorMessage(id)
	{
		$('#'+id).html('');
	}
</script>

    