    <section class="content-header" style="margin-bottom: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="img-man" style="border: 3px solid #ccc !important;height: 349px;margin-top: 128px;width: 33%; border-radius: 50%;">
                        <div class="box-prod group-book" style="width: 100%;height: 100%;">
                            <div class="box-img-book">
                                <div class="book-cover-shadow" style="border-radius: 50%;"></div>
                                <?php if ($userData[0]['image'] != '0'): ?>
                                    <img src="<?php echo base_url(); ?>assets/image/profile_Image/<?php echo $userData[0]['image'] ?>" alt="" style="width: 100%; height: 100%; border-radius: 50%;">
                                    <div class="box-btn-shop">
                                        <div class="bt-img"><a class="btn btn-det-cart" data-toggle="modal" data-target="#modal-default" href="#" style="margin-top: -51px;margin-left: 102px;"><i class="fa fa-edit"></i></a></div>
                                    </div>
                                <?php else: ?>
                                    <img src="<?php echo base_url(); ?>assets/image/profile_Image/profile-picture.jpg" alt="" style="width: 100%; height: 100%; border-radius: 50%;">
                                    <div class="box-btn-shop">
                                        <div class="bt-img"><a class="btn btn-det-cart" data-toggle="modal" data-target="#modal-default" href="#" style="margin-top: -51px;margin-left: 102px;"><i class="fa fa-edit"></i></a></div>
                                    </div>
                                <?php endif ?>                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container page-details-product">
        <div class="row">
            <div class="col-md-12 table-padding">
                <div class="col-md-3 column-box form-search products-right-column">
                    <div class="filter-table">
                        <h3>Settings</h3>
                        <div class="list-category-column">
                            <ul>
                                <li><a href="<?php echo base_url(); ?>web-user-profile">Details</a><div class="line-separator"></div><div class="line-separator2"></div></li>
                                <li><a href="<?php echo base_url(); ?>web-user-profile-edit">Update Profile</a><div class="line-separator"></div><div class="line-separator2"></div></li>
                                <li><a href="<?php echo base_url(); ?>web-user-change-password">Change Password</a><div class="line-separator"></div><div class="line-separator2"></div></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 panel panel-info-full">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <div class="col-xs-6"><h2>User Profile</h2></div>
                        </div>
                    </div>
                    <div class="list-product">
                        <div class="col-md-12 detail-box-desc" style="margin-top: 0px;">
                            <div class="col-md-3"><h1 class="txt-info-vertical" style="margin-top: 100px;">Details</h1></div>
                            <div class="col-md-9 info-box">

                                <p class="cat-txt"><span>Full Name: </span><?php echo $userData[0]['first_name']." ". $userData[0]['last_name']; ?></p>
                                <p class="cat-txt"><span>Mobile: </span><?php echo $userData[0]['mobile']; ?></p>
                                <p class="cat-txt"><span>Gender: </span><?php echo $userData[0]['gender']; ?></p>
                                <p class="cat-txt"><span>Address: </span><?php echo $userData[0]['address']; ?></p>
                                <p class="cat-txt"><span>Email Id: </span><?php echo $userData[0]['email']; ?></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
<div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Profile</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal regietr-form" action="<?php echo base_url(); ?>web-profile-image" method="post" enctype="multipart/form-data" role="form">
              <div class="input-group">
                <input type="hidden" class="form-control" name="id" value="<?php echo $userData[0]['id'];?>" autofocus>
            </div>
            
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="glyphicon glyphicon-upload"></i>
                </span>
                <input type="file" class="form-control" name="image" placeholder="Update Profile Picture" autofocus>
            </div>
            <?php if(form_error('image')){?>
                <div class="alert-error" id="1">
                    <a href="javascript:void();" class="close" onclick="removeErrorMessage(1);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
                    <?php echo form_error('image');?>
                </div>
            <?php } ?>            
              <br>
              <br>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Upload</button>
            </div>
          </form>  
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>