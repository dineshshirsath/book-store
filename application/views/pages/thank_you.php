<section class="services content-header">
        <div class="cover"></div>
        <div class="container">
            <div class="row">
                <h1 class="slide-page-text tlt" alt="Thank You" title="Thank You">Thank You</h1>
            </div>
        </div>
    </section>

    <section class="container table-padding">
        <div class="row about-solution">
            <div class="col-md-12 box-about">
                <div class="col-md-5"><img src="<?php echo base_url();?>assets/image/book/thank.jpg" alt="The Book Lounge" title="The Book Lounge" /></div>
                <div class="col-md-7">
                    <br><br><br><br><br>
                    <h2>Thank you for registration. <br> <a href="<?php echo base_url(); ?>admin" alt="Login Here" title="Login Here">Please Login Here</a></h2>
                </div>
            </div>
        </div>
    </section>