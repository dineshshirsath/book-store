<!-- ------Rating CSS-------- -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.rateyo.min.css"/>

    <section class="services content-header">
        <div class="cover"></div>
        <div class="container">
            <div class="row">
                <h1 class="slide-page-text tlt">Review</h1>
            </div>
        </div>
    </section>

    <section class="container blog-v1">
        <div class="col-md-12 table-padding">
            <div class="col-md-3 column-box">
                <!--FILTER CATEGORIES-->
                <div class="filter-table">
                    <h3>Ratings</h3>
                    <?php $Tstars = 0; $percent = 0; ?>
                        <?php for ($i=5; $i > 0; $i--): ?>
                            <?php $starRatingData = $this->CommonFunctionModel->getReviewRating($bookId, $i); ?>
                            <?php 
                                $Tstars = $Tstars + $starRatingData[0]['star'];
                            ?>
                        <?php endfor ?>
                        <?php for ($i=5; $i > 0; $i--): ?>
                            <?php $starRatingData = $this->CommonFunctionModel->getReviewRating($bookId, $i); ?>
                            <?php 
                                $percent = $starRatingData[0]['star'] * 100 / $Tstars;
                            ?>
                            <div class="skillbar clearfix" data-percent="<?php echo $percent."%"; ?>">
                                <div class="skillbar-title"><span><span><?php echo $i ?>&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
                                <div class="skillbar-bar"></div>
                                <div class="skill-bar-percent"><?php echo $starRatingData[0]['star'] ?></div>
                            </div>
                        <?php endfor ?>
                </div>
            </div>

            <!--Form Order with Email-->
            <div class="col-md-9 content-send-order animate-show animate-hide" aria-hidden="true" style="float: right;margin-top: -40px;">

                <h2>Please Add Book Review</h2>
                <div class="box-content contact">

                    <div class="panel-body">
                        <form action="<?php echo base_url('insert-review'); ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="hidden" class="form-control" name="rating" id="ratingId">
                                    <div id="rateYo"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="hidden" class="form-control" id="book_id" name="book_id" value="<?php echo $bookId; ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Your Name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <textarea class="form-control" rows="6" id="description" name="description" placeholder="Description..." ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <button type="submit" name="submit" class="btn btn-lg btn-primary checkout">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>

            </div>
        </div>        
    </section>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.rateyo.js"></script>

<script type="text/javascript">
 
$(function () {
 
  $("#rateYo").rateYo({
 
    onSet: function (rating, rateYoInstance) {
      var rating_amaunt = rating;
      //alert("Rating is set to: " + rating_amaunt);
      $('#ratingId').val(rating_amaunt);
    }
  });
});

    
</script>