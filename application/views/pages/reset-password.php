<section class="content-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-promotion login-content-txt">
                        <h1 class="register-top-title txt-colot-animate tlt">Special<span> Offers</span></h1>
                        <h1 class="register-top-title register-top-second tlt2"> on all Books</h1>
                    </div>
                    <div class="fadeInRight animated abc-book" style="top: 45px;">
                        <img src="<?php echo base_url();?>assets/image/book/bookback.gif" alt="The Book Lounge" title="The Book Lounge" />
                    </div>
                </div>
            </div>
        </div>
    </section>

<section class="container">
        <div class="row">
            <div class="col-lg-12 table-padding">
                <div class="register-img-background pg-login"></div>
                <div id="loginbox" class="mainbox col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title" alt="Forgot Password" title="Forgot Password">Forgot Password</div>
                            <div class="forgot-pwd">
                                <a href="<?php echo base_url(); ?>login"><button class="btn btn-primary">Log In</button></a>
                            </div>
                        </div>
                        <div style="padding-top: 20px" class="panel-body">
                            <form name="loginForm" class="form-horizontal" method="post" action="<?php echo base_url() ?>reset-password">
								<input type="hidden" name="userId" value="<?php echo $result; ?>"/>
                                <!--<div style="margin-top: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-send"></i></span>
                                    <input type="hidden" class="form-control" value="<?php echo $result->email; ?>" name="email" placeholder="Email" />
                                </div>
                                <?php if(form_error('email')){?>
                                   <div class="alert-error" id="1">
                                    <a href="javascript:void();" class="close" onclick="removeErrorMessage(1);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
                                    <?php echo form_error('email');?>
                                   </div>
                                <?php } ?>-->

                                <div style="margin-top: 25px" class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-lock"></i>
                                    </span>
                                    <input type="password" class="form-control" name="password" placeholder="New Password" />
                                </div>
                                <?php if(form_error('password')){?>
                                   <div class="alert-error" id="2">
                                    <a href="javascript:void();" class="close" onclick="removeErrorMessage(2);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
                                    <?php echo form_error('password');?>
                                   </div>
                                <?php } ?>

                                <div style="margin-top: 25px" class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-lock"></i>
                                    </span>
                                    <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" />
                                </div>
                                <?php if(form_error('confirm_password')){?>
                                   <div class="alert-error" id="3">
                                    <a href="javascript:void();" class="close" onclick="removeErrorMessage(3);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
                                    <?php echo form_error('confirm_password');?>
                                   </div>
                                <?php } ?>

                                <div style="margin-top: 20px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls">
                                        <input type="submit" class="btn btn-success login-btn" value="Reset Password" alt="Reset Password" title="Reset Password">
                                    </div><br /><br />

                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
	function removeErrorMessage(id)
	{
		$('#'+id).html('');
	}
</script>