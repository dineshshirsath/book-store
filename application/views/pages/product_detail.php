    <style type="text/css">
        .fa-star{color: #ffb300; font-size: 14px;}
    </style>
    <section class="pattern-1">
        <div class="container content-top-about">
            <div class="col-md-12 content-top-about">
                <div class="img-man">
                    <img alt="" src="<?php echo base_url();?>assets/image/book/man01.png" alt="The Book Lounge" title="The Book Lounge">
                </div>
                <div class="fadeInUp animated">
                    <img src="<?php echo base_url();?>assets/image/book/law-book.png" alt="The Book Lounge" title="The Book Lounge">
                </div>
            </div>
        </div>
    </section>
	<form action="<?php echo base_url('addcart'); ?>" method="post">
    
	<?php $ProductDetails = $this->ProductModel->getProductDetails($id); ?>
	<?php foreach ($ProductDetails as $row):
	// Create form and send values in 'shopping/add' function.
    //echo form_open(base_url().'add-cart');
    echo form_hidden('id', $row['id']);
    echo form_hidden('name', $row['title']);
    echo form_hidden('product_img', $row['image']);
    echo form_hidden('price', $row['price']);
    ?> 
	<section class="container page-details-product">
        <div class="row">
            <div class="col-md-12 table-padding">

                <div class="panel panel-info-full">

                    <div class="list-product">
                        <div class="col-md-3 my-shop-animation">
                            <div class="box-prod">
                                <div class="box-img-book">
                                    <img src="<?php echo base_url();?>assets/image/book/<?php echo $row['image'] ?>" alt="<?php echo $row['title']; ?>" title="<?php echo $row['title']; ?>">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 box-desc-detail">
                            <h2 class="title-product"><?php echo $row['title']; ?></h2>

                            <p class="author-txt"><span>Author: </span><?php echo $row['authour']; ?></p>

                            <p class="cat-txt"><span>Edition: </span><?php echo $row['edition']; ?></p>

                            <p class="book-price">&#8377;<?php echo $row['price']; ?><span> 8.89</span></p>
                            <p class="cat-txt"><span>Description: </span><?php echo $row['book_discription']; ?></p>
                            <div class="box-btn-shop">
                                <div class="bt-img"><button type="submit" name="submit" class="btn btn-add-cart" alt="Add To Cart" title="Add To Cart"><i class="fa fa-shopping-bag"></i>Add To Cart</button></div>
                            </div>
                            <br>
                            <p class="author-txt"><?php if(!empty($row['rates'])){
                             echo round($row['rates'],1);
                                }else{
                                    echo "No Ratings!";
                                } ?>  <i class="fa fa-star"></i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </form>
<?php endforeach ?>
    <section class="container blog-v1">
        <div class="col-md-12 table-padding">
            <div class="col-md-3 column-box">
                <!--FILTER CATEGORIES-->
                <div class="filter-table">
                    <h3>Ratings</h3>
                    <div class="list-category-column">
                        <?php $Tstars = 0; $percent = 0; ?>
                        <?php for ($i=5; $i > 0; $i--): ?>
                            <?php $starRatingData = $this->CommonFunctionModel->getReviewRating($id, $i); ?>
                            <?php 
                                $Tstars = $Tstars + $starRatingData[0]['star'];
                            ?>
                        <?php endfor ?>
                        <?php for ($i=5; $i > 0; $i--): ?>
                            <?php $starRatingData = $this->CommonFunctionModel->getReviewRating($id, $i); ?>
                            <?php 
                                $percent = $starRatingData[0]['star'] * 100 / $Tstars;
                            ?>
                            <div class="skillbar clearfix" data-percent="<?php echo $percent."%"; ?>">
                                <div class="skillbar-title"><span><span><?php echo $i ?>&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
                                <div class="skillbar-bar"></div>
                                <div class="skill-bar-percent"><?php echo $starRatingData[0]['star'] ?></div>
                            </div>
                        <?php endfor ?>
                    </div>
                </div>
            </div>
            <?php $reviewData = $this->CommonFunctionModel->getReviewData($id); ?>
            <div class="col-md-9 column-box">
                <!-- Mini SLider -->
                <div class="filter-table">
                    <a href="<?php echo base_url(); ?>add-review?bookId=<?php echo $id; ?>"><button class="btn btn-primary btn-blog" style="float: right;">Add Review</button></a>
                    <h3>Reviews</h3>
                </div>
                <?php $i = 1; ?>
                <?php foreach ($reviewData as $row): ?>
                    <?php $i++; ?>
                    <div class="blog-item">
                        <div class="blog-desc">
                            <h2><?php echo $row['title'] ?></h2>
                            <p class="blog-date"><?php echo date("F d, Y", strtotime($row['created_at'])); ?> - <span>by <?php echo $row['user_name'] ?></span></p>
                            <p class="blog-info">
                                <?php echo $row['description'] ?>
                            </p>
                            <?php 
                            $countLike = $this->CommonFunctionModel->getCountLikeDislike($row['id'], '1'); 
                            $countDisLike = $this->CommonFunctionModel->getCountLikeDislike($row['id'], '0'); 
                            $likeColor = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($row['id'], '1');
                            $disLikeColor = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($row['id'], '0');
                            ?>
                            <p class="blog-tags">
                                <i class="fa fa-star" aria-hidden="true" style="font-size: 14px;"></i><span><?php echo $row['rating'] ?></span>
                                <p href="javascript:void()" class="blog-tags" style="float: right;margin-top: -33px; margin-right: 20px;">
                                    
                                    <a href="javascript:void()" onclick="like_dislike('1',<?php echo $row['id'] ?>)"><i class="fa fa-thumbs-up" aria-hidden="true" style="color:<?php echo $likeColor; ?>" id="like_color_<?php echo $row['id'] ?>"></i></a><span id="like_<?php echo $row['id']; ?>"><?php echo $countLike->total; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <a href="javascript:void()" onclick="like_dislike('0',<?php echo $row['id'] ?>)"><i class="fa fa-thumbs-down" id="dislike_color_<?php echo $row['id'] ?>" aria-hidden="true" style="color:<?php echo $disLikeColor; ?>" ></i></a><span id="dislike_<?php echo $row['id']; ?>"><?php echo $countDisLike->total; ?></span>
                                </p>
                            </p>
                            <hr>
                        </div>
                    </div>   
                <?php endforeach ?>
                <?php if (!empty($reviewData)): ?>
                    <a href="<?php echo base_url(); ?>all-review?bookId=<?php echo $id; ?>"><button class="btn btn-primary btn-blog">All Review</button></a>
                <?php endif ?>
            </div>
        </div>        
    </section>

<script>
    function like_dislike(lickdislick,rId)
    {
        var userId = "<?php echo $this->session->userdata('user_id') ?>";
        //alert(rId+' '+lickdislick+' '+userId);
        var url = "<?php echo base_url(); ?>website/RatingReviewController/addLikeDislikeData";
        $.ajax({
                type: 'POST',
                url: url,
                data:{
                    'review_id': rId,
                    'user_id': userId,
                    'like_dislike': lickdislick
                },
                //dataType: 'json',
                success: function (response) {
                    var obj = JSON.parse(response);
                    //var exists = obj.exists;
                    $('#dislike_'+rId).html(obj.countDisLike.total);
                    $('#like_'+rId).html(obj.countLike.total);
                    $('#dislike_color_'+rId).attr('style','color:'+obj.$disLikeColor);
                    $('#like_color_'+rId).attr('style','color:'+obj.likeColor);
                },                    
            });
    }
</script>    
    
<script>
    $(function () {
        "use strict";
        setTimeout(function () {
            $('.tlt').css("visibility", "visible");
            $('.tlt').textillate({ in: { effect: 'bounceIn' } });
        }, 2000);
    });
    </script>

    <script>
    $(document).ready(function ($) {
        "use strict";
    /*************************************************************
    Percentage skillbar
    *************************************************************/
        $(function skillbar() {
            try {
                $('.skillbar').each(function () {
                    $(this).find('.skillbar-bar').animate({
                        width: $(this).attr('data-percent')
                    }, 3000);
                });
            } catch (err) {
            }
        });

    /*************************************************************
    Small FlexSlider Slide
    *************************************************************/
        $(function flexImg() {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: ""

            });
        });
    });
    </script>

    <!--Counter-->
    <script src="http://www.theme-oxygen.com/envato/evobook/preview/html/Scripts/waypoints.min.js" type="text/javascript"></script>
    <script src="http://www.theme-oxygen.com/envato/evobook/preview/html/Scripts/jquery.counterup.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
        "use strict";
        $('.counter').counterUp({
            delay: 40,
            time: 4000
        });
    });
    </script>
