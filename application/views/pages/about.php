    <section class="services content-header">
        <div class="cover"></div>
        <div class="container">
            <div class="row">
                <h1 class="slide-page-text tlt" alt="About Us" title="About Us">About Us</h1>
            </div>
        </div>
    </section>

    <section class="container table-padding">
        <div class="row about-solution">
            <div class="col-md-12 box-about">
                <div class="col-md-5"><img src="<?php echo base_url();?>assets/image/book/digital-book.jpg" alt="The Book Lounge" title="The Book Lounge" /></div>
                <div class="col-md-7">
                    <h3 alt="Our Story" title="Our Story">Our Story </h3>
                    <p>Welcome to The Book Lounge Fin-tech start up with perfect blend of professionals having experience in TAX, Finance and Technology.</p>										
					<h3 alt="Our Vision" title="Our Vision">Our Vision  </h3>					
					<p>Storing and connecting every dot in the knowledge to unleash the power of staying updated. Making knowledge available with ease to the professionals anytime and anyway.</p>					
                </div>
            </div>
        </div>
    </section>

