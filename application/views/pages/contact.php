   <style>
	.alert-error	p {
    	color: red;
    	height: 22px;
	}
	.alert-error	a {
    	float: left;
	    margin-left: 7px;
	    margin-top: -2px;
	    color: #ff0808;
	}
</style>
    <section>
        <div>
            
<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3771.944349863772!2d73.01658466442822!3d19.02217373711878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sS-07-24%2C+Second+floor%2C+Haware+Centurion+Mall%2C+Seawoods+West%2CSector+40%2C+Nerul%2C+Sector+19A%2C+Nerul%2CNavi+Mumbai%2C+Maharashtra+400706+!5e0!3m2!1sen!2sin!4v1536388748811" width="100%" height="300" frameborder="0" alt="The Book Lounge" title="The Book Lounge"></iframe>
                </div>
       
        <div class="container">
            <div class="row">
                <div class="col-md-12 contact-page">
                    <div class="col-md-8 box-1-left">
                        <h3 alt="Leave us a Message" title="Leave us a Message">Leave us a Message</h3>
                        <form id="contact-form" role="form" method="POST" action="<?php echo base_url();?>add-contact-us">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label for="input1">First Name *</label>
                                    <input type="text" name="first_name" placeholder="e.g. Your First Name Here.." class="form-control" id="first_name" value="<?php echo set_value('first_name');?>" autofocus><?php if(form_error('first_name')){?>
					<div class="alert-error" id="1">
        			<a href="javascript:void();" class="close" data-dismiss="alert" onclick="removeErrorMessage(1);">&times;&nbsp;</a>
        			<?php echo form_error('first_name');?>
				</div>
				<?php } ?>
                                </div>
                                
                                                          
                                <div class="form-group col-lg-6">
                                    <label for="input1">Last Name *</label>
                                    <input type="text" name="last_name" placeholder="e.g. Your Last Name Here.." class="form-control" id="last_name" value="<?php echo set_value('last_name');?>"><?php if(form_error('last_name')){?>
					<div class="alert-error" id="1">
        			<a href="javascript:void();" class="close" data-dismiss="alert" onclick="removeErrorMessage(1);">&times;&nbsp;</a>
        			<?php echo form_error('last_name');?>
				</div>
				<?php } ?>
                                </div>
                                
                                                   
                                <div class="clearfix"></div>
                                <div class="form-group col-lg-6">
                                    <label for="input2">Email Address *</label>
                                    <input type="text" name="email" placeholder="e.g. mail@example.com" class="form-control" id="email" value="<?php echo set_value('email');?>"><?php if(form_error('email')){?>
					<div class="alert-error" id="1">
        			<a href="javascript:void();" class="close" data-dismiss="alert" onclick="removeErrorMessage(1);">&times;&nbsp;</a>
        			<?php echo form_error('email');?>
				</div>
				<?php } ?>
                                </div>
                                
                                                          
                                <div class="form-group col-lg-6">
                                    <label for="input3">Phone Number</label>
                                    <input type="phone" name="phone" placeholder="+123 243 4560" class="form-control" id="phone" value="<?php echo set_value('phone');?>"><?php if(form_error('phone')){?>
					<div class="alert-error" id="1">
        			<a href="javascript:void();" class="close" data-dismiss="alert" onclick="removeErrorMessage(1);">&times;&nbsp;</a>
        			<?php echo form_error('phone');?>
				</div>
				<?php } ?>
                                </div>
                                
                                                           
                                <div class="clearfix"></div>
                                <div class="form-group col-lg-12">
                                    <label for="input4">Message</label> 
                                    
                                    <textarea class="form-control" id="message" name="message" rows="30" cols="30" placeholder="Enter message"><?php echo set_value('message'); ?></textarea><?php if(form_error('message')){?>
					<div class="alert-error" id="5">
        			<a href="javascript:void();" class="close" data-dismiss="alert" onclick="removeErrorMessage(5);" >&times;&nbsp;</a>
        			<?php echo form_error('message');?>
				</div>
				<?php } ?>
                                </div>
                                
                                                          
                                <div class="form-group col-lg-12">
                                    
                                    <button type="submit" class="btn btn-primary bt-contact-submit" alt="Send Message" title="Send Message">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 info-store-contact">
                        <h3>Address</h3>
                        <p><i class="fas fa-map-marker"></i>&nbsp;S-07-24, Second floor,<br>
            &nbsp;&nbsp;&nbsp;Haware Centurion Mall,<br>
            &nbsp;&nbsp;&nbsp;Seawoods West,Sector 40,<br>&nbsp;&nbsp;&nbsp;Nerul, Sector 19A,<br>&nbsp;&nbsp;&nbsp;Nerul,Navi Mumbai,<br>
            &nbsp;&nbsp;&nbsp;Maharashtra 400706   	
            </p>
            <p><i class="fas fa-phone"></i>&nbsp;<a href="tel:+91 9594 872 502" title="+91 9594 872 502" >+91 9594 872 502</a></p>
            <p><i class="fas fa-envelope"></i>&nbsp;<a href="mailto: datafalconx@gmail.com" title="datafalconx@gmail.com" class="primary"> datafalconx@gmail.com</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
	function removeErrorMessage(id)
	{
		$('#'+id).html('');
	}
</script>