	<style type="text/css">
		.error p{
			color: red;
		}
	</style>
	<section class="content-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-promotion register-box-text">
                        <h1 class="register-top-title tlt">New Account</h1>
                        <h1 class="register-top-title register-top-second tlt2">+ 1 book free</h1>
                    </div>
                    <div class="fadeInRight animated abc-book">
                        <img src="images/slide-book1.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-lg-12 table-padding">
                <div class="register-img-background"></div>
                <div id="loginbox" class="mainbox col-md-12 register-box">
                    <div class="panel panel-info">
                        <div>
                            <h3>Author KYC<h3>
                        </div>
                        <div class="panel-heading">
                            <div class="panel-title">Document Upload</div>
                        </div>
                        <div style="padding-top: 20px" class="panel-body">
                            <form name="loginForm" class="form-horizontal regietr-form" method="POST" action="<?php echo base_url(); ?>author-kyc-upload" enctype="multipart/form-data">

                            	<div class="input-group">
                                    <input type="hidden" class="form-control" name="seller_id" value="<?php echo $sellerId; ?>"/>
                                </div>

								<label for=""> Adhar Card</label>
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-upload"></i>
                                    </span>
                                    <input type="file" class="form-control" name="adhar_card" placeholder="Adhar card"/>
                                </div>
                                <span class="error"><?php echo form_error('adhar_card');?></span><br>

								<label for=""> Pan Card</label> 
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-upload"></i>
                                    </span>
                                    <input type="file" class="form-control" name="pan_card" placeholder="Pan card"/>
                                </div>
                                <span class="error"><?php echo form_error('pan_card');?></span><br>

								<label for=""> Voter Card</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-upload"></i>
                                    </span>
                                    <input type="file" class="form-control" name="voter_card" placeholder="Voter card"/>
                                </div>
                                <span class="error"><?php echo form_error('voter_card');?></span>

                                <div style="margin-top: 20px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls">
                                        <button type="submit" class="btn btn-success login-btn" value="Register">Upload</button>
                                    </div><br /><br />
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>