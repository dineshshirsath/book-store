<!-- <?php     
	// Example data
	$star1 = 1;
	$star2 = 5;
	$star3 = 7;
	$star4 = 1000;
	$star5 = 8;

	$tot_stars = $star1 + $star2 + $star3 + $star4 + $star5;

	for ($i=1;$i<=5;++$i) {
	  $var = "star$i";
	  $count = $$var;
	  echo $percent = $count * 100 / $tot_stars;
	  printf("\t%2d (%5.2f%%)\n", $count, $percent,2); echo '<br>';
	}
?> -->

	<section class="services content-header">
        <div class="cover"></div>
        <div class="container">
            <div class="row">
                <h1 class="slide-page-text tlt">All Review</h1>
            </div>
        </div>
    </section>
    <?php $productData = $this->CommonFunctionModel->getProductUsingId($bookId); ?>
    <section class="container blog-v1">
        <div class="col-md-12 table-padding">
            <div class="col-md-3 column-box">
                <!--FILTER CATEGORIES-->
                <div class="filter-table">
            		<img style="margin-bottom:50px" src="<?php echo base_url(); ?>assets/image/book/<?php echo $productData->image ?>" alt="">
                </div>
            </div>
            <?php $Tstars = 0; $percent = 0; ?>
        	<div class="col-lg-9 col-md-9 col-sm-9 service-percentage">
                <h2 class="title-team">Ratings</h2>
                <?php for ($i=5; $i > 0; $i--): ?>
                	<?php $starRatingData = $this->CommonFunctionModel->getReviewRating($bookId, $i); ?>
                	<?php 
                		$Tstars = $Tstars + $starRatingData[0]['star'];
                	?>
                <?php endfor ?>
                <?php for ($i=5; $i > 0; $i--): ?>
                	<?php $starRatingData = $this->CommonFunctionModel->getReviewRating($bookId, $i); ?>
                	<?php 
  						$percent = $starRatingData[0]['star'] * 100 / $Tstars;
                	?>
                	<div class="skillbar clearfix" data-percent="<?php echo $percent."%"; ?>">
	                    <div class="skillbar-title"><span><span><?php echo $i ?>&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
	                    <div class="skillbar-bar"></div>
	                    <div class="skill-bar-percent"><?php echo $starRatingData[0]['star'] ?></div>
	                </div>
                <?php endfor ?>
               <!-- <div class="skillbar clearfix" data-percent="80%">
                    <div class="skillbar-title"><span><span>5&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
                    <div class="skillbar-bar"></div>
                    <div class="skill-bar-percent">40%</div>
                </div>  

                <div class="skillbar clearfix " data-percent="95%">
                    <div class="skillbar-title"><span><span>4&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
                    <div class="skillbar-bar"></div>
                    <div class="skill-bar-percent">75%</div>
                </div> 

                <div class="skillbar clearfix " data-percent="100%">
                    <div class="skillbar-title"><span><span>3&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
                    <div class="skillbar-bar"></div>
                    <div class="skill-bar-percent">100%</div>
                </div> 

                <div class="skillbar clearfix " data-percent="70%">
                    <div class="skillbar-title"><span><span>2&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
                    <div class="skillbar-bar"></div>
                    <div class="skill-bar-percent">60%</div>
                </div> 

                <div class="skillbar clearfix " data-percent="65%">
                    <div class="skillbar-title"><span><span>1&nbsp;<i class="fa fa-star" aria-hidden="true" style="font-size: 10px;"></i></span></div>
                    <div class="skillbar-bar"></div>
                    <div class="skill-bar-percent">45%</div>
                </div> -->


            </div>
            <?php $reviewData = $this->CommonFunctionModel->getAllReviewData($bookId); ?>
            <div class="col-md-9 column-box">
                <!-- Mini SLider -->
                <div class="filter-table">
                    <h3>Reviews</h3>
                </div>
                <?php $i = 1; ?>
                <?php foreach ($reviewData as $row): ?>
                    <?php $i++; ?>
                    <div class="blog-item">
                        <div class="blog-desc">
                            <h2><?php echo $row['title'] ?></h2>
                            <p class="blog-date"><?php echo date("F d, Y", strtotime($row['created_at'])); ?> - <span>by <?php echo $row['user_name'] ?></span></p>
                            <p class="blog-info">
                                <?php echo $row['description'] ?>
                            </p>
                            <?php 
                            $countLike = $this->CommonFunctionModel->getCountLikeDislike($row['id'], '1'); 
                            $countDisLike = $this->CommonFunctionModel->getCountLikeDislike($row['id'], '0'); 
                            $likeColor = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($row['id'], '1');
                            $disLikeColor = $this->CommonFunctionModel->checkLoggedUserIsLikeDislike($row['id'], '0');
                            ?>
                            <p class="blog-tags">
                                <i class="fa fa-star" aria-hidden="true" style="font-size: 14px;"></i><span><?php echo $row['rating'] ?></span>
                                <p href="javascript:void()" class="blog-tags" style="float: right;margin-top: -33px; margin-right: 20px;">
                                    
                                    <a href="javascript:void()" onclick="like_dislike('1',<?php echo $row['id'] ?>)"><i class="fa fa-thumbs-up" aria-hidden="true" style="color:<?php echo $likeColor; ?>" id="like_color_<?php echo $row['id'] ?>"></i></a><span id="like_<?php echo $row['id']; ?>"><?php echo $countLike->total; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <a href="javascript:void()" onclick="like_dislike('0',<?php echo $row['id'] ?>)"><i class="fa fa-thumbs-down" id="dislike_color_<?php echo $row['id'] ?>" aria-hidden="true" style="color:<?php echo $disLikeColor; ?>" ></i></a><span id="dislike_<?php echo $row['id']; ?>"><?php echo $countDisLike->total; ?></span>
                                </p>
                            </p>
                            <hr>
                        </div>
                    </div>   
                <?php endforeach ?>
            </div>
        </div>        
    </section>

<script>
    function like_dislike(lickdislick,rId)
    {
        var userId = "<?php echo $this->session->userdata('user_id') ?>";
        //alert(rId+' '+lickdislick+' '+userId);
        var url = "<?php echo base_url(); ?>website/RatingReviewController/addLikeDislikeData";
        $.ajax({
                type: 'POST',
                url: url,
                data:{
                    'review_id': rId,
                    'user_id': userId,
                    'like_dislike': lickdislick
                },
                //dataType: 'json',
                success: function (response) {
                    var obj = JSON.parse(response);
                    //var exists = obj.exists;
                    $('#dislike_'+rId).html(obj.countDisLike.total);
                    $('#like_'+rId).html(obj.countLike.total);
                    $('#dislike_color_'+rId).attr('style','color:'+obj.$disLikeColor);
                    $('#like_color_'+rId).attr('style','color:'+obj.likeColor);
                },                    
            });
    }
</script> 


	<script>
    $(function () {
        "use strict";
        setTimeout(function () {
            $('.tlt').css("visibility", "visible");
            $('.tlt').textillate({ in: { effect: 'bounceIn' } });
        }, 2000);
    });
    </script>

    <script>
    $(document).ready(function ($) {
        "use strict";
    /*************************************************************
    Percentage skillbar
    *************************************************************/
        $(function skillbar() {
            try {
                $('.skillbar').each(function () {
                    $(this).find('.skillbar-bar').animate({
                        width: $(this).attr('data-percent')
                    }, 3000);
                });
            } catch (err) {
            }
        });

    /*************************************************************
    Small FlexSlider Slide
    *************************************************************/
        $(function flexImg() {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: ""

            });
        });
    });
    </script>

    <!--Counter-->
    <script src="http://www.theme-oxygen.com/envato/evobook/preview/html/Scripts/waypoints.min.js" type="text/javascript"></script>
    <script src="http://www.theme-oxygen.com/envato/evobook/preview/html/Scripts/jquery.counterup.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
        "use strict";
        $('.counter').counterUp({
            delay: 40,
            time: 4000
        });
    });
    </script>
