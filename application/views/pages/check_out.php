<?php
$productJSON = "";
$grand_total = 0;
$CartArr = [];
    if ($cart = $this->CartModel->getAddToCatData()):
    	$i = 1;
	    foreach ($cart as $item):
	    	$CartArr[] = array(
	    		'name'=>$item['id'],
	    		'description'=>"",
	    		"value"=>$item['price'],
	    		"qty"=>$item['qty'],
	    		"isRequired"=>"false"	    		
	    	);
	    	$grand_total = $grand_total + $item['subtotal'];
    	endforeach;
    endif;
// Merchant key here as provided by Payu
$MERCHANT_KEY = "rjQUPktU";

// Merchant Salt as provided by Payu
$SALT = "e5iIg1jwi8";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";

$action = '';

$posted = array();

//Generate random transaction id
$txnid = $this->CommonFunctionModel->random_number();
$productJSON = json_encode($CartArr);
if(!empty($_POST)) {
	$posted['amount'] = $_POST['amount'];
	$posted['phone'] = $_POST['phone'];
	$posted['firstname'] = $_POST['firstname'];
	$posted['email'] = $_POST['email'];
	$posted['key'] = $MERCHANT_KEY;
	$posted['txnid'] = $txnid;
	$posted['productinfo'] = $productJSON;
	$posted['email'] = $_POST['email'];
	$posted['firstname'] = $_POST['firstname'];
	$posted['phone'] = $_POST['phone'];
	$posted['surl'] = base_url("payumoney-success");
	$posted['furl'] = base_url("payumoney-error");
	$posted['curl'] = base_url("payumoney-cancel");
	$posted['service_provider'] = 'payu_paisa';
	
}

$hash = '';

// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

if(empty($posted['hash']) && sizeof($posted) > 0) {
	if(
			  empty($posted['key'])
			  || empty($posted['txnid'])
			  || empty($posted['amount'])
			  || empty($posted['firstname'])
			  || empty($posted['email'])
			  || empty($posted['phone'])
			  || empty($posted['productinfo'])
			  || empty($posted['surl'])
			  || empty($posted['furl'])
			  || empty($posted['service_provider'])
	  ) {
		//echo "Fail";
		redirect('check_out/');
	  }
	else{
		
		$hashVarsSeq = explode('|', $hashSequence);
		$hash_string = '';
		foreach($hashVarsSeq as $hash_var){
			  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
			  $hash_string .= '|';
		}

		$hash_string .= $SALT;
		$hash = strtolower(hash('sha512', $hash_string));
		$posted['hash'] = $hash;
		$action = $PAYU_BASE_URL . '/_payment';
		
	}
}
elseif(!empty($posted['hash'])){
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}

?>
<style>
*{box-sizing:border-box}.col-25,{padding:73px 61px;margin-top:105px}.col-50,.col-75{padding:0 12px}input[type=text]{width:100%;margin-bottom:20px;padding:12px;border:1px solid #ccc;border-radius:3px}label{margin-bottom:10px;display:block}.btn{background-color:#4CAF50;color:#fff;padding:12px;margin:10px 0;border:none;width:40%;border-radius:3px;cursor:pointer;font-size:17px}.btn:hover{background-color:#45a049}a{color:#2196F3}hr{border:1px solid #d3d3d3;margin-top:0;margin-bottom:0}span.price{float:right;color:grey}@media (max-width: 800px){.row{flex-direction:column-reverse}.col-25{margin-bottom:20px}}table{border:0 solid #ccc;border-collapse:collapse;margin:0;padding:0;width:100%;table-layout:fixed}table tr{background-color:#f8f8f8;border:0 solid #ddd;padding:.35em}table th,table td{padding:.625em;text-align:center}table th{font-size:.85em;letter-spacing:.1em;text-transform:uppercase;padding-top:43px}@media screen and (max-width: 600px){table{border:0}table thead{border:none;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}table tr{border-bottom:3px solid #ddd;display:block;margin-bottom:.625em}table td{border-bottom:1px solid #ddd;display:block;font-size:.8em;text-align:right}table td::before{content:attr(data-label);float:left;font-weight:700;text-transform:uppercase}table td:last-child{border-bottom:0}}
</style>  
<style>
	.alert-error p{
		color:red;
	}
</style>
    <section class="container">
        <div class="row">
            <!--Form Order with Email-->
            <div class="col-md-6 content-send-order animate-show animate-hide" aria-hidden="true">
                <h2 alt="Please compile the form!" title="Please compile the form!">Please compile the form!</h2>
                <div class="box-content contact">
                    <div class="panel-body">
                        <form name="contactform" class="form-horizontal" action="<?php echo base_url('billing');?>" method="post">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" id="inputName" name="name" placeholder="Full Name"   value="<?php echo set_value('name');?>" autofocus>
                                <?php if(form_error('name')){?>
				                   <div class="alert-error" id="1">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(1);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        							<?php echo form_error('name');?>
				                   </div>
				               <?php } ?>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Your Email"  value="<?php echo set_value('email');?>">
                                <?php if(form_error('email')){?>
				                   <div class="alert-error" id="2">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(2);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('email');?>
				                   </div>
				               <?php } ?>
                                </div>
                            </div>
                            <!-- COUNTRY -->
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <select class="select-opt form-control-select" name="country" aria-invalid="false">
                                        <option class="frt-options" value=""> - Select a Country -</option>
                                        <!-- ngRepeat: country in countries -->
                                        <option   class="other-opt " value="Afghanistan">Afghanistan</option> 
                                        <option   class="other-opt " value="Aland Islands">Aland Islands</option> 
                                        <option   class="other-opt " value="Albania">Albania</option> 
                                        <option   class="other-opt " value="Algeria">Algeria</option> 
                                        <option   class="other-opt " value="American Samoa">American Samoa</option> 
                                        <option   class="other-opt " value="Andorra">Andorra</option> 
                                        <option   class="other-opt " value="Angola">Angola</option> 
                                        <option   class="other-opt " value="Anguilla">Anguilla</option> 
                                        <option   class="other-opt " value="Antarctica">Antarctica</option> 
                                        <option   class="other-opt " value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                        <option   class="other-opt " value="Argentina">Argentina</option> 
                                        <option   class="other-opt " value="Armenia">Armenia</option> 
                                        <option   class="other-opt " value="Aruba">Aruba</option> 
                                        <option   class="other-opt " value="Australia">Australia</option> 
                                        <option   class="other-opt " value="Austria">Austria</option> 
                                        <option   class="other-opt " value="Azerbaijan">Azerbaijan</option> 
                                        <option   class="other-opt " value="Bahamas">Bahamas</option> 
                                        <option   class="other-opt " value="Bahrain">Bahrain</option> 
                                        <option   class="other-opt " value="Bangladesh">Bangladesh</option> 
                                        <option   class="other-opt " value="Barbados">Barbados</option> 
                                        <option   class="other-opt " value="Belarus">Belarus</option> 
                                        <option   class="other-opt " value="Belgium">Belgium</option> 
                                        <option   class="other-opt " value="Belize">Belize</option> 
                                        <option   class="other-opt " value="Benin">Benin</option> 
                                        <option   class="other-opt " value="Bermuda">Bermuda</option> 
                                        <option   class="other-opt " value="Bhutan">Bhutan</option> 
                                        <option   class="other-opt " value="Bolivia">Bolivia</option> 
                                        <option   class="other-opt " value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
                                        <option   class="other-opt " value="Botswana">Botswana</option> 
                                        <option   class="other-opt " value="Bouvet Island">Bouvet Island</option> 
                                        <option   class="other-opt " value="Brazil">Brazil</option> 
                                        <option   class="other-opt " value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
                                        <option   class="other-opt " value="Brunei Darussalam">Brunei Darussalam</option> 
                                        <option   class="other-opt " value="Bulgaria">Bulgaria</option> 
                                        <option   class="other-opt " value="Burkina Faso">Burkina Faso</option> 
                                        <option   class="other-opt " value="Burundi">Burundi</option> 
                                        <option   class="other-opt " value="Cambodia">Cambodia</option> 
                                        <option   class="other-opt " value="Cameroon">Cameroon</option> 
                                        <option   class="other-opt " value="Canada">Canada</option> 
                                        <option   class="other-opt " value="Cape Verde">Cape Verde</option> 
                                        <option   class="other-opt " value="Cayman Islands">Cayman Islands</option> 
                                        <option   class="other-opt " value="Central African Republic">Central African Republic</option> 
                                        <option   class="other-opt " value="Chad">Chad</option> 
                                        <option   class="other-opt " value="Chile">Chile</option> 
                                        <option   class="other-opt " value="China">China</option> 
                                        <option   class="other-opt " value="Christmas Island">Christmas Island</option> 
                                        <option   class="other-opt " value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
                                        <option   class="other-opt " value="Colombia">Colombia</option> 
                                        <option   class="other-opt " value="Comoros">Comoros</option> 
                                        <option   class="other-opt " value="Congo">Congo</option> 
                                        <option   class="other-opt " value="Congo, The Democratic Republic of the">Congo, The Democratic Republic of the</option> 
                                        <option   class="other-opt " value="Cook Islands">Cook Islands</option> 
                                        <option   class="other-opt " value="Costa Rica">Costa Rica</option> 
                                        <option   class="other-opt " value="Cote D'Ivoire">Cote D'Ivoire</option> 
                                        <option   class="other-opt " value="Croatia">Croatia</option> 
                                        <option   class="other-opt " value="Cuba">Cuba</option> 
                                        <option   class="other-opt " value="Cyprus">Cyprus</option> 
                                        <option   class="other-opt " value="Czech Republic">Czech Republic</option> 
                                        <option   class="other-opt " value="Denmark">Denmark</option> 
                                        <option   class="other-opt " value="Djibouti">Djibouti</option> 
                                        <option   class="other-opt " value="Dominica">Dominica</option> 
                                        <option   class="other-opt " value="Dominican Republic">Dominican Republic</option> 
                                        <option   class="other-opt " value="Ecuador">Ecuador</option> 
                                        <option   class="other-opt " value="Egypt">Egypt</option> 
                                        <option   class="other-opt " value="El Salvador">El Salvador</option> 
                                        <option   class="other-opt " value="Equatorial Guinea">Equatorial Guinea</option> 
                                        <option   class="other-opt " value="Eritrea">Eritrea</option> 
                                        <option   class="other-opt " value="Estonia">Estonia</option> 
                                        <option   class="other-opt " value="Ethiopia">Ethiopia</option> 
                                        <option   class="other-opt " value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
                                        <option   class="other-opt " value="Faroe Islands">Faroe Islands</option> 
                                        <option   class="other-opt " value="Fiji">Fiji</option> 
                                        <option   class="other-opt " value="Finland">Finland</option> 
                                        <option   class="other-opt " value="France">France</option> 
                                        <option   class="other-opt " value="French Guiana">French Guiana</option> 
                                        <option   class="other-opt " value="French Polynesia">French Polynesia</option> 
                                        <option   class="other-opt " value="French Southern Territories">French Southern Territories</option> 
                                        <option   class="other-opt " value="Gabon">Gabon</option> 
                                        <option   class="other-opt " value="Gambia">Gambia</option> 
                                        <option   class="other-opt " value="Georgia">Georgia</option> 
                                        <option   class="other-opt " value="Germany">Germany</option> 
                                        <option   class="other-opt " value="Ghana">Ghana</option> 
                                        <option   class="other-opt " value="Gibraltar">Gibraltar</option> 
                                        <option   class="other-opt " value="Greece">Greece</option> 
                                        <option   class="other-opt " value="Greenland">Greenland</option> 
                                        <option   class="other-opt " value="Grenada">Grenada</option> 
                                        <option   class="other-opt " value="Guadeloupe">Guadeloupe</option> 
                                        <option   class="other-opt " value="Guam">Guam</option> 
                                        <option   class="other-opt " value="Guatemala">Guatemala</option> 
                                        <option   class="other-opt " value="Guernsey">Guernsey</option> 
                                        <option   class="other-opt " value="Guinea">Guinea</option> 
                                        <option   class="other-opt " value="Guinea-Bissau">Guinea-Bissau</option> 
                                        <option   class="other-opt " value="Guyana">Guyana</option> 
                                        <option   class="other-opt " value="Haiti">Haiti</option> 
                                        <option   class="other-opt " value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
                                        <option   class="other-opt " value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
                                        <option   class="other-opt " value="Honduras">Honduras</option> 
                                        <option   class="other-opt " value="Hong Kong">Hong Kong</option> 
                                        <option   class="other-opt " value="Hungary">Hungary</option> 
                                        <option   class="other-opt " value="Iceland">Iceland</option> 
                                        <option   class="other-opt " value="India">India</option> 
                                        <option   class="other-opt " value="Indonesia">Indonesia</option> 
                                        <option   class="other-opt " value="Iran, Islamic Republic Of">Iran, Islamic Republic Of</option> 
                                        <option   class="other-opt " value="Iraq">Iraq</option> 
                                        <option   class="other-opt " value="Ireland">Ireland</option> 
                                        <option   class="other-opt " value="Isle of Man">Isle of Man</option> 
                                        <option   class="other-opt " value="Israel">Israel</option> 
                                        <option   class="other-opt " value="Italy">Italy</option> 
                                        <option   class="other-opt " value="Jamaica">Jamaica</option> 
                                        <option   class="other-opt " value="Japan">Japan</option> 
                                        <option   class="other-opt " value="Jersey">Jersey</option> 
                                        <option   class="other-opt " value="Jordan">Jordan</option> 
                                        <option   class="other-opt " value="Kazakhstan">Kazakhstan</option> 
                                        <option   class="other-opt " value="Kenya">Kenya</option> 
                                        <option   class="other-opt " value="Kiribati">Kiribati</option> 
                                        <option   class="other-opt " value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
                                        <option   class="other-opt " value="Korea, Republic of">Korea, Republic of</option> 
                                        <option   class="other-opt " value="Kuwait">Kuwait</option> 
                                        <option   class="other-opt " value="Kyrgyzstan">Kyrgyzstan</option> 
                                        <option   class="other-opt " value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
                                        <option   class="other-opt " value="Latvia">Latvia</option> 
                                        <option   class="other-opt " value="Lebanon">Lebanon</option> 
                                        <option   class="other-opt " value="Lesotho">Lesotho</option> 
                                        <option   class="other-opt " value="Liberia">Liberia</option> 
                                        <option   class="other-opt " value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
                                        <option   class="other-opt " value="Liechtenstein">Liechtenstein</option> 
                                        <option   class="other-opt " value="Lithuania">Lithuania</option> 
                                        <option   class="other-opt " value="Luxembourg">Luxembourg</option> 
                                        <option   class="other-opt " value="Macao">Macao</option> 
                                        <option   class="other-opt " value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
                                        <option   class="other-opt " value="Madagascar">Madagascar</option> 
                                        <option   class="other-opt " value="Malawi">Malawi</option> 
                                        <option   class="other-opt " value="Malaysia">Malaysia</option> 
                                        <option   class="other-opt " value="Maldives">Maldives</option> 
                                        <option   class="other-opt " value="Mali">Mali</option> 
                                        <option   class="other-opt " value="Malta">Malta</option> 
                                        <option   class="other-opt " value="Marshall Islands">Marshall Islands</option> 
                                        <option   class="other-opt " value="Martinique">Martinique</option> 
                                        <option   class="other-opt " value="Mauritania">Mauritania</option> 
                                        <option   class="other-opt " value="Mauritius">Mauritius</option> 
                                        <option   class="other-opt " value="Mayotte">Mayotte</option> 
                                        <option   class="other-opt " value="Mexico">Mexico</option> 
                                        <option   class="other-opt " value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
                                        <option   class="other-opt " value="Moldova, Republic of">Moldova, Republic of</option> 
                                        <option   class="other-opt " value="Monaco">Monaco</option> 
                                        <option   class="other-opt " value="Mongolia">Mongolia</option> 
                                        <option   class="other-opt " value="Montserrat">Montserrat</option> 
                                        <option   class="other-opt " value="Morocco">Morocco</option> 
                                        <option   class="other-opt " value="Mozambique">Mozambique</option> 
                                        <option   class="other-opt " value="Myanmar">Myanmar</option> 
                                        <option   class="other-opt " value="Namibia">Namibia</option> 
                                        <option   class="other-opt " value="Nauru">Nauru</option> 
                                        <option   class="other-opt " value="Nepal">Nepal</option> 
                                        <option   class="other-opt " value="Netherlands">Netherlands</option> 
                                        <option   class="other-opt " value="Netherlands Antilles">Netherlands Antilles</option> 
                                        <option   class="other-opt " value="New Caledonia">New Caledonia</option> 
                                        <option   class="other-opt " value="New Zealand">New Zealand</option> 
                                        <option   class="other-opt " value="Nicaragua">Nicaragua</option> 
                                        <option   class="other-opt " value="Niger">Niger</option> 
                                        <option   class="other-opt " value="Nigeria">Nigeria</option> 
                                        <option   class="other-opt " value="Niue">Niue</option> 
                                        <option   class="other-opt " value="Norfolk Island">Norfolk Island</option> 
                                        <option   class="other-opt " value="Northern Mariana Islands">Northern Mariana Islands</option> 
                                        <option   class="other-opt " value="Norway">Norway</option> 
                                        <option   class="other-opt " value="Oman">Oman</option> 
                                        <option   class="other-opt " value="Pakistan">Pakistan</option> 
                                        <option   class="other-opt " value="Palau">Palau</option> 
                                        <option   class="other-opt " value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
                                        <option   class="other-opt " value="Panama">Panama</option> 
                                        <option   class="other-opt " value="Papua New Guinea">Papua New Guinea</option> 
                                        <option   class="other-opt " value="Paraguay">Paraguay</option> 
                                        <option   class="other-opt " value="Peru">Peru</option> 
                                        <option   class="other-opt " value="Philippines">Philippines</option> 
                                        <option   class="other-opt " value="Pitcairn">Pitcairn</option> 
                                        <option   class="other-opt " value="Poland">Poland</option> 
                                        <option   class="other-opt " value="Portugal">Portugal</option> 
                                        <option   class="other-opt " value="Puerto Rico">Puerto Rico</option> 
                                        <option   class="other-opt " value="Qatar">Qatar</option> 
                                        <option   class="other-opt " value="Reunion">Reunion</option> 
                                        <option   class="other-opt " value="Romania">Romania</option> 
                                        <option   class="other-opt " value="Russian Federation">Russian Federation</option> 
                                        <option   class="other-opt " value="Rwanda">Rwanda</option> 
                                        <option   class="other-opt " value="Saint Helena">Saint Helena</option> 
                                        <option   class="other-opt " value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
                                        <option   class="other-opt " value="Saint Lucia">Saint Lucia</option> 
                                        <option   class="other-opt " value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
                                        <option   class="other-opt " value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option> 
                                        <option   class="other-opt " value="Samoa">Samoa</option> 
                                        <option   class="other-opt " value="San Marino">San Marino</option> 
                                        <option   class="other-opt " value="Sao Tome and Principe">Sao Tome and Principe</option> 
                                        <option   class="other-opt " value="Saudi Arabia">Saudi Arabia</option> 
                                        <option   class="other-opt " value="Senegal">Senegal</option> 
                                        <option   class="other-opt " value="Serbia and Montenegro">Serbia and Montenegro</option> 
                                        <option   class="other-opt " value="Seychelles">Seychelles</option> 
                                        <option   class="other-opt " value="Sierra Leone">Sierra Leone</option> 
                                        <option   class="other-opt " value="Singapore">Singapore</option> 
                                        <option   class="other-opt " value="Slovakia">Slovakia</option> 
                                        <option   class="other-opt " value="Slovenia">Slovenia</option> 
                                        <option   class="other-opt " value="Solomon Islands">Solomon Islands</option> 
                                        <option   class="other-opt " value="Somalia">Somalia</option> 
                                        <option   class="other-opt " value="South Africa">South Africa</option> 
                                        <option   class="other-opt " value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option> 
                                        <option   class="other-opt " value="Spain">Spain</option> 
                                        <option   class="other-opt " value="Sri Lanka">Sri Lanka</option> 
                                        <option   class="other-opt " value="Sudan">Sudan</option> 
                                        <option   class="other-opt " value="Suriname">Suriname</option> 
                                        <option   class="other-opt " value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
                                        <option   class="other-opt " value="Swaziland">Swaziland</option> 
                                        <option   class="other-opt " value="Sweden">Sweden</option> 
                                        <option   class="other-opt " value="Switzerland">Switzerland</option> 
                                        <option   class="other-opt " value="Syrian Arab Republic">Syrian Arab Republic</option> 
                                        <option   class="other-opt " value="Taiwan, Province of China">Taiwan, Province of China</option> 
                                        <option   class="other-opt " value="Tajikistan">Tajikistan</option> 
                                        <option   class="other-opt " value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
                                        <option   class="other-opt " value="Thailand">Thailand</option> 
                                        <option   class="other-opt " value="Timor-Leste">Timor-Leste</option> 
                                        <option   class="other-opt " value="Togo">Togo</option> 
                                        <option   class="other-opt " value="Tokelau">Tokelau</option> 
                                        <option   class="other-opt " value="Tonga">Tonga</option> 
                                        <option   class="other-opt " value="Trinidad and Tobago">Trinidad and Tobago</option> 
                                        <option   class="other-opt " value="Tunisia">Tunisia</option> 
                                        <option   class="other-opt " value="Turkey">Turkey</option> 
                                        <option   class="other-opt " value="Turkmenistan">Turkmenistan</option> 
                                        <option   class="other-opt " value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
                                        <option   class="other-opt " value="Tuvalu">Tuvalu</option> 
                                        <option   class="other-opt " value="Uganda">Uganda</option> 
                                        <option   class="other-opt " value="Ukraine">Ukraine</option> 
                                        <option   class="other-opt " value="United Arab Emirates">United Arab Emirates</option> 
                                        <option   class="other-opt " value="United Kingdom">United Kingdom</option> 
                                        <option   class="other-opt " value="United States">United States</option> 
                                        <option   class="other-opt " value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
                                        <option   class="other-opt " value="Uruguay">Uruguay</option> 
                                        <option   class="other-opt " value="Uzbekistan">Uzbekistan</option> 
                                        <option   class="other-opt " value="Vanuatu">Vanuatu</option> 
                                        <option   class="other-opt " value="Venezuela">Venezuela</option> 
                                        <option   class="other-opt " value="Vietnam">Vietnam</option> 
                                        <option   class="other-opt " value="Virgin Islands, British">Virgin Islands, British</option> 
                                        <option   class="other-opt " value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
                                        <option   class="other-opt " value="Wallis and Futuna">Wallis and Futuna</option> 
                                        <option   class="other-opt " value="Western Sahara">Western Sahara</option> 
                                        <option   class="other-opt " value="Yemen">Yemen</option> 
                                        <option   class="other-opt " value="Zambia">Zambia</option> 
                                        <option   class="other-opt " value="Zimbabwe">Zimbabwe</option> 
                                    </select>
                                </div>
                            </div>
                            <!-- City -->
                            <div class="form-group">
                                <div class="col-lg-12"><input type="text" name="city" class="form-control" placeholder="City"  value="<?php echo set_value('city');?>">
                                <?php if(form_error('city')){?>
				                   <div class="alert-error" id="3">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(3);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('city');?>
				                   </div>
				               <?php } ?>
                                	
                                </div>
                            </div>
                            <!-- ADDRESS -->
                            <div class="form-group">
                                <div class="col-lg-12"><input type="text" name="address" class="form-control" placeholder="Address"  value="<?php echo set_value('address');?>">
                                <?php if(form_error('address')){?>
				                   <div class="alert-error" id="4">
        			                 <a href="javascript:void();" class="close" onclick="removeErrorMessage(4);" data-dismiss="alert" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('address');?>
				                   </div>
				               <?php } ?>
                                	
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-12">
                                    <textarea class="form-control" rows="4" id="inputMessage" name="message" placeholder="Your message..." ></textarea>
                                    <?php if(form_error('message')){?>
				                   <div class="alert-error" id="5">
        			                 <a href="javascript:void();" class="close" data-dismiss="alert" onclick="removeErrorMessage(5);" style="float: left;color: #ff0808;">&times;&nbsp;</a>
        			<?php echo form_error('message');?>
				                   </div>
				               <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-lg btn-primary checkout" value="Order Placed"> Order Placed </button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
       <div class="col-md-6 content-send-order animate-show animate-hide" aria-hidden="true">
            <div class="row">
                <div class="col-25" style="margin-top: 38px;">  
			      <h4 align="center" style="font-size: 24px;">Your Order</h4>
			      <table>  
                    <thead>
    <tr>
      <th scope="col">Product</th>
      <th scope="col">Qty</th>
      <th scope="col">Amt</th>
    </tr>
  </thead>
    <tbody>
    <?php 
    $grand_total = 0;
    if ($cart = $this->CartModel->getAddToCatData()):?>
    <?php
    echo form_open('update-cart');
    $i = 1;
    foreach ($cart as $item):
    	$product = $this->CommonFunctionModel->getProductUsingId($item['id']);
	
        echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
        echo form_hidden('cart[' . $item['id'] . '][rowid]', $item['rowid']);
        echo form_hidden('cart[' . $item['id'] . '][name]', $item['name']);
        echo form_hidden('cart[' . $item['id'] . '][price]', $item['price']);
        echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
        echo form_hidden('cart[' . $item['id'] . '][image]', $item['image']);?>
    
	    <tr>
	      <td data-label="Account"><?php echo $product->title; ?></td>
	      <td data-label="Due Date"><?php echo $item['qty']; ?></td>
	      <td data-label="Amount">&#8377;<?php echo $item['subtotal']; ?></td>   
	    </tr>
	    <?php $grand_total = $grand_total + $item['subtotal'];
         $i++;
         endforeach; 
         endif;
        ?> 
    <tr>
    	<td scope="row" colspan="3"><hr style="width: 565px;"></td>
	</tr>
	<tr>
    <td scope="row">Total</td>
    <td scope="row" ></td>
    <td scope="row"><?php echo $grand_total; ?></td>
	</tr>
	<?php echo form_close(); ?>
  </tbody> 
                  </table>
                    <div align="center" style="display:none; margin-top: 10px;">
                      <a href="<?php echo base_url().'website/products/buy/'.$this->session->userdata('user_id') ?>">
				        	<img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" alt="Checkout PayPal">
				      </a>	
				    </div>
				    
				    <!--	Payment Method	-->
				    <select class="form-control" name="paymetMethod" id="paymentMethod" onchange="getPaymentMethod(this.value)">
			    		<option value="">Select Payment Method</option>
			    		<option value="bank">Bank Payment</option>
			    		<option value="online">Online Payment</option>
			    	</select>
				    <form action="" method="post" name="bank_transfer" id="bankTransfer" class="form-horizontal" style="display:none;">
				    	<h2>Bank Payment Form</h2>
				    	<input type="text" name="bankName" id="bankName" value="" placeholder="Enter Bank Name"/>
				    	<input type="text" name="transId" id="transId" value="" placeholder="Enter Transaction Number"/>
				    	<input type="date" class="form-control" name="transDate" id="transDate" value="" placeholder="Enter Transaction Date" />
				    	<style>
				    		#transDate {
							    border: 1px solid #ccc;
							    border-radius: 3px;
							}
					    	#bankSave {
							    width: 100px;
							}
						</style>
				    	<input type="submit" class="btn btn-lg btn-primary checkout" name="bankSave" id="bankSave" value="Save"  />
				    </form>
				    <form method="post" class="form-horizontal" action="<?php echo $action; ?>" name="payuForm" id="payuForm" style="display: none;">

				<input type="hidden" name="key" value="<?php echo (!isset($posted['key'])) ? '' : $posted['key'] ?>" />
				<input type="hidden" id="hash" name="hash" value="<?php echo (!isset($posted['hash'])) ? '' : $posted['hash'] ?>"/>
				<input type="hidden" name="txnid" value="<?php echo (!isset($posted['txnid'])) ? '' : $posted['txnid'] ?>" />

				<textarea name="productinfo" style="display:none;"><?php echo (empty($posted['productinfo'])) ? $productJSON : $posted['productinfo'] ?></textarea>
				
				<input type="hidden" name="surl" value="<?php echo (!isset($posted['surl'])) ? '' : $posted['surl'] ?>" size="64" />
				<input type="hidden" name="curl" value="<?php echo (!isset($posted['curl'])) ? '' : $posted['curl'] ?>" size="64" />
				<input type="hidden" id="furl" name="furl" value="<?php echo (!isset($posted['furl'])) ? '' : $posted['furl'] ?>" size="64" />
				<input type="hidden" name="service_provider" value="<?php echo (!isset($posted['service_provider'])) ? '' : $posted['service_provider'] ?>" size="64" />
				<input type="hidden" name="order_id" value="<?php echo (!isset($posted['order_id'])) ? '' : $posted['order_id'] ?>" size="64" />
				<?php $LoginUserData = $this->CommonFunctionModel->checkUser(); ?>
				      <input type="hidden" class="form-control" name="firstname" id="firstname" placeholder="Your User Name" required value="<?php echo (!isset($posted['username'])) ? $LoginUserData->user_name : $posted['username'] ?>">
				      <input type="hidden" name="email" id="email" class="form-control" placeholder="Your Email" value="<?php echo (!isset($posted['email'])) ? $LoginUserData->email : $posted['email'] ?>" required>
				    
				      <input type="hidden" name="phone" class="form-control" id="inputPassword3" placeholder="Your Mobile Number" value="<?php echo (!isset($posted['phone'])) ? $LoginUserData->mobile : $posted['phone'] ?>" required>
				      <input type="hidden" name="amount" class="form-control" id="inputPassword3" placeholder="Amount to Pay" value="<?php echo (!isset($posted['amount'])) ? $grand_total : $posted['amount'] ?>" required>
				      
				      <button type="submit" align="center" style="margin-top: 10px;"><img src="http://localhost/payumoney/payumoney_logo.png" alt="Checkout PayPal"></button>
				</form>	      
              </div>
           </div>
       </div>
    </div>
</section>
<script>
function getPaymentMethod(value)
{
	$('#payuForm').hide();	
	if(value == "online"){
		$('#payuForm').show();	
		$('#bankTransfer').hide();
	}else if(value == "bank"){
		$('#payuForm').hide();	
		$('#bankTransfer').show();
	}
}
</script>
    <script>
    	function changeQty(incDec,ind)
    	{
    		var total = "0";
    		var val = $('#qty_'+ind).val();
    		if(incDec == "plus")
    		{
				total = parseInt(val) + 1;	
				$('#qty_'+ind).val(total);	
			}
    		else{			
				total = parseInt(val) - 1;	
				if(total != 0){
					$('#qty_'+ind).val(total);		
				}				
			}  		
		}
        function removeErrorMessage(id)
	   {
		  $('#'+id).html('');
	   }
    </script>
  
<script>
function submitForm(){
	var hash = '<?php echo $hash ?>';    
    if(hash) {
     	if(hash == '') {
        	return;
      		alert('hash');
      	}
      	document.getElementById("payuForm").submit();
    }
} 
submitForm();
	
</script>
