    <section class="container">
        <div class="row">
            <div class="col-lg-12 text-center shop-title-store">
                <h1 class="section-title">Your Order</h1>
            </div>
            <div class="col-md-12 box-blog">
                <div class="content-table-cart">
                    <div class="col-md-6 number-cart-item"><p class="total-items">You Have 3 Items In Your Cart</p></div>
                    <div class="bt-tourn-back">
                        <a href="<?php echo base_url();?>">
                            <i class="fa fa-power-off" alt="back to store" title="back to store"></i>back to store
                        </a>
                    </div>
                    <!-- items -->
                    <table class="table tab-shop">

                        <!-- header -->
                        <?php 
                        $grand_total = 0;
                        if ($cart = $this->CartModel->getAddToCatData()):?>
                        <tbody>
                        
                            <tr class="tab-top-txt">
                                <td><p alt="Product" title="Product">Product</p></td>
                                <td class="tdCenter"><p alt="Quantity" title="Quantity">Quantity</p></td>
                                <td class="tdRight"><p alt="Price" title="Price">Price</p></td>
                                <td class="tdRight"><p alt="Total" title="Total">Total</p></td>
                                <td></td>
                            </tr>

                            <!-- cart items -->
                            <?php
	                        echo form_open('update-cart');
				            $i = 1;
				            foreach ($cart as $item):
				            	$product = $this->CommonFunctionModel->getProductUsingId($item['id']);
                        		
				                echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
				                echo form_hidden('cart[' . $item['id'] . '][rowid]', $item['rowid']);
				                echo form_hidden('cart[' . $item['id'] . '][name]', $item['name']);
				                echo form_hidden('cart[' . $item['id'] . '][price]', $item['price']);
				                echo form_hidden('cart[' . $item['id'] . '][qty]', $item['qty']);
				                echo form_hidden('cart[' . $item['id'] . '][image]', $item['image']);?>
	                            <tr>
                                <td class="cart-column-img">
                                    <div class="col-md-3 cart-prod-img">
                                        <img alt="Divina Commedia" src="<?php echo base_url();?>assets/image/book/<?php echo $item['image']; ?>">
                                    </div>
									<p><b><?php echo $product->title; ?></b></p>
                                </td>
                                <td class="cart-quantity">
                                    <div class="input-append">
                                        
                                        <input class="btn bt-minus-prod" type="button" value="-" aria-disabled="true" onclick="changeQty('minus',<?php echo $i; ?>)">
                                        <?php echo form_input('cart[' . $item['id'] . '][qty]', $item['qty'], 'maxlength="3" size="1" class="text-center quantity-prod" id="qty_'.$i.'"'); ?>
                                        <input class="btn btn-lg btn-primary bt-plus-prod" type="button" value="+" tabindex="0" aria-disabled="false"  onclick="changeQty('plus',<?php echo $i; ?>)" >

                                    </div>
                                </td>
                                <td class="tdRight"><p>&#8377;<?php echo $item['price']; ?></p></td>
                                <td class="tdRight"><p>&#8377;<?php echo $item['subtotal']; ?></p></td>
                                <td class="ic-remove" title="remove from cart">
                                    <a href="<?php echo base_url() ?>remove-cart/<?php echo $item['rowid'];?>">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                            
                            <!-- footer -->
                            <?php $grand_total = $grand_total + $item['subtotal']; ?>
                            <?php
                             $i++;
                             endforeach; 
                             endif;
                            ?> 
                            <tr class="cart-total">
                                <td><button type="submit" class="btn btn-lg btn-primary cart-clear-all" alt="Update Cart" title="Update Cart" >Update Cart<i class="icon-trash icon-white"></i></button></td>
                                <td class="tdCenter"><p></p></td>
                                <td class="tdCenter"><p alt="Total" title="Total"><b>Total:</b></p></td>
                                <td class="tdRight"><p>&#8377;<?php echo $grand_total; ?>/- </p></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="btn-checkout-prod">
                                    <div class="btn-payment">
                                        <a href="<?php echo base_url() ?>check_out"><button class="btn btn-lg btn-primary checkout" type="button" alt="Process To Checkout" title="Process To Checkout">
                                            <i class="icon-ok icon-white"></i> Process To Checkout
                                        </button></a>
                                    </div>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section><?php echo form_close(); ?>

    <script>
    	function changeQty(incDec,ind)
    	{
    		var total = "0";
    		var val = $('#qty_'+ind).val();
    		if(incDec == "plus")
    		{
				total = parseInt(val) + 1;	
				$('#qty_'+ind).val(total);	
			}
    		else{
				
				total = parseInt(val) - 1;	
				if(total != 0){
					$('#qty_'+ind).val(total);		
				}
				
			}
    		
		}
    </script>