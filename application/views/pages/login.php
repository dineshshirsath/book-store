<style>
    .register-top-second{
        color:grey;
    }
    #first .word2{
        color:black;
    }
</style>
    <section class="content-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-promotion login-content-txt
                    ">
                        <h1 class="register-top-title tlt" id="first" style="color: #f29a33;">Special Offers</h1>
                        <h1 class="register-top-title register-top-second tlt2"> on all Books</h1>
                    </div>
                    <div class="fadeInRight animated abc-book" style="top: 45px;">
                        <img src="<?php echo base_url();?>assets/image/book/bookback.gif" alt="The Book Lounge" title="The Book Lounge" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container">
        <div class="row">
            <div class="col-lg-12 table-padding">
                <div class="register-img-background pg-login"></div>
                <div id="loginbox" class="mainbox col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title" alt="Login" title="Login">Login</div>
                            <div class="forgot-pwd">
                                <a href="<?php echo base_url(); ?>forgat-password-view" alt="Forgot your password" title="Forgot your password"><button class="btn btn-primary">Forgot your password?</button></a>
                            </div>
                        </div>
                        <div style="padding-top: 20px" class="panel-body">
                            <form name="loginForm" class="form-horizontal" method="post" action="<?php echo base_url() ?>check-login">

                                <div style="margin-top: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-send"></i></span>
                                    <input type="text" class="form-control" name="username" placeholder="Email or Username" required />
                                </div>
                                
                                <div style="margin-top: 25px" class="input-group">
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-lock"></i>
                                    </span>
                                    <input type="password" class="form-control" name="password" placeholder="Password" required />
                                </div>

                                <div style="margin-top: 20px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls">
                                        <input type="submit" class="btn btn-success login-btn" value="Log In" alt="submit" title="submit">
                                    </div><br /><br />

                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>