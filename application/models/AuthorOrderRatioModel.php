<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class AuthorOrderRatioModel extends CI_Model
{
	public function getAuthorOrderRatioData()
	{
		$query = $this->db->select('u.*')
				->from('tbl_author_order_ratio as aor')
				->join('tbl_users as u', 'u.id = aor.author_id', 'left')
				->group_By('u.id')
				->get();

		return $query->result_array();	
	}

	public function getAuthorOrderRatioByAuthorId($id)
	{
		$query = $this->db->select('aor.*, u.first_name, u.last_name, p.title')
				->from('tbl_author_order_ratio as aor')
				->join('tbl_users as u', 'u.id = aor.author_id', 'left')
				->join('tbl_product as p', 'p.id = aor.book_id', 'left')
				->where('author_id', $id)
				->get();

		return $query->result_array();	
	}

	public function insert($data)
	{
		if ($this->db->insert('tbl_author_order_ratio', $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getAuthorDate()
	{
		$query = $this->db->select('id, first_name, last_name')
				->from('tbl_users')
				->where('type', 's')
				->get();

		return $query->result_array();	
	}

	public function getAuthorRatioData()
	{
		$query = $this->db->select('aor.*, u.first_name, u.last_name, p.title')
				->from('tbl_author_order_ratio as aor')
				->join('tbl_users as u', 'u.id = aor.author_id', 'left')
				->join('tbl_product as p', 'p.id = aor.book_id', 'left')
				->where('author_id', $this->session->userdata('user_id'))
				->get();

		return $query->result_array();
	}
}

?>
