<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class SubCategoryModel extends CI_Model
{
	public function getSubCategoryListingData()
	{
		$query = $this->db->select('sc.*, c.category_name')
				->from('tbl_sub_category sc')
				->join('tbl_category c','c.id = sc.category_id', 'left')
				->get();

		return $query->result_array();	
	}

	public function getCategoryData()
	{
		$query = $this->db->select('id, category_name')
				->from('tbl_category')
				->get();

		return $query->result_array();	
	}

	public function getSubCategoryData()
	{
		$query = $this->db->select('id, sub_category_name')
				->from('tbl_sub_category')
				->get();

		return $query->result_array();	
	}

	public function insert($data)
	{
		if ($this->db->insert('tbl_sub_category', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getUpdatsubCategoryData($id)
	{
		$query = $this->db->select('*')
				->from('tbl_sub_category')
				->where('id', $id)
				->get();
				
		return $query->result_array();	
	}

	public function update($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_sub_category', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		if ($this->db->delete('tbl_sub_category')) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getSubCategoryByCatID()
	{
		$response = array();
		
		$query = $this->db->select('id,sub_category_name')
				->from('tbl_sub_category')
    			->where('category_id', $this->input->post('category_id'))
    			->get();
   		$response = $query->result_array();
		return ($this->db->affected_rows()> 0 )?$response:array('id'=>0);
	}	
}
?>
