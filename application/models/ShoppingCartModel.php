<?php defined('BASEPATH') OR exit('No direct script access allowed');
class ShoppingCartModel extends CI_Model{
      
	function __construct(){
		parent::__construct();
	}	
	
	public function getOrderListingData()
	{
		$query = $this->db->select('o.*, u.first_name, u.last_name, p.*')
			->from('tbl_order as o')
			->join('tbl_users as u','u.id = o.user_id', 'left')
			->join('tbl_product as p','p.id = o.product_ids', 'left')
			->get()->result_array();

		return $query;
	}

	public function getPaymentListingData()
	{
		$query = $this->db->select('p.*, u.first_name, u.last_name')
			->from('tbl_payment as p')
			->join('tbl_users as u','u.id = p.user_id', 'left')
			->get()->result_array();

		return $query;
	}

	public function paymentProductDetails($id)
	{
		$query = $this->db->select('pd.*, p.*')
			->from('tbl_payment_detail as pd')
			->join('tbl_product as p','p.id = pd.product_id', 'left')
			->get()->result_array();

		return $query;
	}

	public function getSellProductList()
	{
		$query = $this->db->select('p.*, u.first_name, u.last_name')
			->from('tbl_payment as p')
			->join('tbl_users as u','u.id = p.user_id', 'left')
			->join('tbl_payment_detail as pd','pd.payment_id = p.id', 'left')
			->join('tbl_product as pro','pro.id = pd.product_id', 'left')
			->where('pro.addedby_id', $this->session->userdata('user_id'))
			->group_By('pro.addedby_id')
			->get()->result_array();

		return $query;
	}

	public function getBookPurchaseUsers()
	{
		$query = $this->db->select('u.*')
			->from('tbl_payment as p')
			->join('tbl_users as u','u.id = p.user_id', 'left')
			->join('tbl_payment_detail as pd','pd.payment_id = p.id', 'left')
			->join('tbl_product as pro','pro.id = pd.product_id', 'left')
			->where('pro.addedby_id', $this->session->userdata('user_id'))
			->group_By('pro.addedby_id')
			->get()->result_array();

		return $query;
	}

}
?>