<?php defined('BASEPATH') OR exit('No direct script access allowed');
class ProductModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function getProductListingData()
	{
		$query = $this->db->select('p.*, c.category_name, sc.sub_category_name')
				->from('tbl_product p')
				->join('tbl_category c','c.id = p.category_id', 'left')
				->join('tbl_sub_category sc', 'sc.id = p.sub_category_id', 'left')
				->where('addedby_id', $this->session->userdata('user_id'))
				->where('delete_status', '0')
				->get()->result_array();

		return $query;
	}

	public function getBookTitleList($id)
	{
		$query = $this->db->select('*')
				->from('tbl_product')
				->where('id', $id)
				->get();
				
		return $query->result_array();	
	}
	
	public function insert($data)
	{
		if ($this->db->insert('tbl_product', $data)) 
		{
   			return true;			
		}
		else
		{
			return false;
		}
	}
	
	public function getUpdatProductData($id)
	{
		$query = $this->db->select('*')
				->from('tbl_product')
				->where('id', $id)
				->get();
				
		return $query->result_array();	
	}

	public function update($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_product', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getProductData()
	{ 
		$query = $this->db->select('p.*,AVG(br.rating) rates,COUNT(br.book_id) as num_rating')
		  	->from('tbl_product as p')
		  	->join('tbl_book_rating as br','br.book_id = p.id','left')
		  	->where('p.status', 'p')
		  	->where('p.delete_status', '0')
		  	->group_By('p.id')
		  	->get();
		return $query->result_array();
	}

	public function getPdfData()
	{
		$query = $this->db->select('*')
			->from('tbl_book_pdf')
			->get();

		return $query->result_array();
	}

	public function getProductDetails($id)
	{ 
		$query = $this->db->select('p.*,AVG(br.rating) rates,')
						  ->from('tbl_product as p')
						  ->join('tbl_book_rating as br','p.id=br.book_id','left')
						  ->where('p.id',$id)
						  ->get();

		return $query->result_array();
	}

	public function delete($id, $data)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_product', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function insertPdfFile($data)
	{
		if ($this->db->insert_batch('tbl_book_pdf', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
?>