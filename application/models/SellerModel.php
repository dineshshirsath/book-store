<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class SellerModel extends CI_Model
{
	public function getSellerListingData()
	{
		$query = $this->db->select('u.*, kyc.id as kycId, kyc.status')
				->from('tbl_users as u')
				->join('tbl_kyc_document as kyc', 'kyc.seller_id = u.id', 'left')
				->where('u.type', 's')
				->get();

		return $query->result_array();	
	}

	public function getUpdatSellerData($id)
	{
		$query = $this->db->select('*')
				->from('tbl_kyc_document')
				->where('id', $id)
				->get();
				
		return $query->result_array();	
	}

	public function update($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_kyc_document', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function authorAddedBooks()
	{
		$query = $this->db->select('p.*, c.category_name, sc.sub_category_name')
			->from('tbl_product p')
			->join('tbl_category c','c.id = p.category_id', 'left')
			->join('tbl_sub_category sc', 'sc.id = p.sub_category_id', 'left')
			->where('addedby_status', 's')
			->where('delete_status', '0')
			->get();

		return $query->result_array();
	}

	public function authorBooksUpdateData($id)
	{
		$query = $this->db->select('*')
				->from('tbl_product')
				->where('id', $id)
				->get();
				
		return $query->result_array();	
	}

	public function authorBookUpdate($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_product', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function updateBookStatusById($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_product', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
?>
