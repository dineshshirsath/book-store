<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class NewsLetterModel extends CI_Model
{
	public function getnewsLetterListingData()
	{
		$query = $this->db->select('*')
				->from('tbl_news_letter')
				->get();

		return $query->result_array();	
	}

	public function insert($data)
	{
		$this->load->library('email');
		
		$config['mailtype'] = 'html';
		$this->email->initialize($config);

		$this->email->from($data['from'], 'The Book Lounge');
		$this->email->to($data['to']);
		$this->email->subject($data['subject']);
		$this->email->message($data['message']);

		$this->email->send();

		$insertData = array(
			'email_id' => $data['to'],
			'email_content' => $data['message']
		);

		if ($this->db->insert('tbl_news_letter_history', $insertData)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getnewsLetterHistoryListingData()
	{
		$query = $this->db->select('*')
				->from('tbl_news_letter_history')
				->get();

		return $query->result_array();
	}
}

?>
