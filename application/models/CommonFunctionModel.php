<?php defined('BASEPATH') OR exit('No direct script access allowed');
class CommonFunctionModel extends CI_Model{
      
	function __construct(){
		parent::__construct();
	}	
	public function checkSellerStatusAprovelorNot()
	{
		$result = $this->db->select('*')
			->from('tbl_kyc_document')
			->where('seller_id', $this->session->userdata('user_id'))
			->get()->result_array();

		return $result;
	}
	function getProductUsingId($productId)
	{
		$this->db->where('id',$productId);
		return $this->db->get('tbl_product')->row();
	}
	function getAddToCatData()
	{
		return $this->cart->contents();
	}
	function checkBook($addedUser)
	{
		$this->db->where('type','s');
		$this->db->where('id',$addedUser);
		$this->db->get('tbl_users');
		return $this->db->affected_rows();
	}
	function checkUser()
	{
		$this->db->where('id',$this->session->userdata('user_id'));
		return $this->db->get('tbl_users')->row();
	}
	function checkAuthorAgreeOrNot($id){
		$this->db->where('id',$id);
		$this->db->where('status','0');
		$this->db->get('tbl_users')->result_array();
		return $this->db->affected_rows();
	}
	function checkProfileImage($id)
	{
		$query = $this->db->select('*')
			->from('tbl_users')
			->where('id', $id)
			->get()->result_array();

		return $query;
	}
	function getProductIdWise($productId)
	{
		$this->db->where('id',$productId);
		return $this->db->get('tbl_product')->row();
	}
	function random_number()
	{
		$length="6"; // Define length of the Random string.
		$char="0123456789"; // Define characters which you needs in your random string
		$random=substr(str_shuffle($char), 0, $length); // Put things together.
		return $random;
	}
	public function getReviewData($book_id)
	{
		$query = $this->db->select('*')
				->from('tbl_review')
				->where('book_id', $book_id)
				->order_by('id','desc')
				->limit(5)
				->get()->result_array();

		return $query;
	}

	public function getAllReviewData($book_id)
	{
		$query = $this->db->select('*')
				->from('tbl_review')
				->where('book_id', $book_id)
				->order_by('id','desc')
				->get()->result_array();

		return $query;
	}

	public function getCountLikeDislike($reviewId, $condition)
	{
		$query = $this->db->select('COUNT(review_id) as total')
				->from('tbl_like_dislike_review')
				->where('review_id', $reviewId)
				->where('like_dislike', $condition)
				->get()->row();

		return $query;
	}
	function checkLoggedUserIsLikeDislike($reviewId, $condition){
		$query = $this->db->select('*')
				->from('tbl_like_dislike_review')
				->where('review_id', $reviewId)
				->where('like_dislike', $condition)
				->where('user_id', $this->session->userdata('user_id'))
				->get()->result_array();

		if($this->db->affected_rows() > 0 ){
			$color = "0dc1c1";
			return $color; 
		}else{
			$color = "#000";
			return $color;
		}
	}

	public function getReviewRating($bookId, $i)
	{
		$query = $this->db->select('*,SUM(rating) as SumTotal,COUNT(rating) as star')
				->from('tbl_review')
				->where('book_id', $bookId)
				->where('rating', $i)
				->get()->result_array();
        
		return $query;
	}
}
?>