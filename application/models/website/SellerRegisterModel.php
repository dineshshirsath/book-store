<?php defined('BASEPATH') OR exit('No direct script access allowed');
class SellerRegisterModel extends CI_Model{
      
	function __construct(){
		parent::__construct();
	}	
	public function insert()
	{
		$data = array(

			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'user_name' => $this->input->post('user_name'),
			'password' => $this->input->post('password'),
			'address' => $this->input->post('address'),
			'mobile' => $this->input->post('mobile'),
			'gender' => $this->input->post('gender'),
			'type' => 's'
		);

		//return $this->db->insert('tbl_users', $data);
		if ($this->db->insert('tbl_users',$data)) 
		{
			$sellerId = $this->db->insert_id();
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userName'=>$this->input->post('first_name').' '.$this->input->post('last_name'),'performActivity'=>'Author Registration','staus'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			return $sellerId;
		}
		else
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userName'=>$this->input->post('first_name').' '.$this->input->post('last_name'),'performActivity'=>'Author Registration','staus'=>'failed');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			return false;
		}
	}

	public function KYCUpload($data)
	{
		if ($this->db->insert('tbl_kyc_document', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>