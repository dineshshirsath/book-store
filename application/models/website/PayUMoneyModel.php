<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PayUMoneyModel extends CI_Model {
	
	public function addPaymentDetail($data)
	{
		return $this->db->insert('tbl_payment',$data);
	}
	function addPayUMoneyTransData($data)
	{
		return $this->db->insert_batch('tbl_payment_detail',$data);
	}
}
?>