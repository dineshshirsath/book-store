<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BillingDetailModel extends CI_Model {
	
	public function insertBillingData($product)
	{
		$product_ids = implode(',',$product);;
		$data = array(
		    'fullname'=> $this->input->post('name'),
		    'user_id'=>$this->session->userdata('user_id'),
		    'address'=> $this->input->post('address'),
			'email'=> $this->input->post('email'),
			'country'=> $this->input->post('country'),
			'city'=> $this->input->post('city'),
			'message'=> $this->input->post('message'),
			'product_ids'=>$product_ids	
		);
		$this->db->insert('tbl_order',$data);
		return $billing_id = $this->db->insert_id();
	}
}
?>