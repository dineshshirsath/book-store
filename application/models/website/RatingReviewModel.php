<?php

/**
 * 
 */
class RatingReviewModel extends CI_Model
{
	public function saveData($data)
	{
		$result = $this->db->where('book_id', $data['book_id'])
		         		->where('user_id', $data['user_id'])
		        		->get('tbl_book_rating')
		         		->result_array();
		if (empty($result)) {
			$this->db->insert('tbl_book_rating',$data);
			return true;
		}else{
			return false;
		}
		
	}
	public function getLikeDislikeExists($reviewId, $userId)
	{
		$query = $this->db->select('*')
			->from('tbl_like_dislike_review')
			->where('review_id', $reviewId)
			->where('user_id', $userId)
			->get();

		return $query->result_array();	
	}

	public function insertLikeDislike($data)
	{
		if ($this->db->insert('tbl_like_dislike_review',$data)){
			return true;
		}else{
			return false;
		}
	}

	public function updataLikeDislike($id, $data)
	{	
		$this->db->where('id', $id);
		if ($this->db->update('tbl_like_dislike_review',$data)){
			return true;
		}else{
			return false;
		}
	}

	public function addReview($data)
	{
		if ($this->db->insert('tbl_review', $data)) {
			return true;
		}else{
			return false;
		}
	}
}
?>