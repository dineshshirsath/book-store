<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterModel extends CI_Model {
	
	public function insertRegistrationData()
	{
		$data = array(
		    
			'first_name'=> $this->input->post('first_name'),
			'last_name'=> $this->input->post('last_name'),
			'user_name'=> $this->input->post('user_name'),
			'email'=> $this->input->post('email'),
			'password'=> $this->input->post('password'),
			'gender'=> $this->input->post('gender'),
			'address'=> $this->input->post('address'),
			'mobile'=> $this->input->post('mobile'),
			'type'=>'u'
			
		);
		//return $this->db->insert('tbl_users',$data);
		if ($this->db->insert('tbl_users',$data)) 
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userName'=>$this->input->post('first_name').' '.$this->input->post('last_name'),'performActivity'=>'Reder Registration','status'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			return true;
		}
		else
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userName'=>$this->input->post('first_name').' '.$this->input->post('last_name'),'performActivity'=>'Reder Registration','status'=>'failed');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			return false;
		}
	}
	function checkLogin()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->db->where("(tbl_users.email = '$username' OR tbl_users.user_name = '$username')");
		$this->db->where('password', $password);
		$result = $this->db->get('tbl_users');

		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('user_email');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_type');
		$this->session->unset_userdata('system_ip');

		if($result->num_rows() > 0)
		{
			$user = $result->row_array();			
			$this->session->set_userdata([
				'user_name'=>$user['user_name'],
				'user_email'=>$user['email'],
				'user_id'=>$user['id'],
				'user_type' =>$user['type'],
				'system_ip' => get_client_ip()
			]);
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'userName'=>$this->session->userdata('user_name'),'performActivity'=>'User Login','status'=>'success');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			return 1;
		}
		else
		{
			//------ Stroe Logs --------
				$system_ip = get_client_ip();
			    $data = array('userId'=>$this->session->userdata('user_id'),'userName'=>$this->session->userdata('user_name'),'performActivity'=>'User Login','status'=>'failed');
			    $activity = json_encode($data);
			    logs($system_ip,$activity);
			//------ Stroe Logs --------
			return 0;
		}
	}

	public function forgotPassword($email)
	{
		$result = $this->db->select('id')->where('email',$email)->where('type !=','a')->get('tbl_users')->row();
		if(!empty($result)){
			$url = base_url().'reset-password?id='.base64_encode($result->id);
			$this->load->library('email');
			
			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('datafalconx@gmail.com', 'The Book Lounge');
			$this->email->to($email);
			$this->email->subject('Reset Password.');
			$this->email->message('<!DOCTYPE html>
				<html lang="en">
				<head>
				<meta charset="utf-8">
				<title>The Book Lounge</title>

				</head>
				<body style="padding-top: 60px;padding-bottom: 40px;">
				    <div class="fixed-header" style=" width: 100%;position: fixed;background: #fff;padding: 10px 0;color: #030303;top: 0;">
				        <div class="container" style="width: 80%;margin: 0 auto;">
				            <nav>
				                <div class="col-md-3 content-logo">
				                   <h2 style="text-decoration:none;font-size: 33px;">The Book<span style="color: green;"> Lounge</span></h2>
				                </div>
				            </nav>
				        </div>
				        <hr>
				    </div>
				    <div class="container">
				    	<div style="background-color: lightgrey;width: 300px;border: 25px solid green;padding: 30px;margin: 74px;margin-left: 359px;">
                         <h2>To Reset Your Password please click below link.</h2>
                             <a href="http://thebooklounge.in/author-reset-password?id=NTQ=" target="_blank" rel="noreferrer"> Click Here </a>
                       </div>
				    </div>    
				    <div class="fixed-footer" style=" width: 100%;position: fixed;background: black;padding: 10px 0;color: white;bottom: 0; ">
				        <div class="container" style="margin-left: 20px;">&copy; <?php echo date("Y"); ?> All Rights Reserved by <a href="#" target="_blank" style="color: green;">FalconX.in</a>. Design by <a href="http://orbs-solutions.com/" target="_blank" style="color: green;"> Orbs Solutions</a></div>        
				    </div>
				</body>
				</html>                ');

			$this->email->send();	
			return 1;
		}
		return 0;
	}
	function updatePassword($id,$password){
		$data['password'] = $password;
		$this->db->where('id',$id);
		$this->db->update('tbl_users',$data);
		return $this->db->affected_rows();
	}
}
?>