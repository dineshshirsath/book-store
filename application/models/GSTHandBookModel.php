<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class GSTHandBookModel extends CI_Model
{
	public function getBookDetails($id)
	{
		$system_ip = get_client_ip();
	    $data = array('userId'=>$this->session->userdata('user_id'),'performActivity'=>'Read Book','staus'=>'success');
	    $activity = json_encode($data);
	    logs($system_ip,$activity);
	    
		$this->db->where('book_title',$id);
		$this->db->where('status', '0');
		$this->db->order_by('order', 'asc');
		return $this->db->get('book_chapter')->result_array();
	}
	public function getSubChapterDetails($id)
	{
		$this->db->where('chapter_id',$id);
		$this->db->where('status', '0');
		$this->db->order_by('order', 'asc');
		return $this->db->get('tbl_book_sub_chapter')->result_array();
	}
	function getSubChapterDetailsIdWise($id){
		$this->db->where('id',$id);
		$this->db->where('status', '0');
		$this->db->order_by('order', 'asc');
		return $this->db->get('tbl_book_sub_chapter')->result_array();
	}
	function getChapterDetailsIdWise($id){
		$this->db->where('id',$id);
		$this->db->where('status', '0');
		$this->db->order_by('order', 'asc');
		return $this->db->get('book_chapter')->result_array();
	}
}
?>