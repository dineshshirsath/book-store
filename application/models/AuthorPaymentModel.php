<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class AuthorPaymentModel extends CI_Model
{
	public function getAuthorPaymentData()
	{
		$query = $this->db->select('ap.*, u.first_name, u.last_name')
				->from('tbl_author_payment as ap')
				->join('tbl_users as u', 'u.id = ap.author_id', 'left')
				->get();

		return $query->result_array();	
	}

	public function insert($data)
	{
		if ($this->db->insert('tbl_author_payment', $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getAuthorDate()
	{
		$query = $this->db->select('id, first_name, last_name')
				->from('tbl_users')
				->where('type', 's')
				->get();

		return $query->result_array();	
	}
}

?>
