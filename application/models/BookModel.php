<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class BookModel extends CI_Model
{
	public function getBookChapterListData($bookId)
	{
		$query = $this->db->select('*')
				->from('book_chapter')
				->where('book_title', $bookId)
				->where('status', '0')
				->get();

		return $query->result_array();	

	}

	public function getBookSubChapterListData($chapterId)
	{
		$query = $this->db->select('bsc.*, bc.chapter_name')
				->from('tbl_book_sub_chapter as bsc')
				->join('book_chapter as bc','bc.id = bsc.chapter_id', 'left')		
				->where('bsc.chapter_id', $chapterId)
				->where('bsc.status', '0')
				->get();

		return $query->result_array();	
	}

	public function insert($data)
	{
		if ($this->db->insert_batch('book_chapter', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function insertSubChapter($data)
	{
		if ($this->db->insert_batch('tbl_book_sub_chapter', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getUpdatChapterData($id)
	{
		$query = $this->db->select('bc.*, tp.title')
				->from('book_chapter as bc')
				->join('tbl_product as tp','tp.id = bc.book_title', 'left')		
				->where('bc.id', $id)
				->get();
				
		return $query->result_array();	
	}

	public function updateChapter($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('book_chapter', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getUpdatSubChapterData($id)
	{
		$query = $this->db->select('tbsc.*, tp.title, bc.chapter_name')
				->from('tbl_book_sub_chapter as tbsc')
				->join('tbl_product as tp','tp.id = tbsc.book_title', 'left')
				->join('book_chapter as bc','bc.id = tbsc.chapter_id', 'left')
				->where('tbsc.id', $id)
				->get();
				
		return $query->result_array();	
	}

	public function updateSubChapter($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_book_sub_chapter', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getBookDetailsByBookId($bookId)
	{
		$query = $this->db->select('*')
						  ->from('tbl_product')
						  ->where('id',$bookId)
						  ->get();

		return $query->result_array();
	}

	public function getChapterDataById($chapterId)
	{
		$query = $this->db->select('bc.id, bc.chapter_name, tp.id as bookId, tp.title')
				->from('book_chapter as bc')
				->join('tbl_product as tp','tp.id = bc.book_title', 'left')
    			->where('bc.id', $chapterId)
    			->get();

		return $query->result_array();
   	}

	public function getChpterByBookId()
	{
		$response = array();
		
		$query = $this->db->select('id,chapter_name')
				->from('book_chapter')
    			->where('book_title', $this->input->post('book_title'))
    			->get();
   		$response = $query->result_array();
		return ($this->db->affected_rows()> 0 )?$response:array('id'=>0);
	}

	public function deleteCapter($id, $data)
	{
		$chpaterId = $id;
		
		$this->db->where('id', $id);
		if ($this->db->update('book_chapter', $data)) 
		{
			$this->db->where('chapter_id', $chpaterId);
			if ($this->db->update('tbl_book_sub_chapter', $data)) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
			{
				return false;
			}
	}

	public function deleteSubCapter($id, $data)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_book_sub_chapter', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getBookId($id)
	{
		$query = $this->db->select('book_title')
				->from('book_chapter')
    			->where('id', $id)
    			->get();
		return $query->result_array();    	
	}

	public function getBookIdWithSubChapter($id)
	{
		$query = $this->db->select('book_title')
				->from('tbl_book_sub_chapter')
    			->where('id', $id)
    			->get();
		return $query->result_array();
	}

	public function checkSubChapterByChapterId($chapterId)
	{
		$query = $this->db->select('*')
			->from('tbl_book_sub_chapter')
			->where('chapter_id', $chapterId)
			->where('status', '0')
			->get();

		return $query->result_array();
	}

}

?>
