<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model{
      
	function __construct(){
		parent::__construct();
	}	
	public function checkLogin()
	{
	    $username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->db->where("(tbl_users.email = '$username' OR tbl_users.user_name = '$username')");
		$this->db->where('password', $password);
		$result = $this->db->get('tbl_users');
		
		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('user_email');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_type');

		if($result->num_rows() > 0){
			
			$user = $result->row_array(); 
			
			$system_ip = get_client_ip();
    	    $data = array('userId'=>$user['id'],'performActivity'=>'login','status'=>'success');
    	    $activity = json_encode($data);
    	    logs($system_ip,$activity);
    	    
			$this->session->set_userdata([
				'user_name'=>$user['first_name'],
				'user_email'=>$user['email'],
				'user_id'=>$user['id'],
				'user_type'=>$user['type']
			]);
			return 1;
		}else{
		    $system_ip = get_client_ip();
    	    $data = array('userId'=>'','LoginUsername'=>$username,'performActivity'=>'login','status'=>'failed');
    	    $activity = json_encode($data);
    	    logs($system_ip,$activity);
			return 0;
		}		
	}

	public function forgotPassword($email, $data)
	{
		$this->db->where('email', $email);
		if ($this->db->update('tbl_users', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function checkSellerKYCDocument()
	{
		$result = $this->db->select('u.*, kyc.id as kycId, kyc.status')
			->from('tbl_users as u')
			->where(['u.id'=>$this->session->userdata('user_id'), 'u.type'=>$this->session->userdata('user_type')])
			->join('tbl_kyc_document as kyc', 'kyc.seller_id = u.id', 'inner')
			->get()->result_array();

		return $result;
	}

}
?>