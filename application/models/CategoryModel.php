<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class CategoryModel extends CI_Model
{
	public function getCategoryListingData()
	{
		$query = $this->db->select('*')
				->from('tbl_category')
				->get();

		return $query->result_array();	
	}

	public function insert($data)
	{
		if ($this->db->insert('tbl_category', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getUpdatCategoryData($id)
	{
		$query = $this->db->select('*')
				->from('tbl_category')
				->where('id', $id)
				->get();
				
		return $query->result_array();	
	}

	public function update($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_category', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		if ($this->db->delete('tbl_category')) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function getCategoryData()
	{
		$query = $this->db->select('id, category_name')
				->from('tbl_category')
				->get();

		return $query->result_array();	
	}
}

?>
