<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


//Route Of Admin Side
$route['admin'] = ADMIN.'login/index';
$route['admin-login'] = ADMIN.'login/index';
$route['admin-forgot-password'] = ADMIN.'login/forgotPasswordView';
$route['forgot-password'] = ADMIN.'login/forgotPassword';
$route['author-reset-password'] = ADMIN.'login/resetPassword';
$route['admin-logout'] = ADMIN.'login/logout';
$route['admin-dashboard'] = ADMIN.'home/index';

$route['listing-category'] = ADMIN.ADMIN_CATEGORY.'CategoryController/index';
$route['add-category'] = ADMIN.ADMIN_CATEGORY.'CategoryController/add';
$route['store-category'] = ADMIN.ADMIN_CATEGORY.'CategoryController/insert';
$route['edit-category/(:any)'] = ADMIN.ADMIN_CATEGORY.'CategoryController/updateView/$1';
$route['delete-category/(:any)'] = ADMIN.ADMIN_CATEGORY.'CategoryController/delete/$1';

$route['listing-sub-category'] = ADMIN.ADMIN_SUB_CATEGORY.'SubCategoryController/index';
$route['add-sub-category'] = ADMIN.ADMIN_SUB_CATEGORY.'SubCategoryController/add';
$route['store-sub-category'] = ADMIN.ADMIN_SUB_CATEGORY.'SubCategoryController/insert';
$route['edit-sub-category/(:any)'] = ADMIN.ADMIN_SUB_CATEGORY.'SubCategoryController/updateView/$1';
$route['delete-sub-category/(:any)'] = ADMIN.ADMIN_SUB_CATEGORY.'SubCategoryController/delete/$1';

$route['listing-product'] = ADMIN.PRODUCT.'ProductController/index';
$route['add-product'] = ADMIN.PRODUCT.'ProductController/add';
$route['store-product'] = ADMIN.PRODUCT.'ProductController/insert';
$route['edit-product/(:any)'] = ADMIN.PRODUCT.'ProductController/updateView/$1';
$route['delete-product/(:any)'] = ADMIN.PRODUCT.'ProductController/delete/$1';
$route['add-pdf-file'] = ADMIN.PRODUCT.'ProductController/addPdfFile';
$route['store-pdf-file'] = ADMIN.PRODUCT.'ProductController/insertPdfFile';

$route['listing-author'] = ADMIN.SELLER.'SellerController/index';
$route['edit-author/(:any)'] = ADMIN.SELLER.'SellerController/updateView/$1';
$route['listing-author-book'] = ADMIN.SELLER.'SellerController/authorAddedBooks';
$route['edit-author-book/(:any)'] = ADMIN.SELLER.'SellerController/authorBookUpdateView/$1';

$route['listing-user'] = ADMIN.USER.'UserController/index';
$route['user-profile'] = ADMIN.USER.'UserController/userProfile';
$route['user-profile-update'] = ADMIN.USER.'UserController/update';
$route['user-change-password'] = ADMIN.USER.'UserController/changePassword';
$route['profile-image'] = ADMIN.USER.'UserController/changeProfileImage';

$route['handbook'] = ADMIN.'GST-handbook/GSTHandBookController/index';
$route['book-purchase-message'] = ADMIN.'GST-handbook/GSTHandBookController/book_purchase_msg';

$route['listing-book'] = ADMIN.BOOK.'BookController/index';
$route['add-book'] = ADMIN.BOOK.'BookController/add';
$route['add-book-chapter/(:any)'] = ADMIN.BOOK.'BookController/addBookChapter/$1';
$route['add-book-subchapter/(:any)'] = ADMIN.BOOK.'BookController/addBookSubChapter/$1';
$route['store-book'] = ADMIN.BOOK.'BookController/insert';
$route['store-sub-chapter'] = ADMIN.BOOK.'BookController/insertSubChapter';
$route['edit-book-chapter/(:any)'] = ADMIN.BOOK.'BookController/updateChpterView/$1';
$route['edit-book-sub-chapter/(:any)'] = ADMIN.BOOK.'BookController/updateSubChpterView/$1';
$route['delete-book-chapter/(:any)'] = ADMIN.BOOK.'BookController/deleteCapter/$1';
$route['delete-book-sub-chapter/(:any)'] = ADMIN.BOOK.'BookController/deleteSubCapter/$1';
$route['delete-book/(:any)'] = ADMIN.BOOK.'BookController/delete/$1';
$route['view-book/(:any)'] = ADMIN.BOOK.'BookController/preview/$1';

$route['listing-news-letter'] = ADMIN.NEWS_LETTER.'NewsLetterController/index';
$route['send-news-letter-mail'] = ADMIN.NEWS_LETTER.'NewsLetterController/add';
$route['store-news-letter-mail'] = ADMIN.NEWS_LETTER.'NewsLetterController/insert';
$route['news-letter-history-listing'] = ADMIN.NEWS_LETTER.'NewsLetterController/newsletterHistory';

$route['listing-author-payment'] = ADMIN.AUTHOR_PAYMENT.'AuthorPaymentController/index';
$route['add-author-payment'] = ADMIN.AUTHOR_PAYMENT.'AuthorPaymentController/add';
$route['store-author-payment'] = ADMIN.AUTHOR_PAYMENT.'AuthorPaymentController/insert';

$route['listing-author-order-ratio'] = ADMIN.AUTHOR_ORDER_RATIO.'AuthorOrderRatioController/index';
$route['details-author-order-ratio/(:any)'] = ADMIN.AUTHOR_ORDER_RATIO.'AuthorOrderRatioController/authorOrderRatioByAuthorId/$1';
$route['add-author-order-ratio'] = ADMIN.AUTHOR_ORDER_RATIO.'AuthorOrderRatioController/add';
$route['store-author-order-ratio'] = ADMIN.AUTHOR_ORDER_RATIO.'AuthorOrderRatioController/insert';
$route['listing-author-ratio'] = ADMIN.AUTHOR_ORDER_RATIO.'AuthorOrderRatioController/authorRatioListing';

$route['listing-order'] = ADMIN.SHOPPING_CART.'ShoppingCartController/orderListing';
$route['listing-payment'] = ADMIN.SHOPPING_CART.'ShoppingCartController/paymentListing';
$route['payment-product-details/(:any)'] = ADMIN.SHOPPING_CART.'ShoppingCartController/getPaymentDetails/$1';
$route['listing-sell-product'] = ADMIN.SHOPPING_CART.'ShoppingCartController/getSellProductListByAuthor';
$route['Book-purchase-users-listing'] = ADMIN.SHOPPING_CART.'ShoppingCartController/getBookPurchaseUsers';


//website

$route['contact'] = 'website/ContactController/index';
$route['add-contact-us'] = 'website/ContactController/insertContactUs';

$route['register'] = 'website/RegisterController/index';
$route['register-add'] = 'website/RegisterController/insert';
$route['check-login'] = 'website/RegisterController/login';
$route['logout'] = 'website/RegisterController/logout';
$route['forgat-password-view'] = 'website/RegisterController/forgotPasswordView';
$route['forgat-password-store'] = 'website/RegisterController/forgotPasswordAdd';
$route['reset-password'] = 'website/ResetPasswordController/index';

$route['author-register'] = 'website/SellerRegisterController/index';
$route['author-add'] = 'website/SellerRegisterController/insert';
$route['author-kyc-add'] = 'website/SellerRegisterController/KYCAdd';
$route['author-kyc-upload'] = 'website/SellerRegisterController/KYCUpload';
$route['thank-you'] ='website/SellerRegisterController/thankYou';

$route['cart'] = 'website/CartController/index';
$route['addcart'] = 'website/CartController/add';
$route['check_out'] = 'website/CheckOutController/index';
$route['update-cart'] = 'website/CartController/updateCart';
$route['remove-cart/(:any)']='website/CartController/remove/$1';
$route['billing'] = 'website/BillingDetailController/insert';

$route['product_detail'] = 'welcome/ProductDetail';
$route['about'] = 'welcome/about';
$route['login'] = 'welcome/login';

$route['newsletter'] ='website/SubscribeNewsLetterController/index';

$route['terms-and-conditions'] ='admin/Terms-Conditions/TermsConditionsController/index';
$route['change-status'] ='admin/Terms-Conditions/TermsConditionsController/ChangeStatus';

$route['web-user-profile'] = 'website/UserController/index';
$route['web-user-profile-edit'] = 'website/UserController/updateView';
$route['web-user-profile-update'] = 'website/UserController/update';
$route['web-user-change-password'] = 'website/UserController/changePassword';
$route['web-user-change-password-insert'] = 'website/UserController/changePasswordAdd';
$route['web-profile-image'] = 'website/UserController/changeProfileImage';

//Payumoney Payment Gateway Integration
$route['payumoney-success'] ='website/payumoney/payumoney/success';
$route['payumoney-error'] ='website/payumoney/payumoney/error';
$route['payumoney-cancel'] ='website/payumoney/payumoney/cancel';
/* --------------------Book Rating------------------------*/
$route['rating'] ='website/RatingReviewController';
$route['add-review'] ='website/RatingReviewController/addReview';
$route['insert-review'] ='website/RatingReviewController/storeReview';
$route['all-review'] ='website/RatingReviewController/allReview';


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
