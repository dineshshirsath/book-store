 
    <!-- NEWS LETTERS -->
    <section class="footer2 footer-newsletters">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 box-newsletters">
                        <h2 class="newsletters-title">SUBSCRIBE NOW FOR STAYING UPDATED</h2>
                        <form name="newsForm" class="form-horizontal" method="post" action="<?php echo base_url();?>newsletter">
                            <div class="input-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required="" >
                            </div>
                            <div style="margin-top: 20px" class="form-group">
                                <!-- Button -->
                                <div class="col-sm-12 controls">
                                    <input type="submit" class="btn btn-success news-btn" name="subscribe" value="subscribe" alt="subscribe" title="subscribe">
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 newsletters-img"><img src="<?php echo base_url();?>assets/image/book/bookback.gif" alt="The Book Lounge" title="The Book Lounge"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
<section id="footer" class="ftr-2">

        <div class="container footer2">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3 footer-zone footer-about">
                        <h3 alt="About Us" title="About Us">About Us</h3>
                        <p>Welcome to The Book Lounge Fin-tech start up with perfect blend of professionals having experience in TAX, Finance and Technology. Making knowledge available with ease to the professionals anytime and anyway.</p>

                       <!-- <div class="socialfooter">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-google-plus-g"></i></a>
                        </div>-->
                    </div>
                    <div class="col-md-3 footer-about">
                        <h3 alt="Useful Links" title="Useful Links">Useful Links</h3>
                        <div class="col-md-12 footer-box-news-book"> 
                            <div class="col-md-8 footer-small-desc">
                                <i class="fas fa-minus" style="color: green;"></i> &nbsp;&nbsp; <a href="<?php echo base_url();?>" alt="Home" title="Home">Home</a>
                            </div>
                        </div>
                        <div class="col-md-12 footer-box-news-book"> 
                            <div class="col-md-8 footer-small-desc" style="width: 100.66666667%;">
                                <i class="fas fa-minus" style="color: green;"></i> &nbsp;&nbsp; 
                                <a href="<?php echo base_url();?>author-register" alt="Author Registration" title="Author Registration">Author Registration &nbsp;</a>
                                <span style="color: #ffffff">/</span>&nbsp;<a href="<?php echo base_url();?>admin" alt="Login" title="Login">&nbsp;Login</a>   
                            </div>
                        </div>
                        <div class="col-md-12 footer-box-news-book">
                            <div class="col-md-8 footer-small-desc">
                                <i class="fas fa-minus" style="color: green;"></i>&nbsp;&nbsp;<a href="<?php echo base_url();?>about" alt="About Us" title="About Us">&nbsp;About Us</a>
                            </div>
                        </div>
                        <div class="col-md-12 footer-box-news-book">
                            <div class="col-md-8 footer-small-desc">
                               <i class="fas fa-minus" style="color: green;"></i> &nbsp;&nbsp;<a href="<?php echo base_url();?>contact" alt="Contact Us" title="Contact Us">Contact Us</a>
                            </div>
                        </div>
						<div class="col-md-12 footer-box-news-book">
                            <div class="col-md-8 footer-small-desc">
                               <i class="fas fa-minus" style="color: green;"></i> &nbsp;&nbsp;<a href="#" alt="Terms & Conditions" title="Terms & Conditions">Terms & Conditions</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 footer-zone footer-about">
                        <h3 alt="Contact Us" title="Contact Us">CONTACTS DETAILS</h3>
                        <p><i class="fa fa-map-marker"></i>Address:Haware Centurian Mall, Sewoods West, Sector 19A,New Mumbai</p>
                        <p><i class="fa fa-phone"></i><a href="tel:+91 9594 872 502" title="+91 9594 872 502" style="color: white;" alt="Call" title="Call">+91 9594 872 502</a></p>
                        <p><i class="fas fa-envelope"></i><a href="mailto:datafalconx@gmail.com" title="datafalconx@gmail.com" style="color: white;" class="primary" alt="Email" title="Email"> datafalconx@gmail.com</a></p>

                    </div>
                    <div class="col-md-3 box-tags footer-about">
                    </div>
                    <div class="footer-map"><img src="<?php echo base_url();?>assets/image/book/open-book.png" alt="The Book Lounge" title="The Book Lounge"></div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-xs-6 copyright"> &copy; <?php echo date("Y"); ?> All Rights Reserved by <a href="#" target="_blank" style="color: green;" alt="FalconX.in" title="FalconX.in">FalconX.in</a>. Design by <a href="http://orbs-solutions.com/" target="_blank" style="color: green;" alt="Orbs Solutions" title="Orbs Solutions"> Orbs Solutions</a> </div>
                        <div class="col-xs-6 payment-card">
                            <a href="#" alt="Facebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                            <a href="#" alt="Twitter" title="Twitter"><i class="fab fa-twitter"></i></a>
                            <a href="#" alt="Instagram" title="Instagram"><i class="fab fa-instagram"></i></a>
                            <a href="#" alt="Google Plus" title="Google Plus"><i class="fab fa-google-plus-g"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/website/modernizr.2.5.3.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/website/bootstrap.min.js"></script>

    <!--Mini Flexslide-->
    <script src="<?php echo base_url();?>assets/js/website/jquery.flexslider.js" type="text/javascript"></script>

    <script src="<?php echo base_url();?>assets/js/website/shCore.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/website/shBrushXml.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/website/shBrushJScript.js" type="text/javascript"></script>

    <!-- OwnCarousel -->
    <script src="<?php echo base_url();?>assets/js/website/owl.carousel.js"></script>

    <script src="<?php echo base_url();?>assets/js/website/jquery.mixitup.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/website/turn.min.js"></script>

    <!-- Animation Text -->
    <script src="<?php echo base_url();?>assets/js/website/jquery.fittext.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/website/jquery.lettering.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/website/jquery.textillate.js" type="text/javascript"></script>

    <!-- General Script -->
    <script src="<?php echo base_url();?>assets/js/website/main.js" type="text/javascript"></script>

    <script>
    $(function () {
        "use strict";
        setTimeout(function () {
            $('.tlt').css("visibility", "visible");
            $('.tlt').textillate({ in: { effect: 'rollIn' } });
        }, 2000);
        setTimeout(function () {
            $('.tlt2').css("visibility", "visible");
            $('.tlt2').textillate({ in: { effect: 'bounceIn' } });
        }, 3000);
        setTimeout(function () {
            $('.tlt2').css("visibility", "visible");
        }, 3000);
    });
    </script>


</body></html>